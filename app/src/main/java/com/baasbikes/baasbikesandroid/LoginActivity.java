package com.baasbikes.baasbikesandroid;

import android.app.AlertDialog;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.Authentication;
import com.baasbikes.baasbikesandroid.models.Person;
import com.baasbikes.baasbikesandroid.rest.BaasCoreRestApi;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    private EditText emailField;
    private EditText passwordField;
    private Button logInButton;
    private TextView forgotPasswordButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        setupView();
//        getMixpanelAPI().track("account.signIn.viewed");
    }

    @Override
    public void setupView() {
        emailField = findViewById(R.id.log_in_email_field);
        passwordField = findViewById(R.id.log_in_password_field);
        logInButton = findViewById(R.id.log_in_button);
        forgotPasswordButton = findViewById(R.id.log_in_forgot_password_link);

        logInButton.setOnClickListener(v -> logIn());
        forgotPasswordButton.setOnClickListener(v -> forgotPassword());
    }

    @Override
    public void onBackPressed() {
//        getMixpanelAPI().track("account.signIn.back");
        super.onBackPressed();
    }

    private void logIn() {
        showProgress(R.string.login_progress_message);
        person = new Person();
        person.setEmail(emailField.getText().toString());
        person.setPassword(passwordField.getText().toString());
        person.logIn(new AsyncTaskCallback<Authentication>() {
            @Override
            public void success(Authentication data) {
                dismissProgress();
//                getMixpanelAPI().track("account.signIn.error");
//                getMixpanelAPI().identify(person.getEmail());
                ApplicationState.getInstance().restore();
                LoginActivity.this.finish();
            }

            @Override
            public void error(Throwable e) {
                dismissProgress();
//                getMixpanelAPI().track("account.signIn.error");
                showAlertDialog(R.string.login_alert_invalid_title, R.string.login_alert_invalid_message);
            }
        });
    }

    private void forgotPassword() {
        final EditText dialogEmailField = (EditText) getLayoutInflater().inflate(R.layout.edit_text_within_dialog, null);
        if (emailField.getText().length() > 0) {
            dialogEmailField.setText(emailField.getText());
            dialogEmailField.setSelection(dialogEmailField.getText().length());
        }

        new AlertDialog.Builder(this)
                .setTitle(R.string.login_alert_forgot_password_title)
                .setMessage(R.string.login_alert_forgot_password_message)
                .setView(dialogEmailField)
                .setPositiveButton(R.string.generic_continue, (dialog, which) -> {
//                    getMixpanelAPI().track("account.signIn.forgotPassword.submitted");
                    this.resetPassword(dialogEmailField.getText().toString());
                    dialog.dismiss();
                })
                .setNegativeButton(R.string.cancel, (dialog, which) -> {
//                    getMixpanelAPI().track("account.signIn.forgotPassword.canceled");
                    dialog.dismiss();
                }).show();

//        getMixpanelAPI().track("account.signIn.forgotPassword.viewed");
    }

    private void resetPassword(String email) {
        Map<String, String> params = new HashMap<>();
        params.put("email", email);

        BaasCoreRestApi.Service.getInstance().resetPassword(params).enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> c, Response<Object> response) {
                showAlertDialog(R.string.login_alert_reset_password_success_title,
                        R.string.login_alert_reset_password_success_message);
            }

            @Override
            public void onFailure(Call<Object> c, Throwable t) {
                showAlertDialog(R.string.login_alert_reset_password_success_title,
                        R.string.login_alert_reset_password_success_message);
            }
        });
    }
}
