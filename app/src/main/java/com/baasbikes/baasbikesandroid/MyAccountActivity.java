package com.baasbikes.baasbikesandroid;

import android.content.Context;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.PaymentMethod;
import com.baasbikes.baasbikesandroid.models.Person;
import com.google.android.material.switchmaterial.SwitchMaterial;

public class MyAccountActivity extends BasePaymentActivity {

    private TextView tvFullName;
    private TextView tvEmail;
    private TextView tvPaymentDescription;
    private SwitchMaterial swShowInstructions;
    private LinearLayout editInfoLayout;
    private LinearLayout editPaymentLayout;
    private LinearLayout showInstructionsLayout;
    private LinearLayout signOutLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        setupView();
        populateAccountDetails();
    }

    @Override
    public void setupView() {
        tvFullName = (TextView) findViewById(R.id.tv_full_name);
        tvEmail = (TextView) findViewById(R.id.tv_email);
        tvPaymentDescription = (TextView) findViewById(R.id.tv_payment_description);
        swShowInstructions = (SwitchMaterial) findViewById(R.id.sw_show_instructions);
        editInfoLayout = (LinearLayout) findViewById(R.id.btn_edit_info);
        editPaymentLayout = (LinearLayout) findViewById(R.id.btn_edit_payment);
        showInstructionsLayout = (LinearLayout) findViewById(R.id.btn_show_instructions);
        signOutLayout = (LinearLayout) findViewById(R.id.btn_sign_out);

        editInfoLayout.setOnClickListener(v -> onEditInfo());
        editPaymentLayout.setOnClickListener(v -> startAddCardActivity());
        showInstructionsLayout.setOnClickListener(v -> onShowInstructions());
        signOutLayout.setOnClickListener(v -> onSignOut());
    }

    private void populateAccountDetails() {
        person = Person.getLoggedInPerson(BaasBikesApplication.getAppContext());

        tvFullName.setText(person.getFullName());
        tvEmail.setText(person.getEmail());
        if (person.getPaymentMethod() != null) {
            PaymentMethod paymentMethod = person.getPaymentMethod();
            tvPaymentDescription.setAllCaps(true);
            tvPaymentDescription.setText(getString(R.string.my_account_payment_description,
                    paymentMethod.getCardType(), paymentMethod.getLastFour()));
        } else {
            tvPaymentDescription.setText(R.string.add_payment_card);
        }
        swShowInstructions.setChecked(person.shouldSeeInstructions());
    }

    private void onEditInfo() {
//        getMixpanelAPI().track("account.edit");
        navigateTo(EditAccountInfoActivity.class, false);
    }

    private void onShowInstructions() {
        person = Person.getLoggedInPerson(BaasBikesApplication.getAppContext());
        person.setSeeInstructions(!person.shouldSeeInstructions());
        person.saveAsLoggedInPerson(BaasBikesApplication.getAppContext());
        swShowInstructions.setChecked(person.shouldSeeInstructions());
    }

    private void onSignOut() {
//        getMixpanelAPI().track("account.signOut");
        Context context = BaasBikesApplication.getAppContext();
        person = Person.getLoggedInPerson(context);
        person.logOut(context);
        ApplicationState.getInstance().notifyAppOfStateChange();
        this.finish();
    }
}
