package com.baasbikes.baasbikesandroid.models;

import com.google.gson.annotations.Expose;

/**
 * Created by jmolineaux on 4/2/16.
 */
public class BikeStyle {

    @Expose
    private String name;

    public String getName() {
        return this.name;
    }
}
