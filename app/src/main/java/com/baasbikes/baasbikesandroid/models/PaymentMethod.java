package com.baasbikes.baasbikesandroid.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by jmolineaux on 4/17/16.
 */
public class PaymentMethod implements Serializable {

    @Expose
    private String cardType;
    @Expose
    private String issuingBank;
    @Expose
    @SerializedName("default")
    private Boolean isDefault;
    @Expose
    @SerializedName("expired")
    private Boolean isExpired;
    @Expose
    @SerializedName("last_4")
    private String lastFour;
    @Expose
    private String expirationMonth;
    @Expose
    private String expirationYear;

    public String getCardType() {
        return this.cardType;
    }

    public String getLastFour() {
        return this.lastFour;
    }

}
