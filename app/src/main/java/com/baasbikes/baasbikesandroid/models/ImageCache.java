package com.baasbikes.baasbikesandroid.models;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by jmolineaux on 7/27/16.
 */
public class ImageCache {

    private static String TAG = ImageCache.class.getSimpleName();
    private static ImageCache instance;
    private HashMap<UUID, HashMap<String, Bitmap>> cache;

    public static ImageCache getInstance() {
        if (instance == null) {
            instance = new ImageCache();
        }
        return instance;
    }

    private ImageCache() {
        this.cache = new HashMap<>();
    }

    public void cacheImageSets(final Map<String, Map<String, String>> imageSets) {
        if (imageSets != null) {
            for (String setId : imageSets.keySet()) {
                Map<String, String> imageSet = imageSets.get(setId);
//                for (String key : imageSet.keySet()) {
//                    this.cacheImage(imageSet.get(key), key, UUID.fromString(setId));
//                }
            }
        }
    }

    public void cacheImage(final String imageUri, final String key, final UUID set) {
        Bitmap image = loadBitmapFromDataUri(imageUri);
        if (this.cache.get(set) != null) {
            this.cache.get(set).put(key, image);
        } else {
            HashMap<String, Bitmap> setMap = new HashMap<>();
            setMap.put(key, image);
            this.cache.put(set, setMap);
        }
    }

    public Bitmap getImage(final String key, final UUID set) {
        Map<String, Bitmap> imageSet = this.cache.get(set);
        if (imageSet == null) { return null; }
        return imageSet.get(key);
    }

    private Bitmap loadBitmapFromDataUri(String uri) {
        String imageUri = uri.replaceAll("data:image/\\w+;base64,", "");
        byte[] b = Base64.decode(imageUri, Base64.DEFAULT);
        Bitmap bmp = BitmapFactory.decodeByteArray(b, 0, b.length);
        return bmp;
    }


}
