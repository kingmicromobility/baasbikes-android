package com.baasbikes.baasbikesandroid.models.lock;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LockFactory {

    public static String FAKE_LOCK = "fake";
    public static String DUTCH_RETROFIT_LOCK = "dutch-retrofit";
    public static String SEAT_POST_LOCK = "seat-post";

    public static Lock createLock(String type, String lockId, String macAddress, String urn) {
        Lock lock;
        if (type.equalsIgnoreCase(DUTCH_RETROFIT_LOCK)) {
            lock = new DutchRetrofitLock(type, lockId, macAddress, urn);
        } else if (type.equalsIgnoreCase(SEAT_POST_LOCK)) {
            lock = new SeatPostLock(type, lockId, macAddress, urn);
        } else {
            lock = new FakeLock(type, lockId, macAddress, urn);
        }
        return lock;
    }

    public static Lock createLock(String urn) {
        String urnM = "baas.lock.fake.baas-11345";
        Pattern urnPattern = Pattern.compile("baas\\.lock\\.([\\w\\d\\-_:]+)\\.(baas-([\\w\\d\\-_:]+))");
        Matcher urnMatcher = urnPattern.matcher(urnM);
        urnMatcher.find();
        return createLock(urnMatcher.group(1), urnMatcher.group(2), urnMatcher.group(3), urnMatcher.group(0));
    }
}
