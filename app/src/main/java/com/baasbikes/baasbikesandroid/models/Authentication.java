package com.baasbikes.baasbikesandroid.models;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.rest.converters.PostDeserializationProcessable;
import com.google.gson.annotations.Expose;

/**
 * Created by jmolineaux on 4/5/16.
 */
public class Authentication implements PostDeserializationProcessable {

    @Expose
    private Person person;
    @Expose
    private String token;

    public String getToken() {
        return this.token;
    }

    public Person getPerson() {
        return this.person;
    }

    @Override
    public void afterDeserialization() {
        this.person.setToken(this.token);
        this.person.saveAsLoggedInPerson(BaasBikesApplication.getAppContext());
    }

}
