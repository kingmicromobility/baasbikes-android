package com.baasbikes.baasbikesandroid.models;

import com.baasbikes.baasbikesandroid.rest.APIException;
import com.baasbikes.baasbikesandroid.rest.BaasCoreRestApi;
import com.baasbikes.baasbikesandroid.rest.NeedToAuthenticateException;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BikeFeedbackPromptSet implements Serializable {

    public static final String OUT_OF_SESSION_PROBLEM_PROMPTS = "out_of_session_problem";
    public static final String IN_SESSION_PROBLEM_PROMPTS = "in_session_problem";
    public static final String SESSION_END_FEEDBACK = "session_end_feedback";

    @Expose
    private UUID guid;
    @Expose
    private String name;
    @Expose
    private List<BikeFeedbackPrompt> prompts;

    public static void getByName(final String name, final AsyncTaskCallback<BikeFeedbackPromptSet> asyncTaskCallback) {
        BaasCoreRestApi.Service.getInstance()
                .getFeedbackPromptSets(name).enqueue(new Callback<ApiResultPage<BikeFeedbackPromptSet>>() {
            @Override
            public void onResponse(
                    Call<ApiResultPage<BikeFeedbackPromptSet>> call,
                    Response<ApiResultPage<BikeFeedbackPromptSet>> response
            ) {
                if (response.code() == 200) {
                    ArrayList<BikeFeedbackPromptSet> set = response.body().getResults();
                    if (set != null && !set.isEmpty()) {
                        asyncTaskCallback.success(response.body().getResult(0));
                    } else {
                        asyncTaskCallback.error(new APIException("Feedback prompt set is null or empty: " + name));
                    }
                } else {
                    asyncTaskCallback.error(new APIException("Failed to get feedback prompt set: " + name));
                }
            }

            @Override
            public void onFailure(Call<ApiResultPage<BikeFeedbackPromptSet>> call, Throwable t) {
                if (t instanceof NeedToAuthenticateException) {
                    asyncTaskCallback.success(null);
                } else {
                    asyncTaskCallback.error(t);
                }
            }
        });
    }

    public List<BikeFeedbackPrompt> getPrompts() {
        return prompts;
    }

    public BikeFeedbackPrompt getPrompt(Integer position) {
        return this.getPrompts().get(position);
    }

    public UUID getGuid() {
        return guid;
    }
}
