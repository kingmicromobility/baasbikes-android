package com.baasbikes.baasbikesandroid.models.lock;

import com.baasbikes.baasbikesandroid.models.LockEvent;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;

/**
 * Created by jmolineaux on 4/5/16.
 */
public class DutchRetrofitLock extends Lock {

    public DutchRetrofitLock(String type, String lockId, String macAddress, String urn) {
        this.type = type;
        this.baasLockId = lockId;
        this.macAddress = macAddress;
        this.urn = urn;
    }

    @Override
    public void unlock(AsyncTaskCallback<LockEvent> asyncTaskCallback) {
        LockEvent.createLockEvent(bike, "unlocked", asyncTaskCallback);
    }

    @Override
    public void lock(AsyncTaskCallback<LockEvent> asyncTaskCallback) {
        LockEvent.createLockEvent(bike, "lock", asyncTaskCallback);
    }

}
