package com.baasbikes.baasbikesandroid.models;

import com.baasbikes.baasbikesandroid.rest.BaasCoreRestApi;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jmolineaux on 4/7/16.
 */
public class BikeFeedbackEvent implements Serializable {

    @Expose
    private UUID guid;
    @Expose
    @SerializedName("bike")
    private UUID bikeGuid;
    @Expose
    @SerializedName("prompts")
    private UUID promptSetUUID;
    @Expose
    private LatLng location;
    @Expose
    private Boolean bikeRideable;
    @Expose
    private Integer starRating;
    @Expose
    private Collection<BikeFeedbackDetail> detailSet;

    public BikeFeedbackEvent(Bike bike, LatLng location, Boolean bikeRideable, Integer starRating,
                             UUID promptSetUUID, Collection<BikeFeedbackDetail> detailSet) {
        this.bikeGuid = bike.getGuid();
        this.location = location;
        this.bikeRideable = bikeRideable;
        this.starRating = starRating;
        this.promptSetUUID = promptSetUUID;
        this.detailSet = detailSet;
    }

    public void save(final AsyncTaskCallback<BikeFeedbackEvent> asyncTaskCallback) {
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        if (guid == null) {
            api.createBikeFeedbackEvent(this).enqueue(new Callback<BikeFeedbackEvent>() {
                @Override
                public void onResponse(Call<BikeFeedbackEvent> c, Response<BikeFeedbackEvent> response) {
                    asyncTaskCallback.success(response.body());
                }
                @Override
                public void onFailure(Call<BikeFeedbackEvent> c, Throwable t) {
                    asyncTaskCallback.error(t);
                }
            });
        }
    }

    public Integer getStarRating() {
        return this.starRating;
    }

    public Boolean isRideable() {
        return this.bikeRideable;
    }

    public UUID getPromptSetUUID() {
        return this.promptSetUUID;
    }

    public Collection<BikeFeedbackDetail> getDetailSet() {
        return this.detailSet;
    }

}
