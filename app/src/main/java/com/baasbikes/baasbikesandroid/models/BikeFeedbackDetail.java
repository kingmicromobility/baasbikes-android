package com.baasbikes.baasbikesandroid.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.UUID;

/**
 * Created by jmolineaux on 4/7/16.
 */
public class BikeFeedbackDetail {

    @Expose
    private UUID guid;
    @Expose
    @SerializedName("prompt")
    private UUID promptGuid;
    @Expose
    private String value;

    public BikeFeedbackDetail(BikeFeedbackPrompt prompt, String value) {
        this.promptGuid = prompt.getGuid();
        this.value = value;
    }

    public UUID getPromptGuid() {
        return this.promptGuid;
    }

    public String getValue() {
        return this.value;
    }

}
