package com.baasbikes.baasbikesandroid.models;

/**
 * Created by jmolineaux on 4/4/16.
 */
public enum RentalState {

    VIEWING_MARKET,
    BIKE_SELECTED,
    BIKE_RESERVED,
    TRIP_IN_PROGRESS,
    BIKE_HELD,
    AWAITING_BIKE_FEEDBACK

}
