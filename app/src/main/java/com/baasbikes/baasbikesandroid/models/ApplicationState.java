package com.baasbikes.baasbikesandroid.models;

import android.content.Context;
import android.util.Log;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.location.LocationService;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.baasbikes.baasbikesandroid.views.MapLayout;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

/**
 * Created by jmolineaux on 3/26/16.
 */
public class ApplicationState extends Observable {

    private static String TAG = ApplicationState.class.getSimpleName();
    private static ApplicationState instance;
    private Reservation currentReservation;
    private Session currentSession;
    private Market currentMarket;
    private Bike currentBike;
    private Trip currentTrip;
    private MapLayout mainMap;
    private Map<String, BikeFeedbackPromptSet> bikeFeedbackPromptSets = new HashMap<>();

    private ApplicationState() {}

    public static ApplicationState getInstance() {
        if (instance == null) {
            instance = new ApplicationState();
        }
        return instance;
    }

    public void restore(final Person person) {
        final LatLng location = LocationService.getInstance().getCurrentLatLng();

        if (person != null && person.isLoggedIn()) {
            this.fetchFeedbackPrompts(BikeFeedbackPromptSet.SESSION_END_FEEDBACK);
            this.fetchFeedbackPrompts(BikeFeedbackPromptSet.IN_SESSION_PROBLEM_PROMPTS);
            this.fetchFeedbackPrompts(BikeFeedbackPromptSet.OUT_OF_SESSION_PROBLEM_PROMPTS);
            person.activeSession(new AsyncTaskCallback<Session>() {
                @Override
                public void success(final Session session) {
                    if (session != null) {
                        Trip trip = session.getCurrentTrip();
                        if (trip != null) {
                            setState(session.getReservation(),
                                    session,
                                    session.getMarket(),
                                    session.getBike(),
                                    trip);
                            notifyAppOfStateChange();
                        } else {
                            setState(session.getReservation(),
                                    session,
                                    session.getMarket(),
                                    session.getBike(),
                                    null);
                            notifyAppOfStateChange();
                        }
                    } else {
                        person.activeReservation(new AsyncTaskCallback<Reservation>() {
                            @Override
                            public void success(final Reservation reservation) {
                                if (reservation != null) {
                                    setState(reservation,
                                            null,
                                            reservation.getMarket(),
                                            reservation.getBike(),
                                            null);
                                    notifyAppOfStateChange();
                                } else {
                                    Market.nearest(location, new AsyncTaskCallback<Market>() {
                                        @Override
                                        public void success(final Market market) {
                                            setState(null, null, market, null, null);
                                            notifyAppOfStateChange();
                                        }
                                        @Override
                                        public void error(Throwable e) {
                                            notifyAppOfStateRestoreFailure(e);
                                        }
                                    });
                                }
                            }
                            @Override
                            public void error(Throwable e) {
                                notifyAppOfStateRestoreFailure(e);
                            }
                        });
                    }
                }
                @Override
                public void error(Throwable e) {
                    notifyAppOfStateRestoreFailure(e);
                }
            });
        } else {
            Market.nearest(location, new AsyncTaskCallback<Market>() {
                @Override
                public void success(final Market market) {
                    setState(null, null, market, null, null);
                    notifyAppOfStateChange();
                }

                @Override
                public void error(Throwable e) {
                    notifyAppOfStateRestoreFailure(e);
                }
            });
        }
    }

    public void restore() {
        Person person = Person.getLoggedInPerson(BaasBikesApplication.getAppContext());
        this.restore(person);
    }

    private void fetchFeedbackPrompts(final String name) {
        BikeFeedbackPromptSet.getByName(name, new AsyncTaskCallback<BikeFeedbackPromptSet>() {
            @Override
            public void success(BikeFeedbackPromptSet data) {
                ApplicationState.this.bikeFeedbackPromptSets.put(name, data);
            }
            @Override
            public void error(Throwable e) {
                Log.e(TAG, e.getMessage());
            }
        });
    }

    public Reservation getCurrentReservation() {
        return currentReservation;
    }

    public Session getCurrentSession() {
        return currentSession;
    }

    public Market getCurrentMarket() {
        return currentMarket;
    }

    public Bike getCurrentBike() {
        return currentBike;
    }

    public Trip getCurrentTrip() {
        return currentTrip;
    }

    public MapLayout getMainMap() {
        return mainMap;
    }

    public BikeFeedbackPromptSet getBikeFeedbackPrompts(String name) {
        return bikeFeedbackPromptSets.get(name);
    }

    public RentalState getRentalState() {
        if (currentTrip != null) {
            return RentalState.TRIP_IN_PROGRESS;
        } else if (currentSession != null) {
            if (currentSession.isEnded()) {
                return RentalState.AWAITING_BIKE_FEEDBACK;
            } else {
                return RentalState.BIKE_HELD;
            }
        } else if (currentReservation != null) {
            return RentalState.BIKE_RESERVED;
        } else if (currentBike != null) {
            return RentalState.BIKE_SELECTED;
        } else {
            return RentalState.VIEWING_MARKET;
        }
    }

    public void setCurrentBike(Bike bike) {
        Bike previousBike = this.getCurrentBike();
        this.currentBike = bike;
        if (previousBike != null) { previousBike.getMarker().refresh(); }
        if (this.currentBike != null) { this.currentBike.getMarker().refresh(); };
    }

    public void setCurrentReservation(Reservation reservation) {
        this.currentReservation = reservation;
        this.setChanged();
    }

    public void setCurrentSession(Session session) {
        this.currentSession = session;
        this.setChanged();
    }

    public void setCurrentTrip(Trip trip) {
        this.currentTrip = trip;
        this.setChanged();
    }

    public void setMainMap(MapLayout mapLayout) {
        this.mainMap = mapLayout;
    }

    public void clearRentalState() {
        setState(null, null, this.currentMarket, null, null);
        notifyAppOfStateChange();
    }

    public void notifyAppOfStateChange() {
        setChanged();
        notifyObservers();
    }

    private void setState(
            final Reservation currentReservation,
            final Session currentSession,
            final Market currentMarket,
            final Bike currentBike,
            final Trip currentTrip) {
        this.currentReservation = currentReservation;
        this.currentSession = currentSession;
        this.currentMarket = currentMarket;
        this.currentTrip = currentTrip;
        this.setCurrentBike(currentBike);
        this.setChanged();
    }

    private void notifyAppOfStateRestoreFailure(Throwable e) {
        if (this.bikeFeedbackPromptSets.keySet().size() == 0) {
            this.fetchFeedbackPrompts(BikeFeedbackPromptSet.SESSION_END_FEEDBACK);
            this.fetchFeedbackPrompts(BikeFeedbackPromptSet.IN_SESSION_PROBLEM_PROMPTS);
            this.fetchFeedbackPrompts(BikeFeedbackPromptSet.OUT_OF_SESSION_PROBLEM_PROMPTS);
        }
        Log.d(TAG, Log.getStackTraceString(e));
    }

}
