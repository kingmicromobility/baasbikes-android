package com.baasbikes.baasbikesandroid.models;

import com.google.gson.annotations.Expose;

import java.util.UUID;

/**
 * Created by jmolineaux on 4/2/16.
 */
public class AttributionChannel {
    @Expose
    private UUID guid;
    @Expose
    private String name;
    @Expose
    private String descriptivePhrase;
    @Expose
    private String attributionCode;

    public UUID getGuid() {
        return guid;
    }

    public String getName() {
        return name;
    }

    public String getDescriptivePhrase() {
        return descriptivePhrase;
    }

    public String getAttributionCode() {
        return attributionCode;
    }

    public String toString() {
        return getDescriptivePhrase();
    }
}
