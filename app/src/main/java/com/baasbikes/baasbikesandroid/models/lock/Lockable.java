package com.baasbikes.baasbikesandroid.models.lock;

import com.baasbikes.baasbikesandroid.models.LockEvent;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;

/**
 * Created by jmolineaux on 4/5/16.
 */
public interface Lockable {

    void unlock(AsyncTaskCallback<LockEvent> asyncTaskCallback);

    void lock(AsyncTaskCallback<LockEvent> asyncTaskCallback);

    Boolean isLocked();

}
