package com.baasbikes.baasbikesandroid.models;

import android.content.Context;
import android.util.Log;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.rest.APIException;
import com.baasbikes.baasbikesandroid.rest.BaasCoreRestApi;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jmolineaux on 3/26/16.
 */
public class Person implements Serializable {

    public static final String TAG = Person.class.getSimpleName();
    public static final String PREFS_FILE = "USER_PREFERENCES";
    public static final String KEY_REGISTERED_USER = "KEY_REGISTERED_USER";
    public static final String KEY_DEEP_LINK_DATA = "KEY_DEEP_LINK_DATA";

    @Expose
    @SerializedName("token")
    private String token;

    @Expose
    @SerializedName("guid")
    private UUID guid;

    @Expose
    @SerializedName("first_name")
    private String firstName;

    @Expose
    @SerializedName("last_name")
    private String lastName;

    @Expose
    @SerializedName("phone_number")
    private String phoneNumber;

    @Expose
    @SerializedName("payment_gateway_id")
    private String paymentGatewayId;

    @Expose
    private User user;

    @Expose
    private String selfReportedChannelCode;

    @Expose
    private String autoReportedChannelCode;

    @Expose(serialize=false)
    private PaymentMethod paymentMethod;

    @Expose
    private Boolean seeInstructions = true;

    public static Person getLoggedInPerson(final Context context) {
        Person loggedInPerson = null;
        String json = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)
                .getString(KEY_REGISTERED_USER, null);

        if (json != null) {
            loggedInPerson = new Gson().fromJson(json, Person.class);
            Log.d(TAG, "user token: " + loggedInPerson.getToken());
        }

        return loggedInPerson;
    }

    public static void saveDeepLinkData(JSONObject deepLinkData, Context context) {
        Log.d(TAG, "DEEP LINK DATA: " + deepLinkData.toString());
        Type type = new TypeToken<Map<String, Object>>(){}.getType();
        Map<String, Object> newData = new Gson().fromJson(deepLinkData.toString(), type);
        Map<String, Object> existingData = getDeepLinkData(context);
        if (existingData != null && existingData.keySet().size() > 0) {
            existingData.putAll((Map<String, Object>) newData.get("nameValuePairs"));
        } else {
            existingData = (Map<String, Object>) newData.get("nameValuePairs");
        }
        context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)
                .edit()
                .putString(KEY_DEEP_LINK_DATA, new Gson().toJson(existingData))
                .apply();
    }

    public static Map<String, Object> getDeepLinkData(Context context) {
        Map<String, Object> linkData = null;
        String json = context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)
                .getString(KEY_DEEP_LINK_DATA, null);

        if (json != null) {
            Type type = new TypeToken<Map<String, Object>>(){}.getType();
            linkData = new Gson().fromJson(json, type);
        }

        return linkData;
    }

    public void saveAsLoggedInPerson(final Context context) {
        context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)
                .edit()
                .putString(KEY_REGISTERED_USER, new Gson().toJson(this))
                .apply();
    }

    public String getToken() { return token; }

    public void setToken(String token) { this.token = token; }

    public Boolean shouldSeeInstructions() {
        return this.seeInstructions;
    }

    public void setSeeInstructions(Boolean seeInstructions) {
        this.seeInstructions = seeInstructions;
    }

    public void activeReservation(final AsyncTaskCallback<Reservation> callback) {
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        api.activeReservationsForPerson(this.guid.toString())
                .enqueue(new Callback<ApiResultPage<Reservation>>() {
                    @Override
                    public void onResponse(Call<ApiResultPage<Reservation>> c, Response<ApiResultPage<Reservation>> response) {
                        if (response.code() == 200) {
                            ApiResultPage<Reservation> resultPage = response.body();
                            if (resultPage.getCount() > 0) {
                                final Reservation reservation = resultPage.getResult(0);
                                Market.getByGuid(reservation.getMarket().getGuid(), new AsyncTaskCallback<Market>() {
                                    @Override
                                    public void success(Market market) {
                                        reservation.setMarket(market);
                                        callback.success(reservation);
                                    }
                                    @Override
                                    public void error(Throwable e) {
                                        callback.error(e);
                                    }
                                });
                            } else {
                                callback.success(null);
                            }
                        } else {
                            callback.error(new APIException("Failed to get active reservation"));
                        }
                    }
                    @Override
                    public void onFailure(Call<ApiResultPage<Reservation>> c, Throwable t) { callback.error(t); }
                });
    }

    public void activeSession(final AsyncTaskCallback<Session> callback) {
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        api.activeSessionsForPerson(this.guid.toString())
                .enqueue(new Callback<ApiResultPage<Session>>() {
                    @Override
                    public void onResponse(Call<ApiResultPage<Session>> c, Response<ApiResultPage<Session>> response) {
                        if (response.code() == 200) {
                            ApiResultPage<Session> resultPage = response.body();
                            if (resultPage.getCount() > 0) {
                                final Session session = resultPage.getResult(0);
                                Market.getByGuid(session.getMarket().getGuid(), new AsyncTaskCallback<Market>() {
                                    @Override
                                    public void success(Market market) {
                                        session.setMarket(market);
                                        callback.success(session);
                                    }

                                    @Override
                                    public void error(Throwable e) {
                                        callback.error(e);
                                    }
                                });

                            } else {
                                callback.success(null);
                            }
                        } else {
                            callback.error(new APIException("Failed to get active session"));
                        }
                    }
                    @Override
                    public void onFailure(Call<ApiResultPage<Session>> c, Throwable t) { callback.error(t); }
                });
    }

    public void save(final AsyncTaskCallback<Person> asyncTaskCallback) {
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        if (guid == null) {
            api.createPerson(this).enqueue(new Callback<Person>() {
                @Override
                public void onResponse(Call<Person> call, Response<Person> response) {
                    if (response.code() == 201) {
                        Person result = response.body();
                        guid = result.getGuid();
                        result.saveAsLoggedInPerson(BaasBikesApplication.getAppContext());
                        asyncTaskCallback.success(result);
                    } else {
                        asyncTaskCallback.error(new APIException("Failed to create person."));
                    }
                }
                @Override
                public void onFailure(Call<Person> call, Throwable t) {
                    asyncTaskCallback.error(t);
                }
            });
        } else {
            api.updatePerson(this.guid.toString(), this).enqueue(new Callback<Person>() {
                @Override
                public void onResponse(Call<Person> call, Response<Person> response) {
                    if (response.code() == 200) {
                        Person result = response.body();
                        result.saveAsLoggedInPerson(BaasBikesApplication.getAppContext());
                        asyncTaskCallback.success(result);
                    } else {
                        asyncTaskCallback.error(new APIException("Failed to update person."));
                    }
                }

                @Override
                public void onFailure(Call<Person> call, Throwable t) {
                    asyncTaskCallback.error(t);
                }
            });
        }
    }

    public void updateAttributes(Map<String, Object> attributes, final AsyncTaskCallback<Person> asyncTaskCallback) {
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        api.updatePersonAttributes(this.guid.toString(), attributes).enqueue(new Callback<Person>() {
            @Override
            public void onResponse(Call<Person> call, Response<Person> response) {
                if (response.code() == 200) {
                    Person result = response.body();
                    result.saveAsLoggedInPerson(BaasBikesApplication.getAppContext());
                    asyncTaskCallback.success(result);
                } else {
                    asyncTaskCallback.error(new APIException("Failed to update person."));
                }
            }

            @Override
            public void onFailure(Call<Person> call, Throwable t) {
                asyncTaskCallback.error(t);
            }
        });
    }

    public void logIn(final AsyncTaskCallback<Authentication> asyncTaskCallback) {
        final Person thisPerson = this;
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        Map<String, String> params = new HashMap<String, String>();
        params.put("email", this.getEmail());
        params.put("password", this.getPassword());

        api.logIn(params).enqueue(new Callback<Authentication>() {
            @Override
            public void onResponse(Call<Authentication> c, Response<Authentication> response) {
                if (response.code() == 200) {
                    final Authentication thisAuthentication = response.body();
                    thisPerson.setToken(thisAuthentication.getToken());
                    thisPerson.setGuid(thisAuthentication.getPerson().getGuid());
                    thisPerson.setFirstName(thisAuthentication.getPerson().getFirstName());
                    thisPerson.setLastName(thisAuthentication.getPerson().getLastName());
                    thisPerson.setEmail(thisAuthentication.getPerson().getEmail());
                    thisPerson.setPhoneNumber(thisAuthentication.getPerson().getPhoneNumber());
                    thisPerson.saveAsLoggedInPerson(BaasBikesApplication.getAppContext());
                    thisPerson.fetchPaymentMethod(new AsyncTaskCallback<List<PaymentMethod>>() {
                        @Override
                        public void success(List<PaymentMethod> data) {
                            asyncTaskCallback.success(thisAuthentication);
                        }

                        @Override
                        public void error(Throwable e) {
                            asyncTaskCallback.error(e);
                        }
                    });
                } else {
                    asyncTaskCallback.error(new APIException("Incorrect Username or Password"));
                }
            }

            @Override
            public void onFailure(Call<Authentication> c, Throwable t) {
                asyncTaskCallback.error(t);
            }
        });
    }

    public void fetchPaymentMethod(final AsyncTaskCallback<List<PaymentMethod>> asyncTaskCallback) {
        final Person thisPerson = this;
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        api.getPaymentMethodsForPerson(this.getGuid().toString()).enqueue(new Callback<List<PaymentMethod>>() {
            @Override
            public void onResponse(Call<List<PaymentMethod>> call, Response<List<PaymentMethod>> response) {
                if (response.code() == 200) {
                    List<PaymentMethod> paymentMethods = response.body();
                    if (paymentMethods.size() > 0) {
                        thisPerson.setPaymentMethod(paymentMethods.get(0));
                        thisPerson.saveAsLoggedInPerson(BaasBikesApplication.getAppContext());
                    }
                    asyncTaskCallback.success(paymentMethods);
                } else {
                    asyncTaskCallback.error(new APIException("Failed to retrieve payment methods"));
                }
            }

            @Override
            public void onFailure(Call<List<PaymentMethod>> call, Throwable t) {
                asyncTaskCallback.error(t);
            }
        });
    }

    public void logOut(final Context context) {
        context.getSharedPreferences(PREFS_FILE, Context.MODE_PRIVATE)
                .edit()
                .remove(KEY_REGISTERED_USER)
                .apply();
    }

    public boolean isLoggedIn() {
        return this.getToken() != null;
    }

    public UUID getGuid() {
        return this.guid;
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public PaymentMethod getPaymentMethod() {
        return this.paymentMethod;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        if (this.user == null) { this.user = new User(); }
        return this.user.email;
    }

    public void setEmail(String email) {
        if (this.user == null) { this.user = new User(); }
        this.user.setEmail(email);
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public void setSelfReportedChannelCode(String selfReportedChannelCode) {
        this.selfReportedChannelCode = selfReportedChannelCode;
    }

    public void setAutoReportedChannelCode(String autoReportedChannelCode) {
        this.autoReportedChannelCode = autoReportedChannelCode;
    }

    public void setPassword(String password) {
        if (this.user == null) { this.user = new User(); }
        this.user.setPassword(password);
    }

    public String getPassword() {
        return this.user.getPassword();
    }

    public void setCurrentPassword(String password) {
        if (this.user == null) { this.user = new User(); }
        this.user.setCurrentPassword(password);
    }

    public void setNewPassword(String password) {
        if (this.user == null) { this.user = new User(); }
        this.user.setNewPassword(password);
    }

    public void setConfirmNewPassword(String password) {
        if (this.user == null) { this.user = new User(); }
        this.user.setConfirmNewPassword(password);
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getSelfReportedChannelCode() {
        return selfReportedChannelCode;
    }

    public String getAutoReportedChannelCode() {
        return autoReportedChannelCode;
    }

    public class User implements Serializable {
        private Integer pk;
        @Expose
        private String email;
        private String[] groups;
        @Expose(deserialize = false)
        private String password;
        @Expose(deserialize = false)
        private String oldPassword;
        @Expose(deserialize = false)
        private String newPassword;
        @Expose(deserialize = false)
        private String confirmNewPassword;

        public String getEmail() {
            return this.email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPassword() {
            return this.password;
        }

        public void setCurrentPassword(String oldPassword) {
            this.oldPassword = oldPassword;
        }

        public void setNewPassword(String newPassword) {
            this.newPassword = newPassword;
        }

        public void setConfirmNewPassword(String confirmNewPassword) {
            this.confirmNewPassword = confirmNewPassword;
        }
    }

}
