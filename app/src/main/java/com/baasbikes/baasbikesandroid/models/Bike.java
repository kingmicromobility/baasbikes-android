package com.baasbikes.baasbikesandroid.models;

import android.graphics.Bitmap;
import android.util.Log;

import com.baasbikes.baasbikesandroid.models.lock.Lock;
import com.baasbikes.baasbikesandroid.rest.converters.PostDeserializationProcessable;
import com.baasbikes.baasbikesandroid.util.AsyncBoundaryCallback;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

public class Bike implements PostDeserializationProcessable {

    private static String TAG = Bike.class.getSimpleName();

    @Expose
    private UUID guid;
    @Expose
    private String name;
    @Expose
    private UUID ambassador;
    @Expose
    private UUID imageSet;
    @Expose
    private BikeBrand brand;
    @Expose
    private BikeStyle style;
    @Expose
    private BikeSize size;
    @Expose
    private String status;
    @Expose
    private Boolean ownerAvailable;
    @Expose
    private Boolean baasAvailable;
    @Expose
    private LatLng lastLocation;
    @Expose
    private Integer uniqueRenterCount;
    @Expose
    private Map<String, String> detailSet;
    @Expose
    private Lock lock;

    private transient Market market;
    private transient BikeMapMarker marker;
    private transient Boolean outOfBounds = false;
    private transient String withinMarketBounds = "unknown";

    public UUID getGuid() {
        return this.guid;
    }

    public Market getMarket() {
        return this.market;
    }

    public String getName() {
        return this.name;
    }

    public UUID getImageSet() {
        return this.imageSet;
    }

    public Bitmap getImage(final String key) {
        return ImageCache.getInstance().getImage(key, this.imageSet);
    }

    public BikeBrand getBrand() {
        return this.brand;
    }

    public LatLng getLocation() {
        return this.lastLocation;
    }

    public BikeStyle getStyle() {
        return style;
    }

    public BikeSize getSize() {
        return size;
    }

    public String getStatus() {
        return status;
    }

    public Integer getUniqueRenterCount() {
        return uniqueRenterCount;
    }

    public Map<String, String> getDetailSet() {
        return detailSet;
    }

    public BikeMapMarker getMarker() {
        if (this.marker == null) {
            this.marker = new BikeMapMarker(this);
        }
        return this.marker;
    }

    public Boolean isSelected() {
        return this.equals(ApplicationState.getInstance().getCurrentBike());
    }

    public Boolean isAvailable() {
        return this.status.equalsIgnoreCase("available");
    }

    public Boolean isOutOfBounds() {
        return this.outOfBounds;
    }

    public void setLocation(LatLng location) {
        this.lastLocation = location;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public void setOutOfBounds(Boolean outOfBounds) {
        this.outOfBounds = outOfBounds;
    }

    public void select() {
        ApplicationState.getInstance().setCurrentBike(this);
    }

    public void deselect() {
        ApplicationState.getInstance().setCurrentBike(null);
    }

    public void reserve(AsyncTaskCallback<Reservation> asyncTaskCallback) {
        Reservation reservation = new Reservation(this);
        reservation.begin(asyncTaskCallback);
    }

    public void unlock(AsyncTaskCallback<LockEvent> asyncTaskCallback) {
        if (this.lock != null) {
            this.lock.unlock(asyncTaskCallback);
        } else {
            asyncTaskCallback.error(new Exception("Lock is null"));
        }
    }

    public void lock(AsyncTaskCallback<LockEvent> asyncTaskCallback) {
        if (this.lock != null) {
            this.lock.lock(asyncTaskCallback);
        } else {
            asyncTaskCallback.error(new Exception("Lock is null"));
        }
    }

    public void isWithinMarketBounds(final AsyncBoundaryCallback<Market> asyncBoundaryCallback) {
        final Bike thisBike = this;
        this.market.isInsideBounds(this.getLocation(), new AsyncBoundaryCallback<Market>() {
            @Override
            public void insideBounds(Market data) {
                thisBike.setOutOfBounds(false);
                thisBike.withinMarketBounds = "yes";
                asyncBoundaryCallback.insideBounds(data);
                ApplicationState.getInstance().notifyObservers();
            }

            @Override
            public void outsideBounds(Market data) {
                thisBike.setOutOfBounds(true);
                thisBike.withinMarketBounds = "no";
                asyncBoundaryCallback.outsideBounds(data);
                ApplicationState.getInstance().notifyObservers();
            }

            @Override
            public void error(Throwable e) {
                asyncBoundaryCallback.error(e);
            }
        });
    }

    public void updateFromBike(Bike otherBike) {
        this.name = otherBike.getName();
        this.brand = otherBike.getBrand();
        this.style = otherBike.getStyle();
        this.size = otherBike.getSize();
        this.status = otherBike.getStatus();
        this.lastLocation = otherBike.getLocation();
        this.uniqueRenterCount = otherBike.getUniqueRenterCount();
        this.detailSet = otherBike.getDetailSet();
        this.getMarker().refresh();
    }

    @Override
    public void afterDeserialization() {
        if (this.lock != null) {
            this.lock.setBike(this);
        }
    }

    @Override
    public boolean equals(Object otherBike) {
        Boolean isABike = otherBike instanceof Bike;
        return isABike && ((Bike) otherBike).getGuid().equals(getGuid());
    }

    public JSONObject getPropertiesAsJson() {
        JSONObject props = new JSONObject();
        final String undefined = "UNDEFINED";

        try {
            if (this.ambassador != null) {
                props.put("bike_ambassador", this.ambassador.toString());
            } else {
                props.put("bike_ambassador", undefined);
            }
            props.put("bike_name", this.name);
            if (this.brand != null) {
                props.put("bike_brand", this.brand.getName());
            } else {
                props.put("bike_brand", undefined);
            }
            if (this.style != null) {
                props.put("bike_style", this.style.getName());
            } else {
                props.put("bike_style", undefined);
            }
            if (this.size != null) {
                props.put("bike_size", this.size.getName());
            } else {
                props.put("bike_size", undefined);
            }
            props.put("bike_status", this.status);
            props.put("bike_renter_count", this.uniqueRenterCount);
            if (this.lock != null) {
                props.put("bike_lock_type", this.lock.getType());
            } else {
                props.put("bike_lock_type", undefined);
            }
            if (this.lastLocation != null) {
                props.put("bike_last_lat", this.lastLocation.latitude);
                props.put("bike_last_lng", this.lastLocation.longitude);
            } else {
                props.put("bike_last_lat", undefined);
                props.put("bike_last_lng", undefined);
            }
            props.put("bike_within_market", this.withinMarketBounds);

            if (this.detailSet != null && !this.detailSet.isEmpty()) {
                for (String key : this.detailSet.keySet()) {
                    props.put(key, this.detailSet.get(key));
                }
            }

            JSONObject marketProps = this.market.getPropertiesAsJson();
            Iterator<String> keys = marketProps.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                props.put(key, marketProps.get(key));
            }
        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
        }
        return props;
    }
}
