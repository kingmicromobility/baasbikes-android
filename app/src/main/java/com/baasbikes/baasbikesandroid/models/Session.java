package com.baasbikes.baasbikesandroid.models;

import android.util.Log;

import com.baasbikes.baasbikesandroid.rest.APIException;
import com.baasbikes.baasbikesandroid.rest.BaasCoreRestApi;
import com.baasbikes.baasbikesandroid.rest.converters.PostDeserializationProcessable;
import com.baasbikes.baasbikesandroid.util.AsyncBoundaryCallback;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.gson.annotations.Expose;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jmolineaux on 3/26/16.
 */
public class Session implements Serializable, PostDeserializationProcessable {

    private static String TAG = Session.class.getSimpleName();

    @Expose
    private UUID guid;
    @Expose
    private Reservation reservation;
    @Expose
    private Market market;
    @Expose
    private Bike bike;
    @Expose(serialize = false)
    private Date startedAt;
    @Expose(serialize = false)
    private Date endedAt;
    @Expose(deserialize = false)
    private Boolean ended = false;
    @Expose
    private Boolean endOutsideMarket = false;
    @Expose
    private List<Trip> tripSet;

    public Session(Reservation reservation) {
        this.reservation = reservation;
        this.market = reservation.getMarket();
        this.bike = this.market.getBikeMap().get(reservation.getBike().getGuid());
        this.tripSet = new ArrayList<>();
    }

    public UUID getGuid() { return guid; }

    public Reservation getReservation() {
        return reservation;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }

    public Bike getBike() {
        return this.market.getBikeMap().get(this.bike.getGuid());
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public Boolean getEndOutsideMarket() {
        return this.endOutsideMarket;
    }

    public void setEndOutsideMarket(Boolean endOutsideMarket) {
        this.endOutsideMarket = endOutsideMarket;
    }

    public List<Trip> getTripSet() {
        return this.tripSet;
    }

    public void begin(final AsyncTaskCallback<Session> asyncTaskCallback) {
        final Session session = this;
        this.getBike().unlock(new AsyncTaskCallback<LockEvent>() {
            @Override
            public void success(LockEvent lockEvent) {
                Trip trip = lockEvent.getTrip();
                lockEvent.getTrip().setSession(session);
                trip.startTracking();
                session.getTripSet().add(trip);
                session.setGuid(trip.getSessionGuid());
                session.startedAt = new Date();
                ApplicationState.getInstance().setCurrentTrip(trip);
                ApplicationState.getInstance().setCurrentSession(session);
                ApplicationState.getInstance().notifyAppOfStateChange();
                asyncTaskCallback.success(session);
            }

            @Override
            public void error(Throwable e) {
                asyncTaskCallback.error(e);
            }
        });
    }

    public void hold(final AsyncBoundaryCallback<Market> asyncBoundaryCallback) {
        final Bike thisBike = this.getBike();
        final Session session = this;
        thisBike.lock(new AsyncTaskCallback<LockEvent>() {
            @Override
            public void success(LockEvent lockEvent) {
                Trip trip = session.getCurrentTrip();
                trip.setEndedEvent(lockEvent.getGuid());
                trip.stopTracking();
                ApplicationState.getInstance().setCurrentTrip(null);
                ApplicationState.getInstance().setCurrentSession(session);
                thisBike.isWithinMarketBounds(asyncBoundaryCallback);
            }

            @Override
            public void error(Throwable e) {
                asyncBoundaryCallback.error(e);
            }
        });
    }

    public void resume(final AsyncTaskCallback<Session> asyncTaskCallback) {
        final Session session = this;
        this.getBike().unlock(new AsyncTaskCallback<LockEvent>() {
            @Override
            public void success(LockEvent lockEvent) {
                Trip trip = lockEvent.getTrip();
                lockEvent.getTrip().setSession(session);
                trip.startTracking();
                session.getTripSet().add(trip);
                ApplicationState.getInstance().setCurrentTrip(trip);
                ApplicationState.getInstance().setCurrentSession(session);
                ApplicationState.getInstance().notifyAppOfStateChange();
                asyncTaskCallback.success(session);
            }

            @Override
            public void error(Throwable e) {
                asyncTaskCallback.error(e);
            }
        });
    }

    public void end(final AsyncBoundaryCallback<Session> asyncBoundaryCallback) {
        this.ended = true;
        final Session session = this;
        this.getBike().isWithinMarketBounds(new AsyncBoundaryCallback<Market>() {
            @Override
            public void insideBounds(Market data) {
                session.save(new AsyncTaskCallback<Session>() {
                    @Override
                    public void success(Session data) {
                        session.endedAt = data.getEndedAt();
                        ApplicationState.getInstance().notifyAppOfStateChange();
                        asyncBoundaryCallback.insideBounds(data);
                    }

                    @Override
                    public void error(Throwable e) {
                        asyncBoundaryCallback.error(e);
                    }
                });
            }

            @Override
            public void outsideBounds(Market data) {
                if (session.getEndOutsideMarket()) {
                    session.save(new AsyncTaskCallback<Session>() {
                        @Override
                        public void success(Session data) {
                            session.endedAt = data.getEndedAt();
                            ApplicationState.getInstance().notifyAppOfStateChange();
                            asyncBoundaryCallback.insideBounds(data);
                        }

                        @Override
                        public void error(Throwable e) {
                            asyncBoundaryCallback.error(e);
                        }
                    });
                } else {
                    asyncBoundaryCallback.outsideBounds(session);
                }
            }

            @Override
            public void error(Throwable e) {
                asyncBoundaryCallback.error(e);
            }
        });
    }



    @Override
    public void afterDeserialization() {
//        this.market.getBikeMap().put(this.bike.getGuid(), this.bike);
//        this.bike.setMarket(this.market);
        for (Trip trip : this.tripSet) { trip.setSession(this); }
        if (this.ended == null) { this.ended = false; }
        if (this.endOutsideMarket == null) { this.endOutsideMarket = false; }
    }

    public void save(final AsyncTaskCallback<Session> asyncTaskCallback) {
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        if (guid == null) {
            api.createSession(this).enqueue(new Callback<Session>() {
                @Override
                public void onResponse(Call<Session> c, Response<Session> response) {
                    asyncTaskCallback.success(response.body());
                }
                @Override
                public void onFailure(Call<Session> c, Throwable t) {
                    asyncTaskCallback.error(t);
                }
            });
        } else {
            api.updateSession(this.guid.toString(), this).enqueue(new Callback<Session>() {
                @Override
                public void onResponse(Call<Session> call, Response<Session> response) {
                    asyncTaskCallback.success(response.body());
                }
                @Override
                public void onFailure(Call<Session> call, Throwable t) {
                    asyncTaskCallback.error(t);
                }
            });
        }
    }

    public Boolean isEnded() {
        return this.ended || this.endedAt != null;
    }

    public Date getEndedAt() {
        return this.endedAt;
    }

    public Long getDuration() {
        if (endedAt == null) {
            return (new Date().getTime() - this.startedAt.getTime()) / 1000;
        } else {
            return (this.endedAt.getTime() - this.startedAt.getTime()) / 1000;
        }
    }

    public Long getPrice() {
        Long duration = this.getDuration();
        Long hours = duration / 3600;
        Long price = hours * 100 + 100;
        return price;
    }

    public Trip getCurrentTrip() {
        Trip trip = this.tripSet.get(this.tripSet.size() - 1);
        if (trip.isInProgress()) {
            return trip;
        } else {
            return null;
        }
    }

    public JSONObject getPropertiesAsJson() {
        JSONObject props = new JSONObject();
        try {
            props.put("session_started_at", this.startedAt);
            props.put("session_ended", this.ended);
            props.put("session_allow_outside_market", this.endOutsideMarket);
            props.put("session_elapsed_time", this.getDuration());
            props.put("session_total_price", this.getPrice());
            JSONObject reservationProps = this.reservation.getPropertiesAsJson();
            Iterator<String> keys = reservationProps.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                props.put(key, reservationProps.get(key));
            }
        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
        }

        return props;
    }
}
