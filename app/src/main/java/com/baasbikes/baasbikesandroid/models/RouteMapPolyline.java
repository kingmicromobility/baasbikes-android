package com.baasbikes.baasbikesandroid.models;

import android.location.Location;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.R;
import com.baasbikes.baasbikesandroid.views.MapLayout;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by jmolineaux on 8/4/16.
 */
public class RouteMapPolyline {

    private Trip trip;
    private MapLayout assignedMap;
    private PolylineOptions polylineOptions;

    public RouteMapPolyline(Trip trip) {
        this.trip = trip;
    }

    public Trip getTrip() {
        return this.trip;
    }

    public PolylineOptions getPolylineOptions() {
        return this.polylineOptions;
    }

    public void assignToMap(MapLayout googleMap) {
        this.assignedMap = googleMap;
        this.refresh();
    }

    public void refresh() {
        if (this.trip.isInProgress()) { this.display(); }
        else { this.hide(); }
    }

    private void display() {
        this.polylineOptions = new PolylineOptions()
                .color(BaasBikesApplication.getAppContext().getResources().getColor(R.color.activeRouteColor))
                .width(25)
                .visible(true);
        for (Location location : this.trip.getRoute()) {
            this.polylineOptions.add(new LatLng(location.getLatitude(), location.getLongitude()));
        }
        this.show();
    }

    private void show() {
        if (this.assignedMap != null) {
            assignedMap.updatePolylineDisplay(this);
        }
    }

    private void hide() {
        this.polylineOptions.visible(false);
        this.show();
    }

}
