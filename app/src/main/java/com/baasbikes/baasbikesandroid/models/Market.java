package com.baasbikes.baasbikesandroid.models;

import android.util.Log;

import com.baasbikes.baasbikesandroid.BuildConfig;
import com.baasbikes.baasbikesandroid.models.geo.MultiPolygon;
import com.baasbikes.baasbikesandroid.rest.APIException;
import com.baasbikes.baasbikesandroid.rest.BaasCoreRestApi;
import com.baasbikes.baasbikesandroid.rest.converters.BaasConverterFactory;
import com.baasbikes.baasbikesandroid.rest.converters.PostDeserializationProcessable;
import com.baasbikes.baasbikesandroid.util.AsyncBoundaryCallback;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.SubscriptionEventListener;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Attr;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Market implements Serializable, PostDeserializationProcessable {

    private static String TAG = Market.class.getSimpleName();

    @Expose
    private UUID guid;
    @Expose
    private String name;
    @Expose(serialize = false)
    private List<Bike> bikeSet = new ArrayList<Bike>();
    @Expose
    private MultiPolygon geoshape;
    @Expose
    private List<AttributionChannel> attributionChannels = new ArrayList<AttributionChannel>();
    @Expose
    private Double closestBikeDistance;
    @Expose
    private Bike closestBike;
    @Expose
    private Map<String, Map<String, String>> bikeImages;

    private transient Map<UUID, Bike> bikeMap = new HashMap<UUID, Bike>();
    private transient Pusher websocket;
    private transient Channel websocketChannel;

    public static void getByGuid(final UUID marketGuid, final AsyncTaskCallback<Market> callback) {
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        api.getMarket(marketGuid.toString()).enqueue(new Callback<Market>() {
            @Override
            public void onResponse(Call<Market> call, Response<Market> response) {
                if (response.code() == 200) {
                    callback.success(response.body());
                } else {
                    callback.error(new APIException("Failed to retrieve market by guid."));
                }
            }

            @Override
            public void onFailure(Call<Market> call, Throwable t) {
                callback.error(t);
            }
        });
    }

    public static void nearest(final LatLng location, final AsyncTaskCallback<Market> callback) {
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
//        api.markets().enqueue(new Callback<ApiResultPage<Market>>() {
//            public void onResponse(Call<ApiResultPage<Market>> c, Response<ApiResultPage<Market>> response) {
//                if (response.code() == 200) {
//                    ApiResultPage<Market> resultPage = response.body();
//                    if (resultPage.getCount() > 0) {
//                        callback.success(resultPage.getResult(0));
//                    } else {
//                        callback.success(null);
//                    }
//                } else {
//                    callback.error(new APIException("Failed to get market."));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ApiResultPage<Market>> c, Throwable t) {
//                callback.error(t);
//            }
//        });

        api.nearestMarket(location.latitude, location.longitude)
                .enqueue(new Callback<ApiResultPage<Market>>() {
                    @Override
                    public void onResponse(Call<ApiResultPage<Market>> c, Response<ApiResultPage<Market>> response) {
                        if (response.code() == 200) {
                            ApiResultPage<Market> resultPage = response.body();
                            if (resultPage.getCount() > 0) {
                                callback.success(resultPage.getResult(0));
                            } else {
                                callback.success(null);
                            }
                        } else {
                            callback.error(new APIException("Failed to get market."));
                        }
                    }

                    @Override
                    public void onFailure(Call<ApiResultPage<Market>> c, Throwable t) {
                        callback.error(t);
                    }
                });
    }

    public void afterDeserialization() {
        this.cacheBikeImages(bikeImages);
        this.setMarketForBikes(bikeSet);
        this.bikeMap = bikeMapFromList(bikeSet);
        this.receiveWebSocketUpdates();
    }

    public UUID getGuid() {
        return this.guid;
    }

    public String getName() {
        return this.name;
    }

    public MultiPolygon getGeoshape() { return this.geoshape; }

    public Map<UUID, Bike> getBikeMap() { return this.bikeMap; }

    public List<AttributionChannel> getAttributionChannels() {
        return this.attributionChannels;
    }

    public void isInsideBounds(LatLng location, final AsyncBoundaryCallback<Market> asyncBoundaryCallback) {
        final Market market = this;
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        api.withinMarket(guid.toString(), location.latitude, location.longitude)
                .enqueue(new Callback<Map<String, String>>() {
                    @Override
                    public void onResponse(Call<Map<String, String>> call, Response<Map<String, String>> response) {
                        if (response.code() == 200) {
                            String withinMarketStr = response.body().get("within_market");
                            Boolean withinMarket = Boolean.parseBoolean(withinMarketStr);
                            if (withinMarket) {
                                asyncBoundaryCallback.insideBounds(market);
                            } else {
                                asyncBoundaryCallback.outsideBounds(market);
                            }
                        } else {
                            asyncBoundaryCallback.error(new APIException("Failed to determine whether location is inside market boundary."));
                        }
                    }
                    @Override
                    public void onFailure(Call<Map<String, String>> call, Throwable t) {
                        asyncBoundaryCallback.error(t);
                    }
                });
    }

    private void cacheBikeImages(Map<String, Map<String, String>> bikeImages) {
        ImageCache.getInstance().cacheImageSets(bikeImages);
    }

    private void setMarketForBikes(List<Bike> bikes) {
        for (Bike bike : bikes) { bike.setMarket(this); }
    }

    private Map<UUID, Bike> bikeMapFromList(List<Bike> bikes) {
        Map<UUID, Bike> bikeMap = new HashMap<UUID, Bike>();
        for (Bike bike : bikes) {
            bikeMap.put(bike.getGuid(), bike);
        }
        return bikeMap;
    }

    private void receiveWebSocketUpdates() {
        this.websocket = new Pusher(BuildConfig.PUSHER_APP_KEY);
        websocket.connect();
        this.websocketChannel = websocket.subscribe("market-" + this.guid.toString());
        this.websocketChannel.bind("bike", new MarketSubscriptionEventListener(this));
    }

    public JSONObject getPropertiesAsJson() {
        JSONObject props = new JSONObject();
        try {
            props.put("market_name", this.name);
            props.put("market_bike_count", this.bikeSet.size());
            props.put("market_closest_available_bike_distance", this.closestBikeDistance);
            if (this.closestBike != null) {
                props.put("market_closest_available_bike_lat", this.closestBike.getLocation().latitude);
                props.put("market_closest_available_bike_lng", this.closestBike.getLocation().longitude);
            }
        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
        }

        return props;
    }

    private class MarketSubscriptionEventListener implements SubscriptionEventListener {
        private Gson gson;
        private Market market;

        public MarketSubscriptionEventListener(Market market) {
            this.market = market;
            this.gson = BaasConverterFactory.getBaasGson();
        }

        @Override
        public void onEvent(String channelName, String eventName, String data) {
            Bike tempBike = this.gson.fromJson(data, Bike.class);
            Bike actualBike = this.market.bikeMap.get(tempBike.getGuid());
            if (actualBike == null) {
                this.market.bikeMap.put(tempBike.getGuid(), tempBike);
            } else {
                actualBike.updateFromBike(tempBike);
            }
            Log.d("Market", data);
        }
    }

}
