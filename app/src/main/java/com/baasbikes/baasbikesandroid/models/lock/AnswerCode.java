package com.baasbikes.baasbikesandroid.models.lock;

import com.baasbikes.baasbikesandroid.rest.BaasCoreRestApi;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.gson.annotations.Expose;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jmolineaux on 4/6/16.
 */
public class AnswerCode {

    @Expose
    private Integer answerCode;

    public static void getAnswerCode(Integer challengeCode, Lock lock, final AsyncTaskCallback<AnswerCode> asyncTaskCallback) {
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        Map<String, Object> params = new HashMap<>();
        params.put("challenge_code", challengeCode);
        params.put("lock", lock.getUrn());
        api.getAnswerCode(params).enqueue(new Callback<AnswerCode>() {
            @Override
            public void onResponse(Call<AnswerCode> call, Response<AnswerCode> response) {
                asyncTaskCallback.success(response.body());
            }

            @Override
            public void onFailure(Call<AnswerCode> call, Throwable t) {
                asyncTaskCallback.error(t);
            }
        });
    }

    public Integer getAnswerCode() { return answerCode; };
}
