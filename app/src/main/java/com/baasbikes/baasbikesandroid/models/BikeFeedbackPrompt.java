package com.baasbikes.baasbikesandroid.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by jmolineaux on 4/7/16.
 */
public class BikeFeedbackPrompt implements Serializable {

    @Expose
    private UUID guid;
    @Expose
    @SerializedName("default")
    private String defaultText;

    public UUID getGuid() {
        return guid;
    }

    public String getDefaultText() {
        return defaultText;
    }

}
