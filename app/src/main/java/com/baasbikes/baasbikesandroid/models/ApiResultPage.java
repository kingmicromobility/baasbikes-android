package com.baasbikes.baasbikesandroid.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by jmolineaux on 3/26/16.
 */
public class ApiResultPage<T> implements Serializable {
    @Expose
    private Integer count;
    @Expose
    private String next;
    @Expose
    private String previous;
    @Expose
    private ArrayList<T> results;

    public int getCount() { return count; }

    public ArrayList<T> getResults() { return results; }

    public T getResult(Integer i) { return results.get(i); }

    public Object getNext() { return next; }

    public Object getPrevious() { return previous; }
}
