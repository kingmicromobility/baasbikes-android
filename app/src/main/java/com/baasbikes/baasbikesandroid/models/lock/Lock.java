package com.baasbikes.baasbikesandroid.models.lock;

import com.baasbikes.baasbikesandroid.models.Bike;

public abstract class Lock implements Lockable {

    protected String type;
    protected String urn;
    protected String baasLockId;
    protected Boolean locked;
    protected String macAddress;
    protected Bike bike;

    public Boolean isLocked() {
        return locked;
    }

    public String getUrn() {
        return urn;
    }

    public String getMacAddress() {
        return macAddress;
    }

    public void setBike(Bike bike) {
        this.bike = bike;
    }

    public String getType() {
        return type;
    }
}
