package com.baasbikes.baasbikesandroid.models;

import android.location.Location;

import com.baasbikes.baasbikesandroid.location.LocationService;
import com.baasbikes.baasbikesandroid.rest.APIException;
import com.baasbikes.baasbikesandroid.rest.BaasCoreRestApi;
import com.baasbikes.baasbikesandroid.rest.converters.PostDeserializationProcessable;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Trip implements Serializable, PostDeserializationProcessable, Observer {

    public static Boolean isTracking = false;

    @Expose(serialize = false)
    private UUID guid;
    @Expose(serialize = false)
    @SerializedName("session")
    private UUID sessionGuid;
    @Expose(serialize = false)
    private UUID startedEvent;
    @Expose(serialize = false)
    private UUID endedEvent;
    @Expose
    private List<Location> route;

    private transient Session session;
    private transient RouteMapPolyline routeMapPolyline;

    public UUID getGuid() {
        return this.guid;
    }

    public void setGuid(final UUID guid) {
        this.guid = guid;
    }

    public Bike getBike() {
        return this.session.getBike();
    }

    public UUID getSessionGuid() {
        return this.sessionGuid;
    }

    public Session getSession() {
        return this.session;
    }

    public void setSession(final Session session) {
        this.session = session;
    }

    public void setEndedEvent(final UUID endedEvent) {
        this.endedEvent = endedEvent;
    }

    public Market getMarket() {
        return this.session.getMarket();
    }

    public List<Location> getRoute() {
        return this.route;
    }

    public Boolean isInProgress() {
        return this.startedEvent != null && this.endedEvent == null;
    }

    public void startTracking() {
        LocationService.getInstance().addObserver(this);
        isTracking = true;
    }

    public void stopTracking() {
        LocationService.getInstance().deleteObserver(this);
        isTracking = false;
    }

    @Override
    public void update(Observable observable, Object data) {
        if (this.route == null) {
            this.route = new ArrayList<>();
        }
        Location location = LocationService.getInstance().getCurrentLocation();
        LatLng latLng = LocationService.getInstance().getCurrentLatLng();

        Date timestamp = new Date(location.getTime());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(timestamp);

        if (calendar.get(Calendar.SECOND) % 5 == 0) {
            this.route.add(location);
            this.routeMapPolyline.assignToMap(ApplicationState.getInstance().getMainMap());
            this.routeMapPolyline.refresh();
        }

        if (calendar.get(Calendar.SECOND) == 0) {
            this.save(new AsyncTaskCallback<Trip>() {
                @Override
                public void success(Trip data) {
                }

                @Override
                public void error(Throwable e) {
                }
            });
        }

        //if the app is in the foreground
        Bike bike = this.getBike();
        if (bike != null) {
            this.getBike().setLocation(latLng);
            this.getBike().getMarker().refreshPosition();
        }
    }

    @Override
    public void afterDeserialization() {
        if (this.isInProgress() && !isTracking) {
            this.startTracking();
        }
        this.routeMapPolyline = new RouteMapPolyline(this);
    }

    public void save(final AsyncTaskCallback<Trip> asyncTaskCallback) {
        final Trip trip = this;
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        if (this.guid == null) {
            api.createTrip(this).enqueue(new Callback<Trip>() {
                @Override
                public void onResponse(Call<Trip> call, Response<Trip> response) {
                    if (response.code() == 201) {
                        Trip newTrip = response.body();
                        trip.setGuid(newTrip.getGuid());
                        asyncTaskCallback.success(trip);
                    } else {
                        asyncTaskCallback.error(new APIException("Failed to create Trip."));
                    }
                }

                @Override
                public void onFailure(Call<Trip> call, Throwable t) {
                    asyncTaskCallback.error(t);
                }
            });
        } else {
            api.updateTrip(this.guid.toString(), this).enqueue(new Callback<Trip>() {
                @Override
                public void onResponse(Call<Trip> call, Response<Trip> response) {
                    if (response.code() == 200) {
                        asyncTaskCallback.success(trip);
                    } else {
                        asyncTaskCallback.error(new APIException("Failed to update Trip."));
                    }
                }

                @Override
                public void onFailure(Call<Trip> call, Throwable t) {
                    asyncTaskCallback.error(t);
                }
            });
        }
    }
}
