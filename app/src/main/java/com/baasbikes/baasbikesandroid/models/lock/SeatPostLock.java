package com.baasbikes.baasbikesandroid.models.lock;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.util.Log;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.models.LockEvent;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.baasbikes.baasbikesandroid.util.ConnectionUtils;

import java.net.ConnectException;
import java.util.UUID;

/**
 * Created by jmolineaux on 4/5/16.
 */
public class SeatPostLock extends Lock {

    private static final String TAG = SeatPostLock.class.getSimpleName();
    private static final String LOCKED_STATE = "locked";
    private static final String UNLOCKED_STATE = "unlocked";

    public SeatPostLock(String type, String lockId, String macAddress, String urn) {
        this.type = type;
        this.baasLockId = lockId;
        this.macAddress = macAddress;
        this.urn = urn;
    }

    @Override
    public void unlock(final AsyncTaskCallback<LockEvent> asyncTaskCallback) {
        connectAndIssueCommand(UNLOCKED_STATE, asyncTaskCallback);
    }

    @Override
    public void lock(final AsyncTaskCallback<LockEvent> asyncTaskCallback) {
        connectAndIssueCommand(LOCKED_STATE, asyncTaskCallback);
    }

    public void connectAndIssueCommand(String command,
                                       final AsyncTaskCallback<LockEvent> asyncTaskCallback) {
        BluetoothAdapter adapter = ConnectionUtils.getBluetoothAdapter();
        BluetoothDevice lockPeripheral = adapter.getRemoteDevice(this.getMacAddress());
        if (lockPeripheral != null) {
            BluetoothGattCallback callback = new IssueCommandCallback(command, this, asyncTaskCallback);
            lockPeripheral.connectGatt(BaasBikesApplication.getAppContext(), false, callback);
        } else {
            abortBluetoothProcess("Failed to connect to lock.", null, asyncTaskCallback);
        }
    }

    public void abortBluetoothProcess(String message, BluetoothGatt gatt,
                                      AsyncTaskCallback<LockEvent> asyncTaskCallback) {
        Log.d(TAG, message);
//        if (gatt != null) {
//            gatt.disconnect();
//        }
        asyncTaskCallback.error(new ConnectException(message));
    }

    private class IssueCommandCallback extends BluetoothGattCallback {

        private UUID BAAS_SEAT_POST_LOCK_SERVICE_UUID = UUID.fromString("855487cd-4ed4-454a-9e5b-1ed4412a5a75");
        private UUID BAAS_LOCK_STATE_CHAR_UUID = UUID.fromString("a91693f8-2206-4472-b85d-ea3f85107f93");
        private UUID BAAS_CHALLENGE_CODE_CHAR_UUID = UUID.fromString("4c3bc30a-821d-47b2-9df3-5e03e99b8401");
        private UUID BAAS_UNLOCK_COMMAND_CHAR_UUID = UUID.fromString("18da67a3-d04c-43e3-90bb-8902735425cf");
        private UUID BAAS_LOCK_COMMAND_CHAR_UUID = UUID.fromString("0d408213-a740-c49f-bae7-ee959d465506");
        private UUID NOTIFICATION_DESCRIPTOR_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

        private String desiredState;
        private Lock lock;
        private AsyncTaskCallback<LockEvent> asyncTaskCallback;
        private BluetoothGattCharacteristic lockState;
        private BluetoothGattCharacteristic challengeCode;
        private BluetoothGattCharacteristic lockCommand;
        private BluetoothGattCharacteristic unlockCommand;

        public IssueCommandCallback(String desiredState, Lock lock,
                                    AsyncTaskCallback<LockEvent> asyncTaskCallback) {
            this.desiredState = desiredState;
            this.lock = lock;
            this.asyncTaskCallback = asyncTaskCallback;
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            if (newState == ConnectionUtils.BLUETOOTH_LE_STATE_CONNECTED) {
                Log.d(TAG, "Connected to device. Discovering services...");
                gatt.discoverServices();
            } else if (newState == ConnectionUtils.BLUETOOTH_LE_STATE_DISCONNECTED) {
                gatt.close();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                BluetoothGattService lockService = gatt.getService(BAAS_SEAT_POST_LOCK_SERVICE_UUID);
                if (lockService != null) {
                    // obtain characteristic handles
                    lockState = lockService.getCharacteristic(BAAS_LOCK_STATE_CHAR_UUID);
                    challengeCode = lockService.getCharacteristic(BAAS_CHALLENGE_CODE_CHAR_UUID);
                    lockCommand = lockService.getCharacteristic(BAAS_LOCK_COMMAND_CHAR_UUID);
                    unlockCommand = lockService.getCharacteristic(BAAS_UNLOCK_COMMAND_CHAR_UUID);
                    // subscribe to state notifications
                    gatt.setCharacteristicNotification(lockState, true);
                    BluetoothGattDescriptor lockStateDescriptor = lockState.getDescriptor(NOTIFICATION_DESCRIPTOR_UUID);
                    lockStateDescriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                    gatt.writeDescriptor(lockStateDescriptor);
                } else {
                    abortBluetoothProcess("Did not discover lock service in gatt database", gatt, asyncTaskCallback);
                }
            } else {
                abortBluetoothProcess("Service discovery failed.", gatt, asyncTaskCallback);
            }
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor,
                                      int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                gatt.readCharacteristic(challengeCode);
            } else {
                abortBluetoothProcess("Failed to write notification descriptor.", gatt, asyncTaskCallback);
            }
        }

        @Override
        public void onCharacteristicRead(final BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (characteristic.getUuid().equals(BAAS_CHALLENGE_CODE_CHAR_UUID)) {
                    Integer challenge = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT16, 0);
                    Log.d(TAG, "Read new value from challenge code characteristic: " + challenge + ". Requesting answer code from server.");
                    AnswerCode.getAnswerCode(challenge, lock, new AsyncTaskCallback<AnswerCode>() {
                        @Override
                        public void success(AnswerCode answerCode) {
                            Log.d(TAG, "Obtained answer code from server: " + answerCode.getAnswerCode() + ".");
                            if (desiredState.equalsIgnoreCase(UNLOCKED_STATE)) {
                                unlockCommand.setValue(
                                        answerCode.getAnswerCode(),
                                        BluetoothGattCharacteristic.FORMAT_UINT16, 0);
                                gatt.writeCharacteristic(unlockCommand);
                            } else if (desiredState.equalsIgnoreCase(LOCKED_STATE)) {
                                lockCommand.setValue(
                                        answerCode.getAnswerCode(),
                                        BluetoothGattCharacteristic.FORMAT_UINT16, 0);
                                gatt.writeCharacteristic(lockCommand);
                            } else {
                                asyncTaskCallback.error(
                                        new ConnectException("Couldn't translate desired state into bluetooth command."));
                            }
                        }
                        @Override
                        public void error(Throwable e) {
                            abortBluetoothProcess("Failed to obtain answer code from server.", gatt, asyncTaskCallback);
                        }
                    });
                }
            } else {
                abortBluetoothProcess("Failed to read challenge code.", gatt, asyncTaskCallback);
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            if (characteristic.equals(lockState)) {
                String state = characteristic.getStringValue(0);
                Log.d(TAG, "NEW LOCK STATE: state. (desired state: " + desiredState + ")");
                if (state.equalsIgnoreCase(desiredState)) {
                    LockEvent.createLockEvent(bike, state, asyncTaskCallback);
                } else {
                    asyncTaskCallback.error(new ConnectException("Failed to set lock to the " + desiredState + " state."));
                }
            }
        }
    }

}
