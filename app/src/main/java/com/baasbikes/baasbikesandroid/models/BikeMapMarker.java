package com.baasbikes.baasbikesandroid.models;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.R;
import com.baasbikes.baasbikesandroid.views.MapLayout;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by jmolineaux on 4/3/16.
 */
public class BikeMapMarker {
    private static Bitmap availableBikeMarker;

    private Bike bike;
    private MapLayout assignedMap;
    private MarkerOptions markerOptions;

    static {
        Context appContext = BaasBikesApplication.getAppContext();
        int wPx = appContext.getResources().getDimensionPixelSize(R.dimen.available_marker_width);
        int hPx = appContext.getResources().getDimensionPixelSize(R.dimen.available_marker_height);
        Bitmap originalImage = BitmapFactory.decodeResource(appContext.getResources(), R.drawable.available_bike_marker);
        availableBikeMarker = Bitmap.createScaledBitmap(originalImage, wPx, hPx, true);
    }

    public BikeMapMarker(Bike bike) {
        this.bike = bike;
        this.markerOptions = new MarkerOptions();
        this.refresh();
    }

    public void refresh() {
        this.markerOptions.title(bike.getName());
        this.markerOptions.position(bike.getLocation());
        if (bike.isSelected()) { this.displaySelected(); }
        else if (bike.isAvailable()) { this.displayAvailable(); }
        else { this.hide(); }
    }

    public void refreshPosition() {
        this.markerOptions.position(bike.getLocation());
        this.updateDisplay();
    }

    public void displaySelected() {
        Context context = BaasBikesApplication.getAppContext();
        View view = LayoutInflater.from(context)
                .inflate(R.layout.layout_selected_bike_marker, null, false);
        ((TextView) view.findViewById(R.id.tv_bike_brand_name)).setText(bike.getBrand().getName());
        ((TextView) view.findViewById(R.id.tv_bike_name)).setText("'" + bike.getName() + "'");
        ((TextView) view.findViewById(R.id.tv_bike_size)).setText(bike.getSize().getName());

        Bitmap thumbnail = bike.getImage("thumbnail");
        if (thumbnail != null) {
            ((ImageView) view.findViewById(R.id.im_bike_icon)).setImageBitmap(thumbnail);
        }
        Bitmap bitmap = loadBitmapFromView(view);
        int wPx = context.getResources().getDimensionPixelSize(R.dimen.selected_marker_width);
        int hPx = context.getResources().getDimensionPixelSize(R.dimen.selected_marker_height);


        Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, wPx, hPx, true);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(scaledBitmap));
        this.markerOptions.visible(true);
        updateDisplay();
    }

    public void displayAvailable() {
        this.markerOptions.icon(BitmapDescriptorFactory.fromBitmap(availableBikeMarker));
        this.markerOptions.visible(true);
        updateDisplay();
    }

    public void hide() {
        this.markerOptions.visible(false);
        updateDisplay();
    }

    public void updateDisplay() {
        if (assignedMap != null) {
            assignedMap.updateMarkerDisplay(this);
        }
    }

    public void assignToMap(MapLayout googleMap) {
        this.assignedMap = googleMap;
        this.refresh();
    }

    public Bike getBike() {
        return this.bike;
    }

    public MarkerOptions getMarkerOptions() {
        return this.markerOptions;
    }

    private Bitmap loadBitmapFromView(View view) {
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        view.draw(canvas);
        return returnedBitmap;
    }

}
