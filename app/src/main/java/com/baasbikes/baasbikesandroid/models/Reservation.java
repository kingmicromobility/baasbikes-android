package com.baasbikes.baasbikesandroid.models;


import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.location.LocationService;
import com.baasbikes.baasbikesandroid.rest.APIException;
import com.baasbikes.baasbikesandroid.rest.BaasCoreRestApi;
import com.baasbikes.baasbikesandroid.rest.NoPaymentMethodException;
import com.baasbikes.baasbikesandroid.rest.converters.PostDeserializationProcessable;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by jmolineaux on 3/26/16.
 */
public class Reservation implements Serializable, PostDeserializationProcessable {

    private static String TAG = Reservation.class.getSimpleName();

    @Expose(serialize = false)
    private UUID guid;
    @Expose
    @SerializedName("bike")
    private UUID bike_guid;
    @Expose(serialize = false)
    @SerializedName("bike_object")
    private Bike bike;
    @Expose
    private Boolean canceled = false;
    @Expose
    private LatLng location;
    @Expose(serialize = false)
    private Market market;
    @Expose(serialize = false)
    private Date expiresAt;

    public Reservation(Bike bike) {
        this.bike = bike;
        this.bike_guid = bike.getGuid();
        this.market = bike.getMarket();
    }

    public UUID getGuid() {
        return this.guid;
    }

    public void setGuid(UUID guid) {
        this.guid = guid;
    }

    public Market getMarket() { return market; }

    public void setMarket(Market market) {
        this.market = market;
    }

    public Date getExpiresAt() {
        return this.expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }

    public Bike getBike() {
        return this.getMarket().getBikeMap().get(this.bike_guid);
    }

    public void begin(final AsyncTaskCallback<Reservation> asyncTaskCallback) {
        this.location = LocationService.getInstance().getCurrentLatLng();
        this.save(new AsyncTaskCallback<Reservation>() {
            @Override
            public void success(Reservation data) {
                ApplicationState.getInstance().setCurrentReservation(data);
                ApplicationState.getInstance().notifyObservers();
                asyncTaskCallback.success(data);
            }

            @Override
            public void error(Throwable e) {
                asyncTaskCallback.error(e);
            }
        });
    }

    public void beginSession(AsyncTaskCallback<Session> asyncTaskCallback) {
        Session session = new Session(this);
        session.begin(asyncTaskCallback);
    }

    public void save(final AsyncTaskCallback<Reservation> asyncTaskCallback) {
        final Reservation reservation = this;
        BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        if (guid == null) {
            api.createReservation(this).enqueue(new Callback<Reservation>() {
                @Override
                public void onResponse(Call<Reservation> c, Response<Reservation> response) {
                    if (response.code() == 201) {
                        Reservation newReservation = response.body();
                        reservation.setGuid(newReservation.getGuid());
                        reservation.setExpiresAt(newReservation.getExpiresAt());
                        asyncTaskCallback.success(reservation);
                    } else if (response.code() == 402) {
                        asyncTaskCallback.error(new NoPaymentMethodException("User has no payment methods."));
                    } else {
                        asyncTaskCallback.error(new APIException("Failed to create Reservation."));
                    }
                }
                @Override
                public void onFailure(Call<Reservation> c, Throwable t) {
                    asyncTaskCallback.error(t);
                }
            });
        } else {
            api.updateReservation(this.guid.toString(), this).enqueue(new Callback<Reservation>() {
                @Override
                public void onResponse(Call<Reservation> call, Response<Reservation> response) {
                    if (response.code() == 200) {
                        asyncTaskCallback.success(response.body());
                    } else {
                        asyncTaskCallback.error(new APIException("Failed to update Reservation."));
                    }
                }
                @Override
                public void onFailure(Call<Reservation> call, Throwable t) {
                    asyncTaskCallback.error(t);
                }
            });
        }
    }

    public void afterDeserialization() {
        this.market.getBikeMap().put(this.bike.getGuid(), this.bike);
        this.bike.setMarket(this.market);
    }

    public void cancel(AsyncTaskCallback<Reservation> asyncTaskCallback) {
        this.canceled = true;
        this.save(asyncTaskCallback);
    }

    public Long getTimeRemaining() {
        Date now = new Date();
        Long timeRemaining = (expiresAt.getTime() - now.getTime()) / 1000;
        ApplicationState appState = ApplicationState.getInstance();
        if (timeRemaining < 1 && appState.getCurrentSession() == null) {
            appState.setCurrentReservation(null);
            appState.notifyAppOfStateChange();
        }
        return timeRemaining;
    }

    public JSONObject getPropertiesAsJson() {
        JSONObject props = new JSONObject();
        try {
            props.put("reservation_lat", this.location.latitude);
            props.put("reservation_lng", this.location.longitude);
            props.put("reservation_canceled", this.canceled);
            props.put("reservation_expires_at", this.expiresAt);
            props.put("reservation_remaining_time", this.getTimeRemaining());
            JSONObject bikeProps = this.bike.getPropertiesAsJson();
            Iterator<String> keys = bikeProps.keys();
            while (keys.hasNext()) {
                String key = keys.next();
                props.put(key, bikeProps.get(key));
            }
        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
        }

        return props;
    }
}
