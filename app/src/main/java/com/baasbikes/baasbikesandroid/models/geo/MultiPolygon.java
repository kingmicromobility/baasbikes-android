package com.baasbikes.baasbikesandroid.models.geo;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.List;

/**
 * Created by jmolineaux on 4/2/16.
 */
public class MultiPolygon {

    private List<LatLng> polygonPoints;

    public MultiPolygon(List<LatLng> polygonPoints) {
        this.polygonPoints = polygonPoints;
    }

    public List<LatLng> getPolygonPoints() {
        return polygonPoints;
    }

    public LatLngBounds getRectangularBounds() {
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (LatLng point : this.getPolygonPoints()) {
            boundsBuilder.include(point);
        }
        return boundsBuilder.build();
    }

}
