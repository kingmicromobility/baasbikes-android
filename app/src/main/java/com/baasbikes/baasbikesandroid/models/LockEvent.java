package com.baasbikes.baasbikesandroid.models;

import android.content.Context;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.location.LocationService;
import com.baasbikes.baasbikesandroid.rest.APIException;
import com.baasbikes.baasbikesandroid.rest.BaasCoreRestApi;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;

import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LockEvent {

    private static final String UNLOCKED_EVENT_TYPE = "unlocked";
    private static final String LOCKED_EVENT_TYPE = "locked";

    @Expose
    private UUID guid;
    @Expose
    private UUID bike;
    @Expose
    private LatLng location;
    @Expose
    private String eventType;
    @Expose(serialize = false)
    private Trip trip;

    public static void createLockEvent(Bike bike, String eventType, AsyncTaskCallback<LockEvent> asyncTaskCallback){
        LatLng location = LocationService.getInstance().getCurrentLatLng();
        LockEvent lockEvent = new LockEvent(bike, location, eventType);
        lockEvent.save(asyncTaskCallback);
    }

    public LockEvent(Bike bike, LatLng location, String eventType) {
        this.bike = bike.getGuid();
        this.location = location;
        this.eventType = eventType;
    }

    public UUID getGuid() {
        return this.guid;
    }

    public Trip getTrip() {
        return this.trip;
    }

    public void save(final AsyncTaskCallback<LockEvent> asyncTaskCallback) {
        final BaasCoreRestApi api = BaasCoreRestApi.Service.getInstance();
        if (guid == null) {
            api.createLockEvent(this).enqueue(new Callback<LockEvent>() {
                @Override
                public void onResponse(Call<LockEvent> call, Response<LockEvent> response) {
                    if (response.code() == 201) {
                        asyncTaskCallback.success(response.body());
                    } else {
                        asyncTaskCallback.error(new APIException("Failed to create lock event."));
                    }
                }
                @Override
                public void onFailure(Call<LockEvent> call, Throwable t) {
                    asyncTaskCallback.error(t);
                }
            });
        }
    }
}
