package com.baasbikes.baasbikesandroid

import android.app.ProgressDialog
import android.content.Intent
import android.util.Log
import com.baasbikes.baasbikesandroid.models.PaymentMethod
import com.baasbikes.baasbikesandroid.models.Person
import com.baasbikes.baasbikesandroid.rest.BaasCoreRestApi
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback
import com.braintreepayments.api.BraintreeClient
import com.braintreepayments.api.Card
import com.braintreepayments.api.CardClient
import com.braintreepayments.api.CardNonce
import io.card.payment.CardIOActivity
import io.card.payment.CreditCard
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val SCAN_REQUEST_CODE = 500;

open class BasePaymentActivity : BaseActivity() {

    private lateinit var progressDialog: ProgressDialog

    protected fun startAddCardActivity() {
//        mixpanelAPI.track("account.payment.add")
        val scanIntent = Intent(this, CardIOActivity::class.java)
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_EXPIRY, true)
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_CVV, true)
        scanIntent.putExtra(CardIOActivity.EXTRA_REQUIRE_POSTAL_CODE, true)
        startActivityForResult(scanIntent, SCAN_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == SCAN_REQUEST_CODE) {
            data?.hasExtra(CardIOActivity.EXTRA_SCAN_RESULT)?.let {
                val scanResult: CreditCard? = data.getParcelableExtra(CardIOActivity.EXTRA_SCAN_RESULT)
                scanResult?.let {
                    if (it.isExpiryValid && it.cvv != null && it.postalCode != null) {
                        saveCardData(it)
                    } else showInvalidCardDataAlert()
                } ?: showInvalidCardDataAlert()
            } ?: showInvalidCardDataAlert()
        }
    }

    private fun saveCardData(creditCard: CreditCard) {
        progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Saving Payment Card...")
        progressDialog.show()

        BaasCoreRestApi.Service.getInstance().paymentGatewayClientToken
            .enqueue(object : Callback<Map<String?, String?>> {
                override fun onResponse(call: Call<Map<String?, String?>>, response: Response<Map<String?, String?>>) {
                    if (response.code() == 200) {
                        val btClientToken = response.body()["token"]
                        if (btClientToken != null) {
                            tokenizeCard(creditCard, btClientToken)
                        } else {
                            progressDialog.dismiss()
                            showPaymentGatewayErrorDialog()
                        }
                    } else {
                        progressDialog.dismiss()
                        showPaymentGatewayErrorDialog()
                    }
                }

                override fun onFailure(call: Call<Map<String?, String?>>, t: Throwable) {
                    progressDialog.dismiss()
                    showPaymentGatewayErrorDialog()
                }
            })
    }

    private fun tokenizeCard(creditCard: CreditCard, btClientToken: String) {
        val card = Card().apply {
            number = creditCard.cardNumber
            expirationMonth = creditCard.expiryMonth.toString()
            expirationYear = creditCard.expiryYear.toString()
            cvv = creditCard.cvv
            postalCode = creditCard.postalCode
        }

        val braintreeClient = BraintreeClient(this, btClientToken)
        val cardClient = CardClient(braintreeClient)
        cardClient.tokenize(card) { cardNonce, exception -> handleTokenizationResponse(cardNonce, exception) }
    }

    private fun handleTokenizationResponse(cardNonce: CardNonce?, exception: Exception?) {
        cardNonce?.let {
            createPaymentMethod(it)
        } ?: run {
            progressDialog.dismiss()
            showInvalidCardDataAlert()
        }
    }

    private fun createPaymentMethod(cardNonce: CardNonce) {
        val requestBody = hashMapOf("nonce" to cardNonce.string, "app_environment" to BuildConfig.APP_ENVIRONMENT)
        person = Person.getLoggedInPerson(BaasBikesApplication.getAppContext())
        BaasCoreRestApi.Service.getInstance().createPaymentMethodForPerson(person.guid.toString(), requestBody)
            .enqueue(object : Callback<PaymentMethod> {
                override fun onResponse(call: Call<PaymentMethod>, response: Response<PaymentMethod>) {
                    if (response.code() == 201) {
                        Log.d(TAG, "Payment Added Successfully.")
//                    mixpanelAPI.track("account.payment.added")
                        person.fetchPaymentMethod(object : AsyncTaskCallback<List<PaymentMethod>> {
                            override fun success(data: List<PaymentMethod>) {
                                Log.d(TAG, "Payment Updated in App Successfully.")
//                            mixpanelAPI.track("account.payment.added")
                                progressDialog.dismiss()
                                if(this@BasePaymentActivity is WelcomeActivity) {
                                    this@BasePaymentActivity.finish()
                                }
                            }

                            override fun error(e: Throwable) {
                                progressDialog.dismiss()
                                showPaymentGatewayErrorDialog()
                            }
                        })
                    } else {
                        progressDialog.dismiss()
                        showPaymentGatewayErrorDialog()
                    }
                }

                override fun onFailure(call: Call<PaymentMethod?>, t: Throwable) {
                    progressDialog.dismiss()
                    showPaymentGatewayErrorDialog()
                }
            })
    }

    private fun showInvalidCardDataAlert() {
//        mixpanelAPI.track("account.payment.add.error")
        showAlertDialog(R.string.alert_invalid_card_data_title, R.string.alert_invalid_card_data_message)
    }

    private fun showPaymentGatewayErrorDialog() {
//        mixpanelAPI.track("account.payment.add.error")
        showAlertDialog(R.string.alert_error_payment_gateway_title, R.string.alert_error_payment_gateway_message)
    }
}