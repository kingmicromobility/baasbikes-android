package com.baasbikes.baasbikesandroid;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.AttributionChannel;
import com.baasbikes.baasbikesandroid.models.Authentication;
import com.baasbikes.baasbikesandroid.models.Person;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class SignUpActivity extends BaseActivity {

    private static final String TAG = SignUpActivity.class.getSimpleName();
    private static final Integer TERMS_OF_SERVICE_REQUEST = 1;
    private static final Integer WELCOME_REQUEST = 2;
    private String autoReportedAttributionCode;
    private List<AttributionChannel> channels;

    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText emailEditText;
    private EditText passwordEditText;
    private EditText phoneEditText;
    private Spinner spinnerSelfReportedAttribution;
    private Button signUpButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        setupView();
//        getMixpanelAPI().track("account.signUp.viewed");
    }

    @Override
    public void setupView() {
        firstNameEditText = findViewById(R.id.et_first_name);
        lastNameEditText = findViewById(R.id.et_last_name);
        emailEditText = findViewById(R.id.et_email);
        passwordEditText = findViewById(R.id.et_password);
        phoneEditText = findViewById(R.id.et_phone);
        spinnerSelfReportedAttribution = findViewById(R.id.spinner_attribution_channels);
        signUpButton = findViewById(R.id.btn_sign_up);

        phoneEditText.addTextChangedListener(new PhoneNumberFormattingTextWatcher(Locale.US.getCountry()));
        signUpButton.setOnClickListener(v -> showTermsOfService());

        setupAttributionChannels();
    }

    private void setupAttributionChannels() {
        channels = ApplicationState.getInstance().getCurrentMarket().getAttributionChannels();
        AttributionSpinnerAdapter adapter = new AttributionSpinnerAdapter(this, channels);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerSelfReportedAttribution.setAdapter(adapter);
        spinnerSelfReportedAttribution.setSelection(determineAttributionChannelFromDeepLink());
    }

    private int determineAttributionChannelFromDeepLink() {
        int result = 0;

        Map<String, Object> linkData = Person.getDeepLinkData(BaasBikesApplication.getAppContext());
        if (linkData != null) {
            autoReportedAttributionCode = (String) linkData.get("attribution_code");
            if (autoReportedAttributionCode != null) {
                int tmpResult = 0;
                for (AttributionChannel channel : channels) {
                    if (channel.getAttributionCode().equalsIgnoreCase(autoReportedAttributionCode)) {
                        result = tmpResult;
                        break;
                    }
                    tmpResult++;
                }
            }
        }
        return result;
    }

    @Override
    public void onBackPressed() {
//        getMixpanelAPI().track("account.signUp.back");
        super.onBackPressed();
    }

    private void showTermsOfService() {
        if (firstNameEditText.getText().length() == 0) {
            showAlertDialog(R.string.alert_invalid_first_name_title, R.string.alert_invalid_first_name_message);
        } else if (lastNameEditText.getText().length() == 0) {
            showAlertDialog(R.string.alert_invalid_last_name_title, R.string.alert_invalid_last_name_message);
        } else if (emailEditText.getText().length() == 0) {
            showAlertDialog(R.string.alert_invalid_email_title, R.string.alert_invalid_email_message);
        } else if (passwordEditText.getText().length() < 6) {
            showAlertDialog(R.string.alert_invalid_password_title, R.string.alert_invalid_password_message);
        } else if (phoneEditText.getText().length() == 0) {
            showAlertDialog(R.string.alert_invalid_phone_title, R.string.alert_invalid_password_message);
        } else {
            AttributionChannel selfReportedChannel = (AttributionChannel) spinnerSelfReportedAttribution.getSelectedItem();
            person = new Person();
            person.setFirstName(firstNameEditText.getText().toString());
            person.setLastName(lastNameEditText.getText().toString());
            person.setEmail(emailEditText.getText().toString());
            person.setPassword(passwordEditText.getText().toString());
            person.setPhoneNumber(phoneEditText.getText().toString());
            person.setSelfReportedChannelCode(selfReportedChannel.getAttributionCode());
            person.setAutoReportedChannelCode(autoReportedAttributionCode);

            Intent intent = new Intent(this, TermsOfServiceActivity.class);
            this.startActivityForResult(intent, TERMS_OF_SERVICE_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TERMS_OF_SERVICE_REQUEST) {
            if (resultCode == TermsOfServiceActivity.RESULT_ACCEPTED) {
                signUp(person);
            }
        } else if (requestCode == WELCOME_REQUEST) {
            this.finish();
        }
    }

    private void signUp(final Person person) {
        showProgress(R.string.progress_creating_account);
        person.save(new AsyncTaskCallback<Person>() {
            @Override
            public void success(Person data) {
                dismissProgress();
                logIn(person);
            }

            @Override
            public void error(Throwable e) {
                dismissProgress();
                showAlertDialog(R.string.alert_creating_account_title,
                        getString(R.string.alert_creating_account_message, e.getMessage()));
            }
        });
    }

    private void logIn(final Person person) {
        showProgress(R.string.progress_logging_in);
        person.logIn(new AsyncTaskCallback<Authentication>() {
            @Override
            public void success(Authentication data) {
                dismissProgress();
                try {
                    JSONObject mpProps = new JSONObject();
                    mpProps.put("$first_name", person.getFirstName());
                    mpProps.put("$last_name", person.getLastName());
                    mpProps.put("$email", person.getEmail());
                    mpProps.put("$phone", person.getPhoneNumber());
                    mpProps.put("$created", new Date());
                    mpProps.put("original market", ApplicationState.getInstance().getCurrentMarket().getName());
                    if (person.getSelfReportedChannelCode() != null) {
                        mpProps.put("self_reported_attribution_code", person.getSelfReportedChannelCode());
                    }
                    if (person.getAutoReportedChannelCode() != null) {
                        mpProps.put("auto_reported_attribution_code", person.getAutoReportedChannelCode());
                    }
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                }

//                getMixpanelAPI().track("account.signUp.success");
//                getMixpanelAPI().alias(person.getEmail(), getMixpanelAPI().getDistinctId());
//                getMixpanelAPI().identify(person.getEmail());
//                getMixpanelAPI().getPeople().set(mpProps);
                ApplicationState.getInstance().restore();
                startActivityForResult(WelcomeActivity.class, WELCOME_REQUEST);
            }

            @Override
            public void error(Throwable e) {
                dismissProgress();
//                getMixpanelAPI().track("account.signUp.error");
                showAlertDialog(R.string.alert_logging_in_title, R.string.alert_logging_in_message);
            }
        });
    }

    //todo: Use SpinnerAdapter with OnItemSelectedListener
    private static class AttributionSpinnerAdapter extends ArrayAdapter<AttributionChannel> {

        private Context context;
        private List<AttributionChannel> attributionChannels;

        public AttributionSpinnerAdapter(Context context, List<AttributionChannel> attributionChannels) {
            super(context, R.layout.list_row_attribution_channel,
                    attributionChannels.toArray(new AttributionChannel[0]));
            this.context = context;
            this.attributionChannels = attributionChannels;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            AttributionChannel currentChannel = attributionChannels.get(position);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            ViewGroup rowView = (ViewGroup) inflater.inflate(R.layout.list_row_attribution_channel, parent, false);
            TextView channelName = rowView.findViewById(R.id.txt_channel_name);
            channelName.setText(currentChannel.getDescriptivePhrase());
            return rowView;
        }
    }
}
