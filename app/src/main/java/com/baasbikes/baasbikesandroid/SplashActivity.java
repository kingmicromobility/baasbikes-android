package com.baasbikes.baasbikesandroid;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;

import com.baasbikes.baasbikesandroid.location.LocationService;
import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.Person;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.baasbikes.baasbikesandroid.util.ConnectionUtils;
import com.google.android.gms.maps.MapsInitializer;
import com.zendesk.sdk.network.impl.ZendeskConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Observable;
import java.util.Observer;

import io.branch.referral.Branch;
import io.branch.referral.BranchError;

public class SplashActivity extends BaseActivity implements Observer {

    private static final String TAG = SplashActivity.class.getSimpleName();

    private Dialog locationAlertDialog;
    private ProgressBar progressBar;
    private TextView progressText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        setupView();
        MapsInitializer.initialize(getApplicationContext());
    }

    @Override
    public void setupView() {
        progressBar = findViewById(R.id.progress_bar);
        progressText = findViewById(R.id.tv_progress);

        progressBar.setIndeterminate(false);
        progressBar.setProgress(0);
        progressText.setText(R.string.progress_launching);
    }

    @Override
    public void onStart() {
        super.onStart();

        saveDeepLinkData();
        progressBar.setProgress(5);
        progressText.setText(R.string.progress_check_bluetooth);
        ConnectionUtils.requestBluetoothEnabled(this);
        progressBar.setProgress(10);
        progressText.setText(R.string.progress_initialize_zendesk);
        initializeZendesk();
        progressBar.setProgress(15);
        progressText.setText(R.string.progress_determine_location);
        ApplicationState.getInstance().addObserver(this);
    }

    private void saveDeepLinkData() {
        Branch.sessionBuilder(this)
                .withCallback(this::handleBranchSessionInit)
                .withData(getIntent().getData())
                .init();
    }

    private void handleBranchSessionInit(@Nullable JSONObject referringParams, @Nullable BranchError error) {
        if (referringParams != null) {
            Log.i("BranchConfigTest", "deep link data: " + referringParams.toString());
            Person.saveDeepLinkData(referringParams, BaasBikesApplication.getAppContext());
//            getMixpanelAPI().track("app.launch.withLink", referringParams);
        } else {
            if (error != null) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put(error.getClass().getName(), error.getErrorCode() + error.getMessage());
//                    getMixpanelAPI().track("app.launch.withLink", jsonObject);
                } catch (JSONException e) {
                    Log.d(TAG, e.getMessage());
                }
            } else {
//                getMixpanelAPI().track("app.launch.withLink", null);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!LocationService.getInstance().isConnected()) {
            connectToLocationService();
        } else {
            beginApplication();
        }
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof ApplicationState) {
            progressBar.setProgress(90);
            ApplicationState.getInstance().deleteObserver(this);
            beginApplication();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ConnectionUtils.LOCATION_FINE_ACCESS_PERMISSIONS_REQUEST) {
            Log.d(TAG, "Location fine access permission changed. Re-attempting location service connection.");
            connectToLocationService();
        } else if (requestCode == ConnectionUtils.BLUETOOTH_ENABLED_REQUEST) {
            Log.d(TAG, "Bluetooth status changed.");
        }
    }

    private void connectToLocationService() {
        boolean locationEnabled = ConnectionUtils.isLocationEnabled(this);
        boolean hasLocationPermission = ConnectionUtils.isLocationPermissionGranted(this);
        boolean showLocationPermissionRationale = ConnectionUtils.shouldShowLocationPermissionRationale(this);
        progressBar.setProgress(20);
        if (locationAlertDialog != null) {
            locationAlertDialog.dismiss();
        }
        if (locationEnabled && hasLocationPermission) {
            LocationService.connect(this, new AsyncTaskCallback<Void>() {
                @Override
                public void success(Void data) {
//                    getMixpanelAPI().track("permissions.location.allowed");
                    progressBar.setProgress(30);
                    progressText.setText(R.string.progress_download_bike_data);
                    ApplicationState.getInstance().restore();
                }

                @Override
                public void error(Throwable e) {
//                    getMixpanelAPI().track("permissions.location.denied");
                    if (e.getMessage().equalsIgnoreCase(LocationService.LOCATION_ERROR)) {
                        progressText.setText(R.string.progress_getting_location);
                    } else {
                        showLocationAlertDialog(R.string.alert_location_permission_title,
                                R.string.alert_location_permission_message);
                    }
                }
            });
        } else if (locationEnabled) {
            ConnectionUtils.requestLocationPermission(this);
        } else {
//            getMixpanelAPI().track("person.instructions.continue-showing.viewed");
            showLocationAlertDialog(R.string.alert_enable_location_title, R.string.alert_enable_location_message);
        }
    }

    private void showLocationAlertDialog(@StringRes int title, @StringRes int message) {
        locationAlertDialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(false)
                .show();
    }

    private void initializeZendesk() {
        String zendeskUrl = BuildConfig.ZENDESK_URL;
        String zendeskAppId = BuildConfig.ZENDESK_APP_ID;
        String zendeskClientId = BuildConfig.ZENDESK_CLIENT_ID;
        ZendeskConfig.INSTANCE.init(this, zendeskUrl, zendeskAppId, zendeskClientId);
    }

    private void beginApplication() {
        progressBar.setProgress(100);
//        getMixpanelAPI().track("app.launch.finished");
        navigateTo(MainActivity.class, true);
    }

    //todo: check if needed
    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.setIntent(intent);
    }
}
