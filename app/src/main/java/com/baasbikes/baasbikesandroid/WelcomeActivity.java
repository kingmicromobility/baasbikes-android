package com.baasbikes.baasbikesandroid;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.baasbikes.baasbikesandroid.models.Person;

public class WelcomeActivity extends BasePaymentActivity {

    private TextView tvNewUserName;
    private Button addPaymentCard;
    private TextView skipForNow;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        setupView();
    }

    @Override
    public void setupView() {
        tvNewUserName = findViewById(R.id.tv_new_user_name);
        addPaymentCard = findViewById(R.id.btn_add_payment_card);
        skipForNow = findViewById(R.id.btn_skip_for_now);

        person = Person.getLoggedInPerson(BaasBikesApplication.getAppContext());
        tvNewUserName.setText(person.getFirstName() + "!");
        addPaymentCard.setOnClickListener(v -> startAddCardActivity());
        skipForNow.setOnClickListener(v -> onSkipForNowPressed());
    }

    public void onSkipForNowPressed() {
//        getMixpanelAPI().track("person.welcome.skipped-payment");
        this.finish();
    }
}
