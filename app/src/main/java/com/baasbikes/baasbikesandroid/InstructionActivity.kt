package com.baasbikes.baasbikesandroid

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources

class InstructionActivity : BaseActivity() {

    private var allowBack: Boolean = true

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instruction)
        setupView()
    }

    override fun setupView() {
        val backgroundImageView = findViewById<ImageView>(R.id.instruction_background_img)
        val instructionTextView = findViewById<TextView>(R.id.instruction_text)
        val actionButton = findViewById<Button>(R.id.instruction_action_button)
        val intent = intent
        allowBack = intent.getBooleanExtra(EXTRA_ALLOW_BACK, true)
        @DrawableRes val bgDrawableId = intent.getIntExtra(EXTRA_BG_DRAWABLE, R.drawable.instructions_after_reserve)
        backgroundImageView.setImageDrawable(AppCompatResources.getDrawable(this, bgDrawableId))
        instructionTextView.text = intent.getStringExtra(EXTRA_INSTRUCTION)
        actionButton.text = intent.getStringExtra(EXTRA_ACTION_TEXT)
        actionButton.setOnClickListener { onActionButtonPressed() }
    }

    override fun onBackPressed() {
        if (allowBack) {
            this.setResult(RESULT_CANCELED)
            super.onBackPressed()
        }
    }

    private fun onActionButtonPressed() {
        this.setResult(RESULT_OK)
        finish()
    }

    companion object {
        const val EXTRA_BG_DRAWABLE = "bg_drawable"
        const val EXTRA_INSTRUCTION = "instruction"
        const val EXTRA_ACTION_TEXT = "action_text"
        const val EXTRA_ALLOW_BACK = "allow_back"
    }
}