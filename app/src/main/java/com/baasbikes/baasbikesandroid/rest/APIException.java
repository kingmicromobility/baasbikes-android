package com.baasbikes.baasbikesandroid.rest;

/**
 * Created by jmolineaux on 3/26/16.
 */
public class APIException extends Exception {

    public APIException(String msg) { super(msg); }

}
