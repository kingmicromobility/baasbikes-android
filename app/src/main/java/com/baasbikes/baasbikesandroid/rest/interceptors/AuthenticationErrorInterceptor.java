package com.baasbikes.baasbikesandroid.rest.interceptors;

import com.baasbikes.baasbikesandroid.rest.NeedToAuthenticateException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by jmolineaux on 4/4/16.
 */
public class AuthenticationErrorInterceptor implements Interceptor {

    private static final String TAG = AuthenticationErrorInterceptor.class.getSimpleName();

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);

        if (response.code() == 403) {
            throw new NeedToAuthenticateException("The API responded with status code 403, meaning the user must pass an Authentication Token header with this request.");
        }

        return response;
    }

}
