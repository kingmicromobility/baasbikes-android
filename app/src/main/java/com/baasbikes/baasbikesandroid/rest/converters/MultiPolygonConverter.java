package com.baasbikes.baasbikesandroid.rest.converters;

import com.baasbikes.baasbikesandroid.models.geo.MultiPolygon;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jmolineaux on 4/2/16.
 */
public class MultiPolygonConverter implements JsonDeserializer<MultiPolygon> {
    @Override
    public MultiPolygon deserialize(JsonElement jsonElement, Type typeOf,
                                    JsonDeserializationContext context) throws JsonParseException {
        Pattern multiPolygonPattern = Pattern.compile("MULTIPOLYGON");
        Matcher multiPolygonMatcher = multiPolygonPattern.matcher(jsonElement.getAsString());
        Pattern pointPattern = Pattern.compile("(([-\\d\\.]+) ([=\\d\\.]+))");
        Matcher pointMatcher = pointPattern.matcher(jsonElement.getAsString());
        List<LatLng> pointArray = new ArrayList<LatLng>();

        if (multiPolygonMatcher.find() && pointMatcher.find()) {
            do {
                String lat = pointMatcher.group(3);
                String lng = pointMatcher.group(2);
                LatLng point = new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
                pointArray.add(point);
            } while (pointMatcher.find());
        } else {
            throw new JsonParseException("Polygon string is not an EWKT Multi Polygon string.");
        }

        return new MultiPolygon(pointArray);
    }
}
