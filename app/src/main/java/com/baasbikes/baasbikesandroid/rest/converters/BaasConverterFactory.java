package com.baasbikes.baasbikesandroid.rest.converters;

import android.location.Location;

import com.baasbikes.baasbikesandroid.models.geo.MultiPolygon;
import com.baasbikes.baasbikesandroid.models.lock.Lock;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;

import retrofit2.Converter;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by jmolineaux on 4/2/16.
 */
public class BaasConverterFactory extends Converter.Factory {

    public static GsonConverterFactory create() {
        return GsonConverterFactory.create(getBaasGson());
    }

    public static Gson getBaasGson() {
        Type routeType = new TypeToken<List<Location>>(){}.getType();
        return new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .excludeFieldsWithoutExposeAnnotation()
                .registerTypeAdapterFactory(new SerializationHooksTypeAdapterFactory())
                .registerTypeAdapter(MultiPolygon.class, new MultiPolygonConverter())
                .registerTypeAdapter(LatLng.class, new LatLngConverter())
                .registerTypeAdapter(Lock.class, new LockConverter())
                .registerTypeAdapter(routeType, new RouteConverter())
                .create();
    }

}
