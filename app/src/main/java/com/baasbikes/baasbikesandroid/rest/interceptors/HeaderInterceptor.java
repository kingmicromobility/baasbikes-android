package com.baasbikes.baasbikesandroid.rest.interceptors;

import android.util.Log;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.models.Person;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Used to add headers to all outgoing requests at run-time.
 * @author Daniel Christopher
 */
public class HeaderInterceptor implements Interceptor {
    private static final String TAG = HeaderInterceptor.class.getSimpleName();

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();

        Person person = Person.getLoggedInPerson(BaasBikesApplication.getAppContext());

        Request.Builder builder = request.newBuilder()
                .header("Content-Type", "application/json")
                .method(request.method(), request.body());

        if (person != null && person.isLoggedIn()) {
            Log.d(TAG, "authorization token header: " + person.getToken());
            builder.header("Authorization", "Token " + person.getToken());
        }

        return chain.proceed(builder.build());
    }

}
