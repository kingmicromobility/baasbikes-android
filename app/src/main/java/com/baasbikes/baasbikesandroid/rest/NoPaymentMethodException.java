package com.baasbikes.baasbikesandroid.rest;

import java.io.IOException;

/**
 * Created by jmolineaux on 4/18/16.
 */
public class NoPaymentMethodException extends IOException {

    public NoPaymentMethodException(String msg) {
        super(msg);
    }
}
