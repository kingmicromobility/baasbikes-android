package com.baasbikes.baasbikesandroid.rest.converters;

/**
 * Created by jmolineaux on 4/2/16.
 */
public interface PostDeserializationProcessable {

    void afterDeserialization();

}
