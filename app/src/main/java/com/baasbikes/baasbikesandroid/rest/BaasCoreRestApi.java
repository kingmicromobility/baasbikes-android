package com.baasbikes.baasbikesandroid.rest;

import android.util.Log;

import com.baasbikes.baasbikesandroid.BuildConfig;
import com.baasbikes.baasbikesandroid.models.ApiResultPage;
import com.baasbikes.baasbikesandroid.models.Authentication;
import com.baasbikes.baasbikesandroid.models.BikeFeedbackEvent;
import com.baasbikes.baasbikesandroid.models.BikeFeedbackPromptSet;
import com.baasbikes.baasbikesandroid.models.LockEvent;
import com.baasbikes.baasbikesandroid.models.Market;
import com.baasbikes.baasbikesandroid.models.PaymentMethod;
import com.baasbikes.baasbikesandroid.models.Person;
import com.baasbikes.baasbikesandroid.models.Reservation;
import com.baasbikes.baasbikesandroid.models.Session;
import com.baasbikes.baasbikesandroid.models.Trip;
import com.baasbikes.baasbikesandroid.models.lock.AnswerCode;
import com.baasbikes.baasbikesandroid.rest.converters.BaasConverterFactory;
import com.baasbikes.baasbikesandroid.rest.interceptors.AuthenticationErrorInterceptor;
import com.baasbikes.baasbikesandroid.rest.interceptors.HeaderInterceptor;

import org.apache.http.conn.ssl.AllowAllHostnameVerifier;

import java.security.GeneralSecurityException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by jmolineaux on 3/26/16.
 */
public interface BaasCoreRestApi {

    @GET("/markets/")
    Call<ApiResultPage<Market>> markets();

    @GET("/markets/")
    Call<ApiResultPage<Market>> nearestMarket(
            @Query("lat") double lat,
            @Query("lon") double lon);

    @GET("/markets/{market_guid}/")
    Call<Market> getMarket(
            @Path("market_guid") String marketGuid);

    @GET("/markets/{market_guid}/contains/")
    Call<Map<String, String>> withinMarket(
            @Path("market_guid") String marketGuid,
            @Query("lat") double lat,
            @Query("lon") double lon);

    @GET("/people/{person_guid}/reservations/?status=active")
    Call<ApiResultPage<Reservation>> activeReservationsForPerson(
            @Path("person_guid") String personGuid);

    @GET("/people/{person_guid}/sessions/?status=active")
    Call<ApiResultPage<Session>> activeSessionsForPerson(
            @Path("person_guid") String personGuid);

    @GET("/people/{session_guid}/trips/?status=active")
    Call<ApiResultPage<Trip>> activeTripsForSession(
            @Path("session_guid") String sessionGuid);

    @POST("/people/")
    Call<Person> createPerson(@Body Person person);

    @PATCH("/people/{person_guid}/")
    Call<Person> updatePerson(
            @Path("person_guid") String personGuid,
            @Body Person person);

    @PATCH("/people/{person_guid}/")
    Call<Person> updatePersonAttributes(
            @Path("person_guid") String personGuid,
            @Body Map<String, Object> person);

    @GET("/people/{person_guid}/payment_methods/")
    Call<List<PaymentMethod>> getPaymentMethodsForPerson(
            @Path("person_guid") String personGuid);

    @POST("/people/{person_guid}/payment_methods/")
    Call<PaymentMethod> createPaymentMethodForPerson(
            @Path("person_guid") String personGuid,
            @Body Map<String, String> body);

    @POST("/reservations/")
    Call<Reservation> createReservation(
            @Body Reservation reservation);

    @PATCH("/reservations/{reservation_guid}/")
    Call<Reservation> updateReservation(
            @Path("reservation_guid") String reservationGuid,
            @Body Reservation reservation);

    @POST("/sessions/")
    Call<Session> createSession(
            @Body Session session);

    @PATCH("/sessions/{session_guid}/")
    Call<Session> updateSession(
            @Path("session_guid") String sessionGuid,
            @Body Session session);

    @POST("/trips/")
    Call<Trip> createTrip(
            @Body Trip trip);

    @PATCH("/trips/{trip_guid}/")
    Call<Trip> updateTrip(
            @Path("trip_guid") String tripGuid,
            @Body Trip trip);

    @POST("/lock_events/")
    Call<LockEvent> createLockEvent(
            @Body LockEvent lockEvent);

    @POST("/feedback_events/")
    Call<BikeFeedbackEvent> createBikeFeedbackEvent(
            @Body BikeFeedbackEvent bikeFeedback);

    @POST("/api-auth-token/")
    Call<Authentication> logIn(
            @Body Map<String, String> params);

    @POST("/temporary_password/")
    Call<Object> resetPassword(
            @Body Map<String, String> params);

    @POST("/answer_codes/")
    Call<AnswerCode> getAnswerCode(
            @Body Map<String, Object> params);

    @GET("/feedback_prompt_sets/")
    Call<ApiResultPage<BikeFeedbackPromptSet>> getFeedbackPromptSets(
            @Query("name") String name);

    @GET("/payments/client/")
    Call<Map<String, String>> getPaymentGatewayClientToken();

    class Service {
        private static final String TAG = Service.class.getSimpleName();
        private static BaasCoreRestApi instance;

        public static BaasCoreRestApi getInstance() {
            if (instance == null) {
                try {
                    instance = new Retrofit.Builder()
                            .baseUrl(BuildConfig.BAAS_API_URL)
                            .addConverterFactory(BaasConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(createHttpClient())
                            .build()
                            .create(BaasCoreRestApi.class);
                } catch (GeneralSecurityException e) {
                    Log.e(TAG, Log.getStackTraceString(e));
                }
            }
            return instance;
        }

        private static OkHttpClient createHttpClient() throws GeneralSecurityException {
            X509TrustManager trustManager = new X509TrustManager() {
                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            };
            TrustManager[] trustManagers = {trustManager};
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustManagers, null);

            HostnameVerifier hostnameVerifier = (hostname, session) -> true;

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            return new OkHttpClient.Builder()
                    .sslSocketFactory(sslContext.getSocketFactory(), trustManager)
                    .hostnameVerifier(new AllowAllHostnameVerifier())
                    .addInterceptor(new HeaderInterceptor())
                    .addInterceptor(loggingInterceptor)
                    .addInterceptor(new AuthenticationErrorInterceptor())
                    .build();
        }
    }
}
