package com.baasbikes.baasbikesandroid.rest.converters;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jmolineaux on 4/2/16.
 */
public class LatLngConverter implements JsonSerializer<LatLng>, JsonDeserializer<LatLng> {

    @Override
    public JsonElement serialize(LatLng latLng, Type typeOf, JsonSerializationContext context) {
        StringBuilder stringBuilder = new StringBuilder("SRID=4326;POINT (")
                .append(latLng.longitude)
                .append(" ")
                .append(latLng.latitude)
                .append(")");
        String latLngString = stringBuilder.toString();
        return new JsonPrimitive(latLngString);
    }

    @Override
    public LatLng deserialize(JsonElement jsonElement, Type typeOf,
                              JsonDeserializationContext context) throws JsonParseException {
        Pattern pointPattern = Pattern.compile("POINT \\((([-\\d\\.]+) ([=\\d\\.]+))\\)");
        Matcher pointMatcher = pointPattern.matcher(jsonElement.getAsString());
        LatLng point;

        if (pointMatcher.find()) {
            Double lat = Double.parseDouble(pointMatcher.group(3));
            Double lng = Double.parseDouble(pointMatcher.group(2));
            point = new LatLng(lat, lng);
        } else {
            throw new JsonParseException("Point string is not an EWKT Point string.");
        }

        return point;
    }

}
