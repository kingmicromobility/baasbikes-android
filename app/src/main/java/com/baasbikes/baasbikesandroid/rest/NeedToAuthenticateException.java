package com.baasbikes.baasbikesandroid.rest;

import java.io.IOException;

/**
 * Created by jmolineaux on 4/4/16.
 */
public class NeedToAuthenticateException extends IOException {

    public NeedToAuthenticateException(String msg) { super(msg); }

}
