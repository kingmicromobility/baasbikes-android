package com.baasbikes.baasbikesandroid.rest.interceptors;

import android.util.Log;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import java.io.IOException;

import okio.Buffer;

/**
 * Intercepts all retrofit activity to log requests and responses.
 * @author Daniel Christopher
 */
public class LoggingInterceptor implements Interceptor {
    public static final String TAG = LoggingInterceptor.class.getSimpleName();

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Response response = chain.proceed(request);

        Request loggableRequest = request.newBuilder().build();
        if (loggableRequest.body() != null) {
            try {
                Buffer requestBodyBuffer = new Buffer();
                loggableRequest.body().writeTo(requestBodyBuffer);
                String loggableRequestBody = requestBodyBuffer.readUtf8();
                Log.d(TAG, "Request: " + loggableRequestBody);
            } catch (IOException e) {
                Log.e(TAG, "Failed to log request: " + e.getMessage());
            }
        }

        Response loggableResponse = response.newBuilder().build();
        ResponseBody loggableResponseBody = loggableResponse.body();
        if (loggableResponseBody != null) {
            String loggableResponseBodyString = loggableResponseBody.string();
            Log.d(TAG, "Response: " + loggableResponseBodyString);
        }

        return response;
    }
}
