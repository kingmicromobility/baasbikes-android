package com.baasbikes.baasbikesandroid.rest.converters;

import com.baasbikes.baasbikesandroid.models.lock.Lock;
import com.baasbikes.baasbikesandroid.models.lock.LockFactory;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jmolineaux on 4/5/16.
 */
public class LockConverter implements JsonDeserializer<Lock> {

    @Override
    public Lock deserialize(JsonElement jsonElement, Type typeOf,
                            JsonDeserializationContext context) throws JsonParseException {
        Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        Map<String, String> lockProperties = new Gson().fromJson(jsonElement, type);
        return LockFactory.createLock(lockProperties.get("urn"));
    }

}
