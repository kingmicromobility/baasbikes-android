package com.baasbikes.baasbikesandroid.rest.converters;

import android.location.Location;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jmolineaux on 8/4/16.
 */
public class RouteConverter implements JsonSerializer<List<Location>>, JsonDeserializer<List<Location>> {

    @Override
    public JsonElement serialize(List<Location> route, Type typeOf, JsonSerializationContext context) {
        StringBuilder stringBuilder = new StringBuilder("");
        for (Location location : route) {
            stringBuilder
                    .append(location.getBearing())
                    .append(",")
                    .append(location.getLatitude())
                    .append(",")
                    .append(location.getLongitude())
                    .append(",")
                    .append(location.getAltitude())
                    .append(",")
                    .append(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ").format(new Date(location.getTime())))
                    .append("\n");
        }
        String routeString = stringBuilder.toString();
        return new JsonPrimitive(routeString);
    }

    @Override
    public List<Location> deserialize(JsonElement jsonElement, Type typeOf, JsonDeserializationContext context) throws JsonParseException {
        List<Location> route = new ArrayList<>();
        Pattern locationPattern = Pattern.compile("([\\d\\.-]+),([\\d\\.-]+),([\\d\\.-]+),([\\d\\.\\-]+),([\\dT:\\.-]+)\\n");
        Matcher locationMatcher = locationPattern.matcher(jsonElement.getAsString());

        while (locationMatcher.find()) {
            Location location = new Location("");
            try {
                location.setBearing(Float.parseFloat(locationMatcher.group(1)));
                location.setLatitude(Double.parseDouble(locationMatcher.group(2)));
                location.setLongitude(Double.parseDouble(locationMatcher.group(3)));
                location.setAltitude(Double.parseDouble(locationMatcher.group(4)));
                location.setTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ").parse(locationMatcher.group(5)).getTime());
            } catch (ParseException e) {
                throw new JsonParseException("Failed to parse date.", e);
            }
            route.add(location);
        }

        return route;
    }

}
