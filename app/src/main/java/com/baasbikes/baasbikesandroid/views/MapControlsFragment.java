package com.baasbikes.baasbikesandroid.views;

import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.R;
import com.baasbikes.baasbikesandroid.location.LocationService;
import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.Market;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MapControlsFragment extends Fragment {

    private static String TAG = MapControlsFragment.class.getSimpleName();
    private MapLayout mapFragment;
    private View view;
    private FloatingActionButton goToMarketBounds;
    private FloatingActionButton goToUserLocation;

//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setRetainInstance(true);
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_map_controls, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setupView();
    }

    private void setupView() {
        goToMarketBounds = view.findViewById(R.id.fab_go_to_market_bounds);
        goToUserLocation = view.findViewById(R.id.fab_go_to_user_location);

        goToMarketBounds.setOnClickListener(v -> goToMarketBounds());
        goToUserLocation.setOnClickListener(v -> goToUserLocation());
    }

    public void goToMarketBounds() {
        mapFragment.animateToMarketBounds();
//        Market market = ApplicationState.getInstance().getCurrentMarket();
//        if (market != null) {
//            BaasBikesApplication.getMixpanel().track("map.market.centered", market.getPropertiesAsJson());
//        }
    }

    public void goToUserLocation() {
        mapFragment.animateToCurrentLocation();
//        LocationService locationService = LocationService.getInstance();
//        JSONObject mpProps = new JSONObject();
//        try {
//            mpProps.put("lat", locationService.getCurrentLocation().getLatitude());
//            mpProps.put("lng", locationService.getCurrentLocation().getLongitude());
//        } catch (JSONException e) {
//            Log.d(TAG, e.getMessage());
//        }
//        BaasBikesApplication.getMixpanel().track("map.user.centered", mpProps);
    }

    public void setMarketBoundsFabActive() {
        goToMarketBounds.setImageResource(R.drawable.fab_go_to_market);
    }

    public void setMarketBoundsFabInactive() {
        goToMarketBounds.setImageResource(R.drawable.fab_go_to_market_inactive);
    }

    public void setUserLocationFabActive() {
        goToUserLocation.setImageResource(R.drawable.fab_go_to_user_location);
    }

    public void setUserLocationFabInactive() {
        goToUserLocation.setImageResource(R.drawable.fab_go_to_user_location_inactive);
    }

    public void setMapFragment(MapLayout mapFragment) {
        this.mapFragment = mapFragment;
    }
}
