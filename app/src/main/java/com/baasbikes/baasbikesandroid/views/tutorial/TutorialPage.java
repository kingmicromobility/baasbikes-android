package com.baasbikes.baasbikesandroid.views.tutorial;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.baasbikes.baasbikesandroid.R;

/**
 * Created by jmolineaux on 4/4/16.
 */
public class TutorialPage extends Fragment {

    private static final String ARG_PAGE_TITLE = "page_title";
    private static final String ARG_PAGE_BG = "page_bg";

    private String pageTitle;
    private Drawable bgImage;

    public static TutorialPage create(String pageTitle, Integer bgDrawableId) {
        TutorialPage fragment = new TutorialPage();
        Bundle args = new Bundle();
        args.putString(ARG_PAGE_TITLE, pageTitle);
        args.putInt(ARG_PAGE_BG, bgDrawableId);
        fragment.setArguments(args);
        return fragment;
    }

    public TutorialPage() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.pageTitle = getArguments().getString(ARG_PAGE_TITLE);
        this.bgImage = getContext().getResources().getDrawable(getArguments().getInt(ARG_PAGE_BG));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_tutorial_page, container, false);
        view.setBackground(this.bgImage);
        ((TextView) view.findViewById(R.id.tutorial_page_title)).setText(this.pageTitle);
        return view;
    }
}
