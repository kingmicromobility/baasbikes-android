package com.baasbikes.baasbikesandroid.views.rentalcontrols;

import androidx.fragment.app.Fragment;

import com.baasbikes.baasbikesandroid.models.ApplicationState;

public class RentalControlsFactory {

    private final RentalControlsUpdater updater;
    private final AuthHandler authHandler;

    public RentalControlsFactory(RentalControlsUpdater updater, AuthHandler authHandler) {
        this.updater = updater;
        this.authHandler = authHandler;
    }

    public Fragment createRentalControlsFragment() {
        RentalControlsFragment rentalControlsFragment;
        switch (ApplicationState.getInstance().getRentalState()) {
            case BIKE_SELECTED:
                rentalControlsFragment = new BikeSelectedControlsFragment();
                break;
            case BIKE_RESERVED:
                rentalControlsFragment = new BikeReservedControlsFragment();
                break;
            case TRIP_IN_PROGRESS:
                rentalControlsFragment = new TripInProgressControlsFragment();
                break;
            case BIKE_HELD:
                rentalControlsFragment = new BikeHeldControlsFragment();
                break;
            case AWAITING_BIKE_FEEDBACK:
                rentalControlsFragment = new BikeFeedbackControlsFragment();
                break;
            default:
                rentalControlsFragment = null;
                break;
        }

        if (rentalControlsFragment != null) {
            rentalControlsFragment.setUpdater(updater);
            rentalControlsFragment.setAuthHandler(authHandler);
        }
        return rentalControlsFragment;
    }
}
