package com.baasbikes.baasbikesandroid.views.rentalcontrols;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.InstructionActivity;
import com.baasbikes.baasbikesandroid.R;
import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.Person;
import com.baasbikes.baasbikesandroid.models.Session;
import com.baasbikes.baasbikesandroid.util.AsyncBoundaryCallback;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Timer;
import java.util.TimerTask;

public class BikeHeldControlsFragment extends RentalControlsFragment {

    public static final Integer BEFORE_RESUME_INSTRUCTION_ACTIVITY_RESULT = 400;
    private static final String TAG = BikeHeldControlsFragment.class.getSimpleName();
    private Timer updateTimer;

    private TextView tvSessionDuration;
    private TextView tvSessionPrice;
    private FloatingActionButton endSession;
    private FloatingActionButton resumeSession;

    public BikeHeldControlsFragment() {
        super(R.layout.fragment_rental_bike_held_controls);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    @Override
    public void setupView() {
        super.setupView();
        tvSessionDuration = getView().findViewById(R.id.tv_session_duration);
        tvSessionPrice = getView().findViewById(R.id.tv_session_price);
        endSession = getView().findViewById(R.id.fab_end_session);
        resumeSession = getView().findViewById(R.id.fab_resume_session);

        endSession.setOnClickListener(v -> endSession());
        resumeSession.setOnClickListener(v -> resumeSession());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (this.updateTimer != null) {
            this.updateTimer.cancel();
            this.updateTimer = null;
        }
        this.updateTimer = new Timer();
        this.updateTimer.schedule(new UpdateScreenTask(), 0, 1000);
    }

    @Override
    public void onStop() {
        super.onStop();
        this.updateTimer.cancel();
        this.updateTimer = null;
    }

    public void endSession() {
        final ProgressDialog progress = showProgressDialog("Ending Rental...");
        final Session thisSession = ApplicationState.getInstance().getCurrentSession();
        thisSession.end(new AsyncBoundaryCallback<Session>() {
            @Override
            public void insideBounds(Session data) {
                progress.dismiss();
//                Session session = ApplicationState.getInstance().getCurrentSession();
//                MixpanelAPI mixpanel = BaasBikesApplication.getMixpanel();
//                mixpanel.track("session.hold.ended", session.getPropertiesAsJson());
//                mixpanel.track("session.ended", session.getPropertiesAsJson());
//                mixpanel.getPeople().increment("total spent", session.getPrice());
            }

            @Override
            public void outsideBounds(Session data) {
                progress.dismiss();
//                BaasBikesApplication.getMixpanel().track("session.end.boundsFeeMessage.viewed", data.getPropertiesAsJson());
                new AlertDialog.Builder(getActivity())
                        .setTitle("Out Of Bounds")
                        .setMessage("Ending a session outside of the boundaries will result in a $25 fee. Are you sure?")
                        .setPositiveButton("Accept Fee", (dialog, which) -> {
                            thisSession.setEndOutsideMarket(true);
                            BikeHeldControlsFragment.this.endSession();
                        })
                        .setNegativeButton("Cancel", (dialog, which) -> dialog.cancel())
                        .show();
            }

            @Override
            public void error(Throwable e) {
                progress.dismiss();
                Log.e(TAG, e.getMessage());
            }
        });
    }

    public void resumeSession() {
        if (Person.getLoggedInPerson(BaasBikesApplication.getAppContext()).shouldSeeInstructions()) {
            Intent intent = new Intent(this.getActivity(), InstructionActivity.class);
            intent.putExtra(InstructionActivity.EXTRA_BG_DRAWABLE, R.drawable.instructions_before_unlock_seat_post);
            intent.putExtra(InstructionActivity.EXTRA_INSTRUCTION, "Push button on smart lock to wake it up");
            intent.putExtra(InstructionActivity.EXTRA_ACTION_TEXT, "UNLOCK");
            intent.putExtra(InstructionActivity.EXTRA_ALLOW_BACK, true);
            this.startActivityForResult(intent, BEFORE_RESUME_INSTRUCTION_ACTIVITY_RESULT);
//            BaasBikesApplication.getMixpanel().track("session.resume.before.instructions.viewed");
        } else {
            performResume();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BEFORE_RESUME_INSTRUCTION_ACTIVITY_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                performResume();
            } else {
//                BaasBikesApplication.getMixpanel().track("session.resume.before.instructions.back");
            }
        }
    }

    private void performResume() {
        final ProgressDialog progress = showProgressDialog("Unlocking...");
        ApplicationState.getInstance().getCurrentSession().resume(new AsyncTaskCallback<Session>() {
            @Override
            public void success(Session data) {
                progress.dismiss();
//                Session session = ApplicationState.getInstance().getCurrentSession();
//                MixpanelAPI mixpanel = BaasBikesApplication.getMixpanel();
//                mixpanel.track("bike.unlock.success", session.getBike().getPropertiesAsJson());
//                mixpanel.track("session.unlock.success", session.getPropertiesAsJson());
//                mixpanel.track("session.trip.started", session.getPropertiesAsJson());
//                mixpanel.timeEvent("session.trip.ended");
//                mixpanel.getPeople().increment("trip count", 1);
                updateControls();
//                if (Person.getLoggedInPerson(BaasBikesApplication.getAppContext()).shouldSeeInstructions()) {
//                    Intent intent = new Intent(getActivity(), InstructionActivity.class);
//                    intent.putExtra(InstructionActivity.EXTRA_BG_DRAWABLE, R.drawable.instructions_after_unlock_seat_post);
//                    intent.putExtra(InstructionActivity.EXTRA_INSTRUCTION,
//                            "Pull chain out and leave it at the bike rack. Take the bike for a test ride!");
//                    intent.putExtra(InstructionActivity.EXTRA_ACTION_TEXT, "CONTINUE");
//                    intent.putExtra(InstructionActivity.EXTRA_ALLOW_BACK, false);
//                    startActivity(intent);
////                    BaasBikesApplication.getMixpanel().track("session.resume.after.instructions.viewed");
//                }
            }

            @Override
            public void error(Throwable e) {
                progress.dismiss();
//                BaasBikesApplication.getMixpanel().track("bike.unlock.fail",
//                        ApplicationState.getInstance().getCurrentBike().getPropertiesAsJson());
                Log.e(TAG, e.getMessage());
            }
        });
    }

    private class UpdateScreenTask extends TimerTask {
        @Override
        public void run() {
            getActivity().runOnUiThread(() -> {
                Session session = ApplicationState.getInstance().getCurrentSession();
                Long duration = session.getDuration();
                Long hours = duration / 3600;
                Long minutes = (duration % 3600) / 60;
                String durationString;
                if (hours == 1 && minutes == 1) {
                    durationString = String.format("Rental Time: %d hour %d minute", hours, minutes);
                } else if (hours == 1 && minutes != 1) {
                    durationString = String.format("Rental Time: %d hour %d minutes", hours, minutes);
                } else if (hours != 1 && minutes == 1) {
                    durationString = String.format("Rental Time: %d hours %d minute", hours, minutes);
                } else {
                    durationString = String.format("Rental Time: %d hours %d minutes", hours, minutes);
                }
                tvSessionDuration.setText(durationString);

                Long price = session.getPrice();
                String priceString = String.format("Price: $%d.%02d", price / 100, price % 100);
                tvSessionPrice.setText(priceString);
            });
        }
    }
}
