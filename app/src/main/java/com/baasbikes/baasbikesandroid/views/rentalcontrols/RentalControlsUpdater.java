package com.baasbikes.baasbikesandroid.views.rentalcontrols;

import android.app.ProgressDialog;

/**
 * Created by jmolineaux on 4/4/16.
 */
public interface RentalControlsUpdater {

    void updateRentalControls();

    ProgressDialog showProgressDialog(String message);

}
