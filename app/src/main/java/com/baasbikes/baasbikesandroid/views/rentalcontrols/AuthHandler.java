package com.baasbikes.baasbikesandroid.views.rentalcontrols;

/**
 * Created by jmolineaux on 4/4/16.
 */
public interface AuthHandler {

    void authenticateUser();
    void promptUserToAddPaymentMethod();
    void askUserIfAppShouldShowInstructions();
}
