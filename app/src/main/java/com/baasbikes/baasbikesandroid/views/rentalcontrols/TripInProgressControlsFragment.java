package com.baasbikes.baasbikesandroid.views.rentalcontrols;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.InstructionActivity;
import com.baasbikes.baasbikesandroid.R;
import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.Market;
import com.baasbikes.baasbikesandroid.models.Person;
import com.baasbikes.baasbikesandroid.models.Session;
import com.baasbikes.baasbikesandroid.util.AsyncBoundaryCallback;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class TripInProgressControlsFragment extends RentalControlsFragment {

    private static final Integer ALIGN_SEAT_INSTRUCTION_ACTIVITY_RESULT = 300;
    private static final Integer BEFORE_LOCK_INSTRUCTION_ACTIVITY_RESULT = 301;
    private static final String TAG = BikeReservedControlsFragment.class.getSimpleName();
    private Timer updateTimer;
    private TextView tvSessionDuration;
    private TextView tvSessionPrice;
    private FloatingActionButton lockButton;

    public TripInProgressControlsFragment() {
        super(R.layout.fragment_rental_trip_in_progress_controls);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    @Override
    public void setupView() {
        super.setupView();
        tvSessionDuration = getView().findViewById(R.id.tv_session_duration);
        tvSessionPrice = getView().findViewById(R.id.tv_session_price);
        lockButton = getView().findViewById(R.id.fab_lock_bike);

        lockButton.setOnClickListener(v -> lockBike());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (this.updateTimer != null) {
            this.updateTimer.cancel();
            this.updateTimer = null;
        }
        this.updateTimer = new Timer();
        this.updateTimer.schedule(new UpdateScreenTask(), 0, 1000);
    }

    @Override
    public void onStop() {
        super.onStop();
        this.updateTimer.cancel();
        this.updateTimer = null;
    }

    private void lockBike() {
        if (Person.getLoggedInPerson(BaasBikesApplication.getAppContext()).shouldSeeInstructions()) {
            presentBeforeLockInstruction();
//            Intent intent = new Intent(getActivity(), InstructionActivity.class);
//            intent.putExtra(InstructionActivity.EXTRA_BG_DRAWABLE, R.drawable.instructions_align_lock_seat_post);
//            intent.putExtra(InstructionActivity.EXTRA_INSTRUCTION, "Align the lock. Lower seat fully and rotate 90° clockwise.");
//            intent.putExtra(InstructionActivity.EXTRA_ACTION_TEXT, "CONTINUE");
//            intent.putExtra(InstructionActivity.EXTRA_ALLOW_BACK, true);
//            startActivityForResult(intent, ALIGN_SEAT_INSTRUCTION_ACTIVITY_RESULT);
//            BaasBikesApplication.getMixpanel().track("session.lock.before.instructions.step-one.viewed");
        } else {
            performLock();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ALIGN_SEAT_INSTRUCTION_ACTIVITY_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                Intent intent = new Intent(this.getActivity(), InstructionActivity.class);
                intent.putExtra(InstructionActivity.EXTRA_BG_DRAWABLE, R.drawable.instructions_before_lock_seat_post);
                intent.putExtra(InstructionActivity.EXTRA_INSTRUCTION, "Insert chain and press button to wake lock.");
                intent.putExtra(InstructionActivity.EXTRA_ACTION_TEXT, "LOCK BIKE");
                intent.putExtra(InstructionActivity.EXTRA_ALLOW_BACK, true);
                this.startActivityForResult(intent, BEFORE_LOCK_INSTRUCTION_ACTIVITY_RESULT);
//                BaasBikesApplication.getMixpanel().track("session.lock.before.instructions.step-two.viewed");
            } else {
//                BaasBikesApplication.getMixpanel().track("session.lock.before.instructions.step-one.back");
            }
        } else if (requestCode == BEFORE_LOCK_INSTRUCTION_ACTIVITY_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                performLock();
            } else {
//                BaasBikesApplication.getMixpanel().track("session.lock.before.instructions.step-two.back");
            }
        }
    }

    public void presentBeforeLockInstruction() {
        Intent intent = new Intent(this.getActivity(), InstructionActivity.class);
        intent.putExtra(InstructionActivity.EXTRA_BG_DRAWABLE, R.drawable.instructions_before_lock_seat_post);
        intent.putExtra(InstructionActivity.EXTRA_INSTRUCTION, "Insert chain and press button to wake lock.");
        intent.putExtra(InstructionActivity.EXTRA_ACTION_TEXT, "LOCK BIKE");
        intent.putExtra(InstructionActivity.EXTRA_ALLOW_BACK, true);
        this.startActivityForResult(intent, BEFORE_LOCK_INSTRUCTION_ACTIVITY_RESULT);
//        BaasBikesApplication.getMixpanel().track("session.lock.before.instructions.step-two.viewed");
    }

    public void performLock() {
        final ProgressDialog progress = showProgressDialog("Locking...");
        ApplicationState.getInstance().getCurrentSession().hold(new AsyncBoundaryCallback<Market>() {
            @Override
            public void insideBounds(Market data) {
                progress.dismiss();
//                ApplicationState appState = ApplicationState.getInstance();
//                MixpanelAPI mixpanel = BaasBikesApplication.getMixpanel();
//                mixpanel.track("bike.lock.success", appState.getCurrentBike().getPropertiesAsJson());
//                mixpanel.track("session.lock.success", appState.getCurrentSession().getPropertiesAsJson());
//                mixpanel.track("session.trip.ended", appState.getCurrentSession().getPropertiesAsJson());
//                mixpanel.track("session.hold.started", appState.getCurrentSession().getPropertiesAsJson());
//                mixpanel.timeEvent("session.hold.ended");
//                mixpanel.getPeople().increment("hold count", 1);
                askUserIfAppShouldShowInstructions();
            }

            @Override
            public void outsideBounds(Market data) {
                progress.dismiss();
//                ApplicationState appState = ApplicationState.getInstance();
//                MixpanelAPI mixpanel = BaasBikesApplication.getMixpanel();
//                mixpanel.track("bike.lock.success", appState.getCurrentBike().getPropertiesAsJson());
//                mixpanel.track("session.lock.success", appState.getCurrentSession().getPropertiesAsJson());
//                mixpanel.track("session.trip.ended", appState.getCurrentSession().getPropertiesAsJson());
//                mixpanel.track("session.hold.started", appState.getCurrentSession().getPropertiesAsJson());
//                mixpanel.timeEvent("session.hold.ended");
//                mixpanel.getPeople().increment("hold count", 1);
            }

            @Override
            public void error(Throwable e) {
                progress.dismiss();
//                ApplicationState appState = ApplicationState.getInstance();
//                BaasBikesApplication.getMixpanel().track("bike.lock.fail", appState.getCurrentBike().getPropertiesAsJson());
//                BaasBikesApplication.getMixpanel().track("session.lock.fail", appState.getCurrentSession().getPropertiesAsJson());
                Log.d(TAG, e.getMessage());
            }
        });
    }

    private class UpdateScreenTask extends TimerTask {
        @Override
        public void run() {
            getActivity().runOnUiThread(() -> {
                Session session = ApplicationState.getInstance().getCurrentSession();
                Long duration = session.getDuration();
                Long hours = duration / 3600;
                Long minutes = (duration % 3600) / 60;
                String durationString;
                if (hours == 1 && minutes == 1) {
                    durationString =
                            String.format(Locale.getDefault(), "Rental Time: %d hour %d minute", hours, minutes);
                } else if (hours == 1 && minutes != 1) {
                    durationString =
                            String.format(Locale.getDefault(), "Rental Time: %d hour %d minutes", hours, minutes);
                } else if (hours != 1 && minutes == 1) {
                    durationString =
                            String.format(Locale.getDefault(), "Rental Time: %d hours %d minute", hours, minutes);
                } else {
                    durationString =
                            String.format(Locale.getDefault(), "Rental Time: %d hours %d minutes", hours, minutes);
                }
                tvSessionDuration.setText(durationString);

                Long price = session.getPrice();
                String priceString =
                        String.format(Locale.getDefault(), "Price: $%d.%02d", price / 100, price % 100);
                tvSessionPrice.setText(priceString);
            });
        }
    }
}
