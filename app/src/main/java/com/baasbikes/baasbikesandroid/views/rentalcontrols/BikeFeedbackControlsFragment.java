package com.baasbikes.baasbikesandroid.views.rentalcontrols;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.baasbikes.baasbikesandroid.BikeFeedbackActivity;
import com.baasbikes.baasbikesandroid.R;
import com.baasbikes.baasbikesandroid.location.LocationService;
import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.BikeFeedbackEvent;
import com.baasbikes.baasbikesandroid.models.BikeFeedbackPromptSet;
import com.baasbikes.baasbikesandroid.models.Session;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;

import java.util.UUID;

public class BikeFeedbackControlsFragment extends RentalControlsFragment implements RatingBar.OnRatingBarChangeListener {

    public static String TAG = BikeFeedbackControlsFragment.class.getSimpleName();

    private RatingBar ratingBar;
    private TextView tvSessionDuration;
    private TextView tvSessionPrice;

    public BikeFeedbackControlsFragment() {
        super(R.layout.fragment_rental_bike_feedback_controls);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
        updateSessionDurationAndPrice();
    }

    @Override
    public void setupView() {
        super.setupView();
        ratingBar = getView().findViewById(R.id.feedback_star_rating);
        tvSessionDuration = getView().findViewById(R.id.tv_session_duration);
        tvSessionPrice = getView().findViewById(R.id.tv_session_price);

        ratingBar.setOnRatingBarChangeListener(this);
    }

    public void updateSessionDurationAndPrice() {
        Session session = ApplicationState.getInstance().getCurrentSession();
        Long duration = session.getDuration();
        Long hours = duration / 3600;
        Long minutes = (duration % 3600) / 60;
        String durationString;
        if (hours == 1 && minutes == 1) {
            durationString = String.format("Rental Time: %d hour %d minute", hours, minutes);
        } else if (hours == 1 && minutes != 1) {
            durationString = String.format("Rental Time: %d hour %d minutes", hours, minutes);
        } else if (hours != 1 && minutes == 1) {
            durationString = String.format("Rental Time: %d hours %d minute", hours, minutes);
        } else {
            durationString = String.format("Rental Time: %d hours %d minutes", hours, minutes);
        }
        tvSessionDuration.setText(durationString);

        Long price = session.getPrice();
        String priceString = String.format("Price: $%d.%02d", price / 100, price % 100);
        tvSessionPrice.setText(priceString);
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        ApplicationState appState = ApplicationState.getInstance();
        LocationService locationService = LocationService.getInstance();
        Integer integerRating = Math.round(rating);
//        JSONObject mpProps = appState.getCurrentSession().getPropertiesAsJson();
//        try {
//            mpProps.put("feedback_rating", integerRating);
//        } catch (JSONException e) {
//            Log.d(TAG, e.getMessage());
//        }
//        BaasBikesApplication.getMixpanel().track("session.feedback.rating", mpProps);
        BikeFeedbackPromptSet set = appState.getBikeFeedbackPrompts(BikeFeedbackPromptSet.SESSION_END_FEEDBACK);
        if (rating < 5 && set != null && set.getPrompts() != null && !set.getPrompts().isEmpty()) {
            Intent intent = new Intent(this.getActivity(), BikeFeedbackActivity.class);
            intent.putExtra(BikeFeedbackActivity.EXTRA_STAR_RATING, integerRating);
            intent.putExtra(BikeFeedbackActivity.EXTRA_PROMPT_SET_NAME, BikeFeedbackPromptSet.SESSION_END_FEEDBACK);
            this.startActivity(intent);
        } else {
            UUID uuid = null;
            if (set != null) {
                uuid = set.getGuid();
            }

            BikeFeedbackEvent bikeFeedback = new BikeFeedbackEvent(
                    appState.getCurrentBike(),
                    locationService.getCurrentLatLng(),
                    true,
                    integerRating,
                    uuid,
                    null
            );
            final ProgressDialog progress = showProgressDialog("Submitting Rating...");
            bikeFeedback.save(new AsyncTaskCallback<BikeFeedbackEvent>() {
                @Override
                public void success(BikeFeedbackEvent data) {
                    progress.dismiss();
                    ApplicationState.getInstance().restore();
                }

                @Override
                public void error(Throwable e) {
                    progress.dismiss();
                    Log.e(TAG, "Error saving feedback.");
                    ApplicationState.getInstance().restore();
                }
            });
        }
    }
}
