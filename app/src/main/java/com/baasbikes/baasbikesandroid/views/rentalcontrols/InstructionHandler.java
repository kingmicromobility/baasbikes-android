package com.baasbikes.baasbikesandroid.views.rentalcontrols;

/**
 * Created by jmolineaux on 4/21/16.
 */
public interface InstructionHandler {

    void showUnlockInstructions();

}
