package com.baasbikes.baasbikesandroid.views.rentalcontrols;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.InstructionActivity;
import com.baasbikes.baasbikesandroid.R;
import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.Bike;
import com.baasbikes.baasbikesandroid.models.Person;
import com.baasbikes.baasbikesandroid.models.Reservation;
import com.baasbikes.baasbikesandroid.rest.NeedToAuthenticateException;
import com.baasbikes.baasbikesandroid.rest.NoPaymentMethodException;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class BikeSelectedControlsFragment extends RentalControlsFragment {

    public BikeSelectedControlsFragment() {
        super(R.layout.fragment_rental_bike_selected_controls);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    @Override
    public void setupView() {
        super.setupView();
        FloatingActionButton cancel = getView().findViewById(R.id.fab_cancel_selection);
        FloatingActionButton reserve = getView().findViewById(R.id.fab_reserve_bike);

        cancel.setOnClickListener(v -> cancelSelection());
        reserve.setOnClickListener(v -> reserveBike());
    }

    public void cancelSelection() {
        Bike bike = ApplicationState.getInstance().getCurrentBike();
        bike.deselect();
//        BaasBikesApplication.getMixpanel().track("bike.selection.canceled", bike.getPropertiesAsJson());
        updateControls();
    }

    public void reserveBike() {
        final ProgressDialog progress = showProgressDialog("Reserving...");
        ApplicationState.getInstance().getCurrentBike().reserve(new AsyncTaskCallback<Reservation>() {
            @Override
            public void success(Reservation data) {
                progress.dismiss();
//                BaasBikesApplication.getMixpanel().track("reservation.started", data.getPropertiesAsJson());
//                BaasBikesApplication.getMixpanel().timeEvent("session.started");
                if (Person.getLoggedInPerson(BaasBikesApplication.getAppContext()).shouldSeeInstructions()) {
                    Intent intent = new Intent(getActivity(), InstructionActivity.class);
                    intent.putExtra(InstructionActivity.EXTRA_BG_DRAWABLE, R.drawable.instructions_after_reserve);
                    intent.putExtra(
                            InstructionActivity.EXTRA_INSTRUCTION,
                            "You've reserved this bike for 15 minutes. Go unlock it!"
                    );
                    intent.putExtra(InstructionActivity.EXTRA_ACTION_TEXT, "CONTINUE");
                    intent.putExtra(InstructionActivity.EXTRA_ALLOW_BACK, false);
                    getActivity().startActivity(intent);
//                    BaasBikesApplication.getMixpanel().track("reservation.instructions.viewed");
                }
            }

            @Override
            public void error(Throwable e) {
                progress.dismiss();
                if (e instanceof NeedToAuthenticateException) {
                    authenticateUser();
                } else if (e instanceof NoPaymentMethodException) {
                    promptUserToAddPaymentMethod();
                }
            }
        });
    }
}
