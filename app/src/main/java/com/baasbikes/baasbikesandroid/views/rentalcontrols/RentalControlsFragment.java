package com.baasbikes.baasbikesandroid.views.rentalcontrols;


import android.app.ProgressDialog;
import android.content.Intent;
import android.widget.ImageView;

import androidx.annotation.LayoutRes;
import androidx.fragment.app.Fragment;

import com.baasbikes.baasbikesandroid.BikeFeedbackActivity;
import com.baasbikes.baasbikesandroid.PurchaseDetailsActivity;
import com.baasbikes.baasbikesandroid.R;
import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.BikeFeedbackPromptSet;

public abstract class RentalControlsFragment extends Fragment {

    protected String TAG;
    private RentalControlsUpdater updater;
    private AuthHandler authHandler;

    public RentalControlsFragment(@LayoutRes int layoutRes) {
        super(layoutRes);
    }

    public void setupView() {
        ImageView reportProblem = getView().findViewById(R.id.btn_report_problem);
        ImageView buy = getView().findViewById(R.id.btn_buy);
        reportProblem.setOnClickListener(v -> reportProblem());
        buy.setOnClickListener(v -> startActivity(new Intent(this.getActivity(), PurchaseDetailsActivity.class)));
    }

    public void setUpdater(RentalControlsUpdater updater) {
        this.updater = updater;
    }

    public void setAuthHandler(AuthHandler authHandler) {
        this.authHandler = authHandler;
    }

    private void reportProblem() {
//        Bike bike = ApplicationState.getInstance().getCurrentBike();
//        BaasBikesApplication.getMixpanel().track("bike.feedback", bike.getPropertiesAsJson());
        BikeFeedbackPromptSet set = ApplicationState.getInstance()
                .getBikeFeedbackPrompts(BikeFeedbackPromptSet.OUT_OF_SESSION_PROBLEM_PROMPTS);
        if (set != null && set.getPrompts() != null && !set.getPrompts().isEmpty()) {
            Intent intent = new Intent(this.getActivity(), BikeFeedbackActivity.class);
            intent.putExtra(BikeFeedbackActivity.EXTRA_PROMPT_SET_NAME,
                    BikeFeedbackPromptSet.OUT_OF_SESSION_PROBLEM_PROMPTS);
            this.startActivity(intent);
        }
    }

    protected void updateControls() {
        if (this.updater != null) {
            this.updater.updateRentalControls();
        }
    }

    protected void authenticateUser() {
        authHandler.authenticateUser();
    }

    protected void promptUserToAddPaymentMethod() {
        authHandler.promptUserToAddPaymentMethod();
    }

    protected void askUserIfAppShouldShowInstructions() {
        authHandler.askUserIfAppShouldShowInstructions();
    }

    protected ProgressDialog showProgressDialog(String message) {
        return updater.showProgressDialog(message);
    }
}
