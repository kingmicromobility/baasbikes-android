package com.baasbikes.baasbikesandroid.views;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.MainActivity;
import com.baasbikes.baasbikesandroid.R;
import com.baasbikes.baasbikesandroid.location.LocationService;
import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.Bike;
import com.baasbikes.baasbikesandroid.models.BikeMapMarker;
import com.baasbikes.baasbikesandroid.models.Market;
import com.baasbikes.baasbikesandroid.models.RouteMapPolyline;
import com.baasbikes.baasbikesandroid.models.geo.MultiPolygon;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.VisibleRegion;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by jmolineaux on 3/26/16.
 */
public class MapLayout extends SupportMapFragment
        implements OnMapReadyCallback, GoogleMap.OnCameraChangeListener, GoogleMap.OnMarkerClickListener {

    private static String TAG = MapLayout.class.getSimpleName();
    private static Integer BOUNDS_PADDING = 25;
    private GoogleMap map;
    private Map<UUID, Marker> bikeGuidToMarkerMap;
    private Map<Marker, UUID> markerToBikeGuidMap;
    private Map<UUID, Polyline> tripToPolylineMap;
    private MapControlsFragment controlsFragment;
    private MainActivity mainActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getMapAsync(this);
        mainActivity = (MainActivity) getActivity();
        bikeGuidToMarkerMap = new HashMap<>();
        markerToBikeGuidMap = new HashMap<>();
        tripToPolylineMap = new HashMap<>();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMyLocationEnabled(true);
        map.setIndoorEnabled(true);
        map.setLocationSource(LocationService.getInstance());
        map.setOnCameraChangeListener(this);
        map.getUiSettings().setZoomGesturesEnabled(true);
        map.getUiSettings().setScrollGesturesEnabled(true);
        map.getUiSettings().setRotateGesturesEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.getUiSettings().setRotateGesturesEnabled(false);
        map.setOnMarkerClickListener(this);

        ApplicationState.getInstance().setMainMap(this);
        drawMarketOnMap();

        if (ApplicationState.getInstance().getCurrentBike() == null) {
            moveToMarketBounds();
        } else {
            moveToLocation(ApplicationState.getInstance().getCurrentBike().getLocation(), false);
        }
    }

    public void animateToCurrentLocation() {
        moveToCurrentLocation(true);
    }

    public void moveToCurrentLocation() {
        moveToCurrentLocation(false);
    }

    public void moveToCurrentLocation(Boolean animate) {
        LocationService locationService = LocationService.getInstance();
        LatLng centerCoord = locationService.getCurrentLatLng();
        moveToLocation(centerCoord, animate);
        controlsFragment.setUserLocationFabActive();
    }

    public void animateToMarketBounds() {
        moveToMarketBounds(true);
    }

    public void moveToMarketBounds() {
        moveToMarketBounds(false);
    }

    public void moveToMarketBounds(Boolean animate) {
        Market market = ApplicationState.getInstance().getCurrentMarket();
        if (market != null) {
            LatLngBounds bounds = market.getGeoshape().getRectangularBounds();
            CameraUpdate camera = CameraUpdateFactory.newLatLngBounds(bounds, BOUNDS_PADDING);
            if (animate) {
                map.animateCamera(camera);
            } else {
                map.moveCamera(camera);
            }
            controlsFragment.setMarketBoundsFabActive();
        }
    }

    public void animateToLocation(LatLng location) {
        moveToLocation(location, true);
    }

    public void moveToLocation(LatLng location, Boolean animate) {
        CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(location, 15.0f);
        if (animate) {
            map.animateCamera(camera);
        } else {
            map.moveCamera(camera);
        }
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        if (locationNearSelectedBike(cameraPosition.target)) {
            Bike currentBike = ApplicationState.getInstance().getCurrentBike();
            animateToLocation(translateToFitBikeBubble(currentBike.getLocation()));
        } else {
            Boolean isAtMarket = false;
            Boolean isAtUserLocation = false;

            Market market = ApplicationState.getInstance().getCurrentMarket();
            if (market != null) {
                LatLng marketCenter = market.getGeoshape().getRectangularBounds().getCenter();
                isAtMarket = latLongAreSimilar(cameraPosition.target, marketCenter);
            }

            LocationService locationService = LocationService.getInstance();
            LatLng currentLocation = locationService.getCurrentLatLng();
            if (currentLocation != null) {
                if (locationNearSelectedBike(currentLocation)) {
                    currentLocation = translateToFitBikeBubble(currentLocation);
                }
                isAtUserLocation = latLongAreSimilar(cameraPosition.target, currentLocation);
            }

            if (controlsFragment != null && !isAtMarket) {
                controlsFragment.setMarketBoundsFabInactive();
            }
            if (controlsFragment != null && !isAtUserLocation) {
                controlsFragment.setUserLocationFabInactive();
            }
        }
    }

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {
        ApplicationState appState = ApplicationState.getInstance();
        if (appState.getCurrentReservation() == null && appState.getCurrentSession() == null) {
            UUID bikeGuid = markerToBikeGuidMap.get(marker);
            Bike bike = appState.getCurrentMarket().getBikeMap().get(bikeGuid);
            if (bike != null) {
                bike.select();
                animateToLocation(bike.getLocation());
//                BaasBikesApplication.getMixpanel().track("bike.selected", bike.getPropertiesAsJson());
//                BaasBikesApplication.getMixpanel().timeEvent("reservation.started");
                mainActivity.updateRentalControls();
            }
        }
        return true;
    }

    public void updateMarkerDisplay(BikeMapMarker marker) {
        if (this.getActivity() != null && marker != null) {
            this.getActivity().runOnUiThread(new MarkerUpdater(marker));
        }
    }

    public void updatePolylineDisplay(RouteMapPolyline polyline) {
        if (this.getActivity() != null && polyline != null) {
            this.getActivity().runOnUiThread(new PolylineUpdater(polyline));
        }
    }

    public void setControlsFragment(MapControlsFragment controlsFragment) {
        this.controlsFragment = controlsFragment;
    }

    public void drawMarketOnMap() {
        Market market = ApplicationState.getInstance().getCurrentMarket();
        MultiPolygon geoshape = market.getGeoshape();
        Context appContext = BaasBikesApplication.getAppContext();
        map.clear();
        bikeGuidToMarkerMap = new HashMap<>();
        markerToBikeGuidMap = new HashMap<>();


        for (Bike bike : market.getBikeMap().values()) {
            bike.getMarker().assignToMap(this);
        }

        PolygonOptions polygon = new PolygonOptions()
                .addAll(geoshape.getPolygonPoints())
                .fillColor(appContext.getResources().getColor(R.color.marketBoundsFill))
                .strokeColor(appContext.getResources().getColor(R.color.marketBoundsStroke))
                .strokeWidth(2.0f);

        map.addPolygon(polygon);
    }

    private class MarkerUpdater implements Runnable {

        private final BikeMapMarker marker;

        public MarkerUpdater(BikeMapMarker marker) {
            this.marker = marker;
        }

        @Override
        public void run() {
            Marker existingMarker = bikeGuidToMarkerMap.get(this.marker.getBike().getGuid());
            if (existingMarker != null) {
                markerToBikeGuidMap.remove(existingMarker);
                existingMarker.remove();
            }
            Marker newMarker = map.addMarker(marker.getMarkerOptions());
            markerToBikeGuidMap.put(newMarker, marker.getBike().getGuid());
            bikeGuidToMarkerMap.put(marker.getBike().getGuid(), newMarker);
        }
    }

    private class PolylineUpdater implements Runnable {

        private final RouteMapPolyline polyline;

        public PolylineUpdater(RouteMapPolyline polyline) { this.polyline = polyline; }

        @Override
        public void run() {
            Polyline oldLine = tripToPolylineMap.get(polyline.getTrip().getGuid());
            if (oldLine != null) { oldLine.remove(); }
            Polyline newLine = map.addPolyline(polyline.getPolylineOptions());
            tripToPolylineMap.put(polyline.getTrip().getGuid(), newLine);
        }
    }

    private Boolean latLongAreSimilar(LatLng latLng1, LatLng latLng2) {
        Double threshold = 0.0001;
        return latLongAreSimilar(latLng1, latLng2, threshold);
    }

    private Boolean latLongAreSimilar(LatLng latLng1, LatLng latLng2, Double threshold) {
        Boolean latitudeIsSimilar = Math.abs(latLng1.latitude - latLng2.latitude) < threshold;
        Boolean longitudeIsSimilar = Math.abs(latLng1.longitude - latLng2.longitude) < threshold;
        return latitudeIsSimilar && longitudeIsSimilar;
    }

    private Boolean locationNearSelectedBike(LatLng latLng) {
        Bike currentBike = ApplicationState.getInstance().getCurrentBike();
        if (currentBike != null) {
            return latLongAreSimilar(currentBike.getLocation(), latLng);
        } else {
            return false;
        }
    }

    private LatLng translateToFitBikeBubble(LatLng latLng) {
        VisibleRegion vr = map.getProjection().getVisibleRegion();
        double top = vr.latLngBounds.northeast.latitude;
        double bottom = vr.latLngBounds.southwest.latitude;

        double latitude = Math.abs(top - bottom) * 0.25 + latLng.latitude;
        return new LatLng(latitude, latLng.longitude);
    }
}
