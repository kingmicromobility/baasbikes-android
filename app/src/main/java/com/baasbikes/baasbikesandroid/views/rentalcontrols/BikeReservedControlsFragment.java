package com.baasbikes.baasbikesandroid.views.rentalcontrols;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.InstructionActivity;
import com.baasbikes.baasbikesandroid.R;
import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.Person;
import com.baasbikes.baasbikesandroid.models.Reservation;
import com.baasbikes.baasbikesandroid.models.Session;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class BikeReservedControlsFragment extends RentalControlsFragment {

    public static String TAG = BikeReservedControlsFragment.class.getSimpleName();
    public static Integer BEFORE_UNLOCK_INSTRUCTION_ACTIVITY_RESULT = 200;

    private Timer updateTimer;
    private TextView tvReservationTimeRemaining;
    private FloatingActionButton cancelReservation;
    private FloatingActionButton unlockBike;

    public BikeReservedControlsFragment() {
        super(R.layout.fragment_rental_bike_reserved_controls);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupView();
    }

    @Override
    public void setupView() {
        super.setupView();
        tvReservationTimeRemaining = getView().findViewById(R.id.tv_reservation_time_remaining);
        cancelReservation = getView().findViewById(R.id.fab_cancel_reservation);
        unlockBike = getView().findViewById(R.id.fab_unlock_bike);

        cancelReservation.setOnClickListener(v -> cancelReservation());
        unlockBike.setOnClickListener(v -> unlockBike());
    }

    @Override
    public void onStart() {
        super.onStart();
        if (updateTimer != null) {
            updateTimer.cancel();
            updateTimer = null;
        }
        updateTimer = new Timer();
        updateTimer.schedule(new UpdateScreenTask(), 0, 1000);
    }

    @Override
    public void onStop() {
        super.onStop();
        updateTimer.cancel();
        updateTimer = null;
    }

    public void cancelReservation() {
        final ProgressDialog progress = showProgressDialog("Canceling Reservation...");
        ApplicationState.getInstance().getCurrentReservation().cancel(new AsyncTaskCallback<Reservation>() {
            @Override
            public void success(Reservation data) {
                progress.dismiss();
//                BaasBikesApplication.getMixpanel().track("reservation.canceled", data.getPropertiesAsJson());
                ApplicationState.getInstance().clearRentalState();
            }

            @Override
            public void error(Throwable e) {
                progress.dismiss();
            }
        });
    }

    public void unlockBike() {
        if (Person.getLoggedInPerson(BaasBikesApplication.getAppContext()).shouldSeeInstructions()) {
            Intent intent = new Intent(this.getActivity(), InstructionActivity.class);
            intent.putExtra(InstructionActivity.EXTRA_BG_DRAWABLE, R.drawable.instructions_before_unlock_seat_post);
            intent.putExtra(InstructionActivity.EXTRA_INSTRUCTION, "Press button to wake lock.");
            intent.putExtra(InstructionActivity.EXTRA_ACTION_TEXT, "UNLOCK");
            intent.putExtra(InstructionActivity.EXTRA_ALLOW_BACK, true);
            startActivityForResult(intent, BEFORE_UNLOCK_INSTRUCTION_ACTIVITY_RESULT);
//            BaasBikesApplication.getMixpanel().track("bike.unlock.before.instructions.viewed");
        } else {
            performUnlock();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BEFORE_UNLOCK_INSTRUCTION_ACTIVITY_RESULT) {
            if (resultCode == Activity.RESULT_OK) {
                performUnlock();
            } else {
//                BaasBikesApplication.getMixpanel().track("bike.unlock.before.instructions.back");
            }
        }
    }

    private void performUnlock() {
        final ProgressDialog progress = showProgressDialog("Unlocking...");
        ApplicationState.getInstance().getCurrentReservation().beginSession(new AsyncTaskCallback<Session>() {
            @Override
            public void success(Session data) {
                progress.dismiss();
//                BaasBikesApplication.getMixpanel().track("bike.unlock.success", data.getBike().getPropertiesAsJson());
//                BaasBikesApplication.getMixpanel().track("session.started", data.getPropertiesAsJson());
//                BaasBikesApplication.getMixpanel().timeEvent("session.ended");
//                BaasBikesApplication.getMixpanel().track("trip.started", data.getPropertiesAsJson());
//                BaasBikesApplication.getMixpanel().timeEvent("trip.ended");
//                BaasBikesApplication.getMixpanel().getPeople().increment("session count", 1);
//                BaasBikesApplication.getMixpanel().getPeople().increment("trip count", 1);
//                if (Person.getLoggedInPerson(BaasBikesApplication.getAppContext()).shouldSeeInstructions()) {
//                    Intent intent = new Intent(getActivity(), InstructionActivity.class);
//                    intent.putExtra(InstructionActivity.EXTRA_BG_DRAWABLE,
//                            R.drawable.instructions_after_unlock_seat_post);
//                    intent.putExtra(InstructionActivity.EXTRA_INSTRUCTION,
//                            "Pull chain out and leave it at the bike rack. Take the bike for a test ride!");
//                    intent.putExtra(InstructionActivity.EXTRA_ACTION_TEXT, "CONTINUE");
//                    intent.putExtra(InstructionActivity.EXTRA_ALLOW_BACK, false);
//                    getActivity().startActivity(intent);
////                    BaasBikesApplication.getMixpanel().track("bike.unlock.after.instructions.viewed");
//                }
            }

            @Override
            public void error(Throwable e) {
                progress.dismiss();
//                Bike bike = ApplicationState.getInstance().getCurrentBike();
//                BaasBikesApplication.getMixpanel().track("bike.unlock.fail", bike.getPropertiesAsJson());
            }
        });
    }


    private class UpdateScreenTask extends TimerTask {
        @Override
        public void run() {
            getActivity().runOnUiThread(() -> {
                Reservation reservation = ApplicationState.getInstance().getCurrentReservation();
                Long timeRemaining = reservation.getTimeRemaining();
                Long minutesRemaining = timeRemaining / 60;
                Long secondsRemaining = timeRemaining % 60;
                String timeRemainingString = String.format(Locale.getDefault(), "%d:%02d Remaining",
                        minutesRemaining, secondsRemaining);
                tvReservationTimeRemaining.setText(timeRemainingString);
            });
        }
    }
}
