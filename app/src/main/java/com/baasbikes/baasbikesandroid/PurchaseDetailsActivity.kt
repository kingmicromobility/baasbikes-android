package com.baasbikes.baasbikesandroid

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.baasbikes.baasbikesandroid.models.ApplicationState
import java.text.NumberFormat
import java.util.*

private const val INFO_NOT_AVAILABLE = "Not Available"
private const val REQUEST_CONFIRM = 0x00000000011

class PurchaseDetailsActivity : BaseActivity() {

    private lateinit var bikeImageView: ImageView
    private lateinit var brandTextView: TextView
    private lateinit var colorTextView: TextView
    private lateinit var modelTextView: TextView
    private lateinit var gearsTextView: TextView
    private lateinit var sizeTextView: TextView
    private lateinit var priceTextView: TextView
    private lateinit var morePurchaseDetails: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_purchase_details)
        setupView()
    }

    override fun setupView() {
        bikeImageView = findViewById(R.id.iv_bike)
        brandTextView = findViewById(R.id.txtv_brand)
        colorTextView = findViewById(R.id.txtv_color)
        modelTextView = findViewById(R.id.txtv_model)
        gearsTextView = findViewById(R.id.txtv_gears)
        sizeTextView = findViewById(R.id.txtv_size)
        priceTextView = findViewById(R.id.txtv_price)
        morePurchaseDetails = findViewById(R.id.btn_more_purchase_details)

        with(ApplicationState.getInstance().currentBike) {
            brandTextView.text = brand?.name ?: INFO_NOT_AVAILABLE
            colorTextView.text = detailSet["color"] ?: INFO_NOT_AVAILABLE
            modelTextView.text = detailSet["model"] ?: INFO_NOT_AVAILABLE
            gearsTextView.text = detailSet["gears"] ?: INFO_NOT_AVAILABLE
            sizeTextView.text = size?.name ?: INFO_NOT_AVAILABLE
            bikeImageView.setImageBitmap(
                getImage("thumbnail") ?: BitmapFactory.decodeResource(
                    resources,
                    R.drawable.baas_bikes_logo
                )
            )
            priceTextView.text = detailSet["price_with_lock"]?.toInt()?.run {
                NumberFormat.getCurrencyInstance(Locale.US).format(this / 100.00)
            } ?: INFO_NOT_AVAILABLE
        }
        morePurchaseDetails.setOnClickListener { purchaseDetailsClicked() }
    }

    private fun purchaseDetailsClicked() {
//        Bike bike = ApplicationState.getInstance().getCurrentBike();
//        getMixpanelAPI().track("bike.details.morePurchaseDetails", bike.getPropertiesAsJson());
        startActivityForResult(Intent(this, ConfirmBikePurchaseActivity::class.java), REQUEST_CONFIRM)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CONFIRM && resultCode == RESULT_OK) {
            finish()
        }
    }
}