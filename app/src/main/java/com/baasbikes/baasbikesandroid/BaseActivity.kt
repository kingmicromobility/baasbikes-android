package com.baasbikes.baasbikesandroid

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.baasbikes.baasbikesandroid.models.Person
import com.mixpanel.android.mpmetrics.MixpanelAPI

open class BaseActivity : AppCompatActivity() {

    protected val TAG by lazy { this.javaClass.simpleName }
    protected val mixpanelAPI: MixpanelAPI by lazy { BaasBikesApplication.getMixpanel() }
    protected lateinit var person: Person
    private val progressDialog: ProgressDialog by lazy { ProgressDialog(this) }

    open fun setupView() {}

    fun navigateTo(destination: Class<*>, withFinish: Boolean) {
        val intent = Intent(this, destination)
        this.startActivity(intent)
        if (withFinish) this.finish()
    }

    fun startActivityForResult(destination: Class<*>, requestCode: Int) {
        intent = Intent(this, destination)
        startActivityForResult(intent, requestCode)
    }

    fun setResultAndFinish(resultCode: Int) {
        setResult(resultCode)
        finish()
    }

    fun showAlertDialog(@StringRes title: Int, @StringRes message: Int) {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.ok) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    fun showAlertDialog(@StringRes title: Int, message: String) {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.ok) { dialog, _ -> dialog.dismiss() }
            .show()
    }

    fun showAlertDialog(
        @StringRes title: Int,
        @StringRes message: Int,
        @StringRes positiveText: Int,
        positiveListener: () -> Unit,
        @StringRes negativeText: Int,
        negativeListener: () -> Unit
    ) {
        AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positiveText) { _, _ -> positiveListener() }
            .setNegativeButton(negativeText) { _, _ -> negativeListener() }
            .show()
    }

    fun showProgress(@StringRes messageRes: Int) {
        progressDialog.setMessage(getString(messageRes))
        progressDialog.show()
    }

    fun dismissProgress() {
        progressDialog.dismiss()
    }
}
