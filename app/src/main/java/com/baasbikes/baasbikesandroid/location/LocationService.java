package com.baasbikes.baasbikesandroid.location;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;
import com.baasbikes.baasbikesandroid.util.ConnectionUtils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.model.LatLng;

import java.util.Observable;


/**
 * Created by jmolineaux on 3/26/16.
 */
public class LocationService extends Observable implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        Application.ActivityLifecycleCallbacks,
        LocationSource, ResultCallback<LocationSettingsResult> {

    private static final String TAG = LocationService.class.getSimpleName();
    private static final Integer LOCATION_UPDATES_INTERVAL = 1000;
    private static final Integer LOCATION_UPDATES_FASTEST_INTERVAL = 1000;
    public static final int ACCURACY_IN_METERS = 10;
    public static final String LOCATION_ERROR = "location_error";
    private static LocationService instance;
    private static Boolean connectionInProgress = false;

    private AsyncTaskCallback<Void> connectionListener;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private OnLocationChangedListener locationSourceDelegate;
    private Location currentLocation;
    /**
     * Stores the types of location services the client is interested in using. Used for checking
     * settings to determine if the device has optimal location settings.
     */
    protected LocationSettingsRequest mLocationSettingsRequest;
    /**
     * Stores parameters for requests to the FusedLocationProviderApi.
     */
    private LocationRequest mLocationRequest;

    public static LocationService getInstance() {
        if (instance == null) {
            instance = new LocationService();
        }
        return instance;
    }

    public static LocationService connect(final Activity activity, final AsyncTaskCallback<Void> callback) {
        LocationService locationService = getInstance();
        if (!connectionInProgress) {
            connectionInProgress = true;
            locationService.connectionListener = callback;

            if (!ConnectionUtils.isLocationEnabled(activity)) {
                callback.error(new Exception());
                return locationService;
            }

            return locationService
                    .buildGoogleApiClient()
                    .createLocationRequest()
                    .buildLocationSettingsRequest()
                    .onResume(activity);
        } else {
            return locationService;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        // If the initial location was never previously requested, we use
        // FusedLocationApi.getLastLocation() to get it. If it was previously requested, we store
        // its value in the Bundle and check for it in onCreate(). We
        // do not request it again unless the user specifically requests location updates by pressing
        // the Start Updates button.
        //
        // Because we cache the value of the initial location in the Bundle, it means that if the
        // user launches the activity,
        // moves to a new location, and then changes the device orientation, the original location
        // is displayed as the activity is re-created.
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
        if (bundle == null) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if (lastLocation != null && lastLocation.hasAccuracy() && lastLocation.getAccuracy() < ACCURACY_IN_METERS) {
                onLocationChanged(lastLocation);
            } else {
                connectionListener.error(new Exception(LOCATION_ERROR));
            }
        }
        connectionInProgress = false;

//        if (currentLocation == null) {
//            if (ActivityCompat.checkSelfPermission(
//                    getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
//                currentLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
//                updateLocationUI();
//            }
//        }

    }

    public boolean isConnected() {
        return googleApiClient != null && googleApiClient.isConnected();
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG, "location service suspended");
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "location changed!");
        currentLocation = location;

        if (locationSourceDelegate !=  null) {
            locationSourceDelegate.onLocationChanged(location);
        }

        if (connectionListener != null) {
            connectionListener.success(null);
            connectionListener = null;
        }

        setChanged();
        notifyObservers();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed");
        connectionInProgress = false;
        connectionListener.error(new Exception());
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        locationSourceDelegate = onLocationChangedListener;
    }

    @Override
    public void deactivate() {
        locationSourceDelegate = null;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {}

    @Override
    public void onActivityStarted(Activity activity) {
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
        onResume(activity);
    }

    @Override
    public void onActivityPaused(Activity activity) {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
            }
        }
    }

    @Override
    public void onActivityStopped(Activity activity) {}

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

    @Override
    public void onActivityDestroyed(Activity activity) {}

    public Location getCurrentLocation() {
        return this.currentLocation;
    }

    public LatLng getCurrentLatLng() {
        if (currentLocation == null) {
            return null;
        }
        return new LatLng(this.currentLocation.getLatitude(), this.currentLocation.getLongitude());
    }

    private LocationService() { super(); }

    /**
     * Builds a GoogleApiClient. Uses the {@code #addApi} method to request the
     * LocationServices API.
     */
    private LocationService buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(BaasBikesApplication.getAppContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        return this;
    }

    /**
     * Sets up the location request. Android has two location request settings:
     * {@code ACCESS_COARSE_LOCATION} and {@code ACCESS_FINE_LOCATION}. These settings control
     * the accuracy of the current location.
     * <p/>
     * When the ACCESS_FINE_LOCATION setting is specified, combined with a fast update
     * interval (5 seconds), the Fused Location Provider API returns location updates that are
     * accurate to within a few feet.
     * <p/>
     * These settings are appropriate for mapping applications that show real-time location
     * updates.
     */
    private LocationService createLocationRequest() {
        locationRequest = LocationRequest.create()
                .setInterval(LOCATION_UPDATES_INTERVAL)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setFastestInterval(LOCATION_UPDATES_FASTEST_INTERVAL);
        return this;
    }

    private LocationService onResume(Activity activity) {
        if (isLocationEnabled(activity.getApplicationContext())) {
            if (googleApiClient != null && googleApiClient.isConnected()) {
                startLocationUpdates(activity);
            }
        }
        googleApiClient.connect();
        return this;
    }

    @Override
    public void onResult(LocationSettingsResult locationSettingsResult) {
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    /**
     * Uses a {@link com.google.android.gms.location.LocationSettingsRequest.Builder} to build
     * a {@link com.google.android.gms.location.LocationSettingsRequest} that is used for checking
     * if a device has the needed location settings.
     */
    protected LocationService buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
        return this;
    }

    /**
     * Requests location updates from the FusedLocationApi.
     * http://qiita.com/daisy1754/items/aa9ad75d1a84b745469b
     */
    protected void startLocationUpdates(Activity activity) {
        if (ActivityCompat.checkSelfPermission(
                activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
            }
            return;
        } else {
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(
                googleApiClient,
                mLocationRequest,
                this
        ).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(Status status) {
                if (status.isSuccess()) {
                    Log.d(TAG, "App Indexing API: Recorded recipe view end successfully."+ status.toString());
                } else {
                    Log.d(TAG, "App Indexing API: There was an error recording the recipe view."
                            + status.toString());
                }
            }
        });

    }
}
