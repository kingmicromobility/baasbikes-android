package com.baasbikes.baasbikesandroid;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.Bike;
import com.baasbikes.baasbikesandroid.models.Market;
import com.baasbikes.baasbikesandroid.models.Person;
import com.baasbikes.baasbikesandroid.models.Session;
import com.baasbikes.baasbikesandroid.views.MapControlsFragment;
import com.baasbikes.baasbikesandroid.views.MapLayout;
import com.baasbikes.baasbikesandroid.views.rentalcontrols.AuthHandler;
import com.baasbikes.baasbikesandroid.views.rentalcontrols.BikeHeldControlsFragment;
import com.baasbikes.baasbikesandroid.views.rentalcontrols.RentalControlsFactory;
import com.baasbikes.baasbikesandroid.views.rentalcontrols.RentalControlsUpdater;
import com.google.android.material.navigation.NavigationView;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.request.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.request.CustomField;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.support.SupportActivity;

import java.util.Arrays;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

public class MainActivity
        extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        RentalControlsUpdater,
        AuthHandler,
        Observer {

    private RentalControlsFactory rentalControlsFactory;
    private MapLayout mapLayout;
    private MapControlsFragment mapControls;
    private TextView signUpLoginButton;
    private DrawerLayout drawer;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(null);
        setContentView(R.layout.activity_main);

        setupView();
        ApplicationState.getInstance().addObserver(this);
    }

    @Override
    public void setupView() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }

        signUpLoginButton = findViewById(R.id.btn_sign_up_log_in);
        signUpLoginButton.setOnClickListener(v -> signUpLogIn());
    }

    @Override
    public void onPostResume() {
        super.onPostResume();

        if (mapLayout == null || mapControls == null) {
            mapLayout = new MapLayout();
            mapControls = new MapControlsFragment();
            mapLayout.setControlsFragment(mapControls);
            mapControls.setMapFragment(mapLayout);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.map, mapLayout)
                    .commit();

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.map_controls, mapControls)
                    .commit();
        }

        rentalControlsFactory = new RentalControlsFactory(this, this);
        updateRentalControls();
        updateNavMenu();
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
//            getMixpanelAPI().track("drawer.left.close");
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_ride_a_bike:
            case R.id.nav_ride_a_bike_logged_out:
//                getMixpanelAPI().track("drawer.left.ride");
                break;
            case R.id.nav_my_account:
//                getMixpanelAPI().track("drawer.left.account");
                navigateTo(MyAccountActivity.class, false);
                break;
            case R.id.nav_contact_us:
                this.launchZendeskContactUs();
                break;
            case R.id.nav_what_are_baas_bikes:
//                getMixpanelAPI().track("drawer.left.whatAreBaasBikes");
                navigateTo(TutorialActivity.class, false);
                break;
            case R.id.nav_help:
            case R.id.nav_help_logged_out:
//                getMixpanelAPI().track("drawer.left.help");
                this.launchZendeskHelpCenter();
                break;
            default:
                break;

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void signUpLogIn() {
        navigateTo(TutorialActivity.class, false);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void update(Observable observable, Object data) {
        if (observable instanceof ApplicationState) {
            updateRentalControls();
            updateNavMenu();
            if (this.mapLayout != null) {
                mapLayout.drawMarketOnMap();
            }
        }
    }

    @Override
    public void updateRentalControls() {
        Bike currentBike = ApplicationState.getInstance().getCurrentBike();

        Fragment oldRentalControls = getSupportFragmentManager().findFragmentById(R.id.rental_controls);
        Fragment newRentalControls = rentalControlsFactory.createRentalControlsFragment();
        if (newRentalControls != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.rental_controls, newRentalControls)
                    .commitAllowingStateLoss();
        } else if (oldRentalControls != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .remove(oldRentalControls)
                    .commitAllowingStateLoss();
        }
        if (newRentalControls instanceof BikeHeldControlsFragment
                && currentBike != null && currentBike.isOutOfBounds()) {
            showOutOfBoundsWarning();
        }
    }

    public void updateNavMenu() {
        Menu menu = navigationView.getMenu();
        person = Person.getLoggedInPerson(BaasBikesApplication.getAppContext());

        if (person != null) {
            signUpLoginButton.setVisibility(View.INVISIBLE);
            menu.setGroupVisible(R.id.nav_logged_in_menu, true);
            menu.setGroupVisible(R.id.nav_logged_out_menu, false);
        } else {
            signUpLoginButton.setVisibility(View.VISIBLE);
            menu.setGroupVisible(R.id.nav_logged_in_menu, false);
            menu.setGroupVisible(R.id.nav_logged_out_menu, true);
        }
    }

    @Override
    public void authenticateUser() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.alert_authenticate_title)
                .setMessage(R.string.alert_authenticate_message)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    dialog.cancel();
                    navigateTo(TutorialActivity.class, false);
                }).show();
    }

    @Override
    public void promptUserToAddPaymentMethod() {
        new AlertDialog.Builder(this)
                .setTitle(R.string.alert_add_credit_card_title)
                .setMessage(R.string.alert_add_credit_card_message)
                .setPositiveButton(R.string.ok, (dialog, which) -> dialog.cancel())
                .show();
    }

    public void showOutOfBoundsWarning() {
//        Session session = ApplicationState.getInstance().getCurrentSession();
//        getMixpanelAPI().track("session.lock.boundsWarning.viewed", session.getPropertiesAsJson());

        new AlertDialog.Builder(this)
                .setTitle(R.string.alert_out_of_bounds_title)
                .setMessage(R.string.alert_out_of_bounds_message)
                .setPositiveButton(R.string.ok, (dialog, which) -> {
                    dialog.cancel();
                    if (Person.getLoggedInPerson(BaasBikesApplication.getAppContext()).shouldSeeInstructions()) {
                        askUserIfAppShouldShowInstructions();
                    }
                }).show();
    }

    public void askUserIfAppShouldShowInstructions() {
//        getMixpanelAPI().track("person.instructions.continue-showing.viewed");

        new AlertDialog.Builder(this)
                .setTitle(R.string.alert_keep_showing_instructions_title)
                .setMessage(R.string.alert_keep_showing_instructions_message)
                .setPositiveButton(R.string.alert_keep_showing_instructions_positive, (dialog, which) -> {
                    Person user = Person.getLoggedInPerson(BaasBikesApplication.getAppContext());
                    user.setSeeInstructions(false);
                    user.saveAsLoggedInPerson(BaasBikesApplication.getAppContext());
//                    getMixpanelAPI().track("person.instructions.continue-showing.no");
                    dialog.cancel();
                })
                .setNegativeButton(R.string.alert_keep_showing_instructions_negative, (dialog, which) -> {
//                    getMixpanelAPI().track("person.instructions.continue-showing.yes");
                    dialog.cancel();
                }).show();
    }

    //todo: check usage of this method
    public ProgressDialog showProgressDialog(String message) {
        ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(message);
        progress.show();
        return progress;
    }

    private void launchZendeskHelpCenter() {
        ZendeskConfig.INSTANCE.setContactConfiguration(new BaseZendeskFeedbackConfiguration() {
            @Override
            public String getRequestSubject() {
                return getString(R.string.help);
            }
        });

        Identity anonymousIdentity = new AnonymousIdentity.Builder().build();
        ZendeskConfig.INSTANCE.setIdentity(anonymousIdentity);

        new SupportActivity.Builder()
                .listArticles(202745648L)
                .show(this);
    }

    private void launchZendeskContactUs() {
        ZendeskConfig.INSTANCE.setContactConfiguration(new BaseZendeskFeedbackConfiguration() {
            @Override
            public String getRequestSubject() {
                return getString(R.string.zendesk_request_subject);
            }

            @Override
            public String getAdditionalInfo() {
                return getString(R.string.zendesk_additional_info);
            }
        });

        ZendeskConfig.INSTANCE.setTicketFormId(62609l);

        String appVersion = String.format(Locale.US, "version_%s", BuildConfig.VERSION_NAME);
        CustomField customFieldAppVersion = new CustomField(28981538l, appVersion);
        String osVersion = String.format(Locale.US, "Android %s, Version %s", Build.VERSION.RELEASE, Build.VERSION.SDK_INT);
        CustomField customFieldOsVersion = new CustomField(28988197l, osVersion);
        String deviceModel = String.format(Locale.US, "%s %s %s", Build.BRAND, Build.DEVICE, Build.MODEL);
        CustomField customFieldDeviceModel = new CustomField(28981548l, deviceModel);
        Market currentMarket = ApplicationState.getInstance().getCurrentMarket();
        CustomField customFieldMarketGuid = new CustomField(28992127l, currentMarket.getGuid().toString());
        CustomField customFieldMarketName = new CustomField(28992047l, currentMarket.getName());

        ZendeskConfig.INSTANCE.setCustomFields(Arrays.asList(
                customFieldAppVersion, customFieldOsVersion, customFieldDeviceModel,
                customFieldMarketGuid, customFieldMarketName));

        Person person = Person.getLoggedInPerson(BaasBikesApplication.getAppContext());
        Identity identity = new AnonymousIdentity.Builder()
                .withNameIdentifier(person.getFullName())
                .withEmailIdentifier(person.getEmail())
                .withExternalIdentifier(person.getGuid().toString())
                .build();
        ZendeskConfig.INSTANCE.setIdentity(identity);

        navigateTo(ContactZendeskActivity.class, false);
    }
}
