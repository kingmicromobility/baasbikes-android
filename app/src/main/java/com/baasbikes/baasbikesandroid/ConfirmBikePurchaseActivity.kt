package com.baasbikes.baasbikesandroid

import android.content.Intent
import android.os.Bundle
import android.widget.Button

private const val REQUEST_MESSAGE_US = 0x00000000111

class ConfirmBikePurchaseActivity : BaseActivity() {

    private lateinit var confirmPurchase: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_bike_purchase)
        setupView()
    }

    override fun setupView() {
        confirmPurchase = findViewById(R.id.btn_confirm_purchase)
        confirmPurchase.setOnClickListener { confirmPurchase() }
    }

    //TODO: The is no logic to subtract money from account
    private fun confirmPurchase() {
//        Bike bike = ApplicationState.getInstance().getCurrentBike();
//        getMixpanelAPI().track("bike.details.purchasecomplete", bike.getPropertiesAsJson());
        val intent = Intent(this, MessageUsToBuyBikeActivity::class.java)
        startActivityForResult(intent, REQUEST_MESSAGE_US)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_MESSAGE_US && resultCode == RESULT_OK) {
            setResult(RESULT_OK)
            finish()
        }
    }
}