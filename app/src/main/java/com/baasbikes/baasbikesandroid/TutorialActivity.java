package com.baasbikes.baasbikesandroid;

import android.content.res.ColorStateList;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.widget.Button;
import android.widget.TextView;

import com.baasbikes.baasbikesandroid.views.tutorial.TutorialPage;

import java.util.Arrays;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class TutorialActivity extends BaseActivity {

    private final List<TutorialPage> tutorialPages = Arrays.asList(
            TutorialPage.create("Find a bike nearby.", R.drawable.tutorial_image_1),
            TutorialPage.create("Push the button on the smart lock to wake it up.", R.drawable.tutorial_image_2),
            TutorialPage.create("Unlock the bike with Bluetooth to test ride.", R.drawable.tutorial_image_3),
            TutorialPage.create("If you like it, you can purchase the bike from the app.", R.drawable.tutorial_image_4),
            TutorialPage.create("...or relock the bike to the original lock.", R.drawable.tutorial_image_5));

    private ViewPager viewPager;
    private CircleIndicator indicator;
    private TextView alreadyHaveAccount;
    private Button nextButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        setupView();
    }

    @Override
    public void setupView() {
        viewPager = findViewById(R.id.tutorial_view_pager);
        indicator = findViewById(R.id.tutorial_view_indicator);
        alreadyHaveAccount = findViewById(R.id.tutorial_already_have_account);
        nextButton = findViewById(R.id.tutorial_next_button);

        viewPager.setAdapter(new TutorialPagerAdapter(getSupportFragmentManager()));
        viewPager.addOnPageChangeListener(new TutorialPagerChangeListener());
        indicator.setViewPager(viewPager);

        alreadyHaveAccount.setOnClickListener(v -> navigateTo(LoginActivity.class, true));
        nextButton.setOnClickListener(v -> advancePage());
    }

    @Override
    public void onBackPressed() {
//        getMixpanelAPI().track("tutorial.back");
        super.onBackPressed();
    }

    public void advancePage() {
        int currentView = viewPager.getCurrentItem();
        if (currentView == tutorialPages.size() - 1) {
            navigateTo(SignUpActivity.class, true);
        } else {
            viewPager.setCurrentItem(Math.min(currentView + 1, tutorialPages.size() - 1));
        }
    }

    private class TutorialPagerAdapter extends FragmentStatePagerAdapter {

        public TutorialPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return tutorialPages.get(position);
        }

        @Override
        public int getCount() {
            return tutorialPages.size();
        }
    }

    private class TutorialPagerChangeListener extends ViewPager.SimpleOnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
            Button button = findViewById(R.id.tutorial_next_button);
            if (position == tutorialPages.size() - 1) {
                int positiveActionColor = getResources().getColor(R.color.colorPositiveAction);
                int textColor = getResources().getColor(R.color.colorStandardBg);
                button.setText(R.string.sign_up);
                button.setTextColor(textColor);
                button.setBackgroundTintList(ColorStateList.valueOf(positiveActionColor));
            } else {
                int standardBgColor = getResources().getColor(R.color.colorStandardBg);
                int textColor = getResources().getColor(R.color.colorStandardText);
                button.setText(R.string.next);
                button.setTextColor(textColor);
                button.setBackgroundTintList(ColorStateList.valueOf(standardBgColor));
            }

//            switch (position) {
//                case 0:
//                    getMixpanelAPI().track("tutorial.page.1.reserve.viewed");
//                    break;
//                case 1:
//                    getMixpanelAPI().track("tutorial.page.2.unlock.viewed");
//                    break;
//                case 2:
//                    getMixpanelAPI().track("tutorial.page.3.adjust-seat.viewed");
//                    break;
//                case 3:
//                    getMixpanelAPI().track("tutorial.page.4.hold.viewed");
//                    break;
//                case 4:
//                    getMixpanelAPI().track("tutorial.page.5.geofence.viewed");
//                    break;
//                default:
//                    break;
//            }
        }
    }
}
