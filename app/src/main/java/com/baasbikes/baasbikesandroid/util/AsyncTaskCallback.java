package com.baasbikes.baasbikesandroid.util;

/**
 * Created by jmolineaux on 3/26/16.
 */
public interface AsyncTaskCallback<T> {

    /**
     * The task has been completed with no critical error.
     * @param data Data from the completed task
     */
    void success(T data);

    /**
     * An error occurred...
     * @param e {@link Throwable} containing the exception which failed the task.
     */
    void error(Throwable e);
}
