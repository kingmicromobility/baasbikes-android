package com.baasbikes.baasbikesandroid.util;

/**
 * Created by jmolineaux on 4/7/16.
 */
public interface AsyncBoundaryCallback<T> {

    void insideBounds(T data);

    void outsideBounds(T data);

    void error(Throwable e);
}
