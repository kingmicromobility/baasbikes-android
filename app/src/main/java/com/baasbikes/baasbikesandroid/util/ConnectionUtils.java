package com.baasbikes.baasbikesandroid.util;

import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;

import com.baasbikes.baasbikesandroid.BaasBikesApplication;

/**
 * Created by jmolineaux on 3/26/16.
 */
public class ConnectionUtils {

    public static int LOCATION_FINE_ACCESS_PERMISSIONS_REQUEST = 1;
    public static int BLUETOOTH_ENABLED_REQUEST = 2;

    public static final int BLUETOOTH_LE_STATE_DISCONNECTED = 0;
    public static final int BLUETOOTH_LE_STATE_CONNECTING = 1;
    public static final int BLUETOOTH_LE_STATE_CONNECTED = 2;

    /**
     * Checks to see if the user's device is on a wifi or mobile connection.
     * @param context
     * @return Whether or not the user has a network connection.
     */
    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    /**
     * Checks to see if the user's device has location services enabled.
     * @param context
     * @return Whether or not location services are enabled.
     */
    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }
        else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static boolean isLocationPermissionGranted(Context context) {
        return ContextCompat.checkSelfPermission(
                context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean shouldShowLocationPermissionRationale(Activity activity) {
        return ActivityCompat.shouldShowRequestPermissionRationale(
                activity, Manifest.permission.ACCESS_FINE_LOCATION);
    }

    public static void requestLocationPermission(Activity activity) {
        ActivityCompat.requestPermissions(activity,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                             Manifest.permission.ACCESS_COARSE_LOCATION},
                LOCATION_FINE_ACCESS_PERMISSIONS_REQUEST);
    }

    public static boolean hasBluetoothLowEnergyFeature(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    public static boolean isBluetoothEnabled() {
        BluetoothAdapter bluetoothAdapter = getBluetoothAdapter();
        return bluetoothAdapter != null && !bluetoothAdapter.isEnabled();
    }

    public static void requestBluetoothEnabled(Activity activity) {
        if (!isBluetoothEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, BLUETOOTH_ENABLED_REQUEST);
        }
    }

    public static BluetoothManager getBluetoothManager() {
        Context context = BaasBikesApplication.getAppContext();
        BluetoothManager manager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        return manager;
    }

    public static BluetoothAdapter getBluetoothAdapter() {
        return getBluetoothManager().getAdapter();
    }
}
