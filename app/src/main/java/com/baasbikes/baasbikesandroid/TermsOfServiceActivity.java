package com.baasbikes.baasbikesandroid;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Button;

public class TermsOfServiceActivity extends BaseActivity {

    public static Integer RESULT_ACCEPTED = 2;
    public static Integer RESULT_DECLINED = 1;

    private WebView webView;
    private Button declineButton;
    private Button acceptButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_of_service);

        setupView();
//        getMixpanelAPI().track("account.terms.viewed");
    }

    @Override
    public void setupView() {
        webView = (WebView) findViewById(R.id.tos_web_view);
        declineButton = (Button) findViewById(R.id.btn_decline);
        acceptButton = (Button) findViewById(R.id.btn_accept);

        webView.loadUrl("file:///android_asset/www/terms_of_service.html");
        declineButton.setOnClickListener(v -> declineTerms());
        acceptButton.setOnClickListener(v -> acceptTerms());
    }

    @Override
    public void onBackPressed() {
//        getMixpanelAPI().track("account.terms.back");
        super.onBackPressed();
    }

    public void declineTerms() {
//        getMixpanelAPI().track("account.terms.canceled");
        setResultAndFinish(RESULT_DECLINED);
    }

    public void acceptTerms() {
//        getMixpanelAPI().track("account.terms.accepted");
        setResultAndFinish(RESULT_ACCEPTED);
    }
}
