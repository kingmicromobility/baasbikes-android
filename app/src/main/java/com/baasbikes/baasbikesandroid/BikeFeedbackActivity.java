package com.baasbikes.baasbikesandroid;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.baasbikes.baasbikesandroid.location.LocationService;
import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.BikeFeedbackDetail;
import com.baasbikes.baasbikesandroid.models.BikeFeedbackEvent;
import com.baasbikes.baasbikesandroid.models.BikeFeedbackPrompt;
import com.baasbikes.baasbikesandroid.models.BikeFeedbackPromptSet;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BikeFeedbackActivity extends BaseActivity {

    public static String EXTRA_PROMPT_SET_NAME = "prompt_set_name";
    public static String EXTRA_STAR_RATING = "star_rating";

    private BikeFeedbackPromptSet feedbackPromptSet;
    private final Map<UUID, BikeFeedbackDetail> feedbackDetails = new HashMap<>();
    private Integer starRating;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike_feedback);

        setupView();
    }

    @Override
    public void setupView() {
        starRating = this.getIntent().getIntExtra(EXTRA_STAR_RATING, -1);
        String promptSetUuidString = this.getIntent().getStringExtra(EXTRA_PROMPT_SET_NAME);
        feedbackPromptSet = ApplicationState.getInstance().getBikeFeedbackPrompts(promptSetUuidString);
        ListView feedbackPromptListView = findViewById(R.id.feedback_prompt_list);
        FeedbackPromptSetAdapter feedbackPromptSetAdapter = new FeedbackPromptSetAdapter(this, feedbackPromptSet);
        feedbackPromptListView.setAdapter(feedbackPromptSetAdapter);
        feedbackPromptListView.setOnItemClickListener(feedbackPromptClickListener);
        Button submitFeedback = findViewById(R.id.btn_submit_feedback);
        submitFeedback.setOnClickListener(v -> onSubmitFeedbackClicked());
        setupKeyboardDismissal(findViewById(R.id.layout_feedback_activity));

//        getMixpanelAPI().track("bike.feedback.prompts.viewed");
    }

    public void setupKeyboardDismissal(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((v, event) -> {
                hideSoftKeyboard();
                return false;
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupKeyboardDismissal(innerView);
            }
        }
    }

    private void onSubmitFeedbackClicked() {
//        getMixpanelAPI().track("bike.feedback.isRideable.viewed",
//                ApplicationState.getInstance().getCurrentBike().getPropertiesAsJson());
        new AlertDialog.Builder(this)
                .setTitle(R.string.alert_submit_feedback_title)
                .setMessage(R.string.alert_submit_feedback_message)
                .setPositiveButton(R.string.yes, (dialog, which) -> submitFeedback(true))
                .setNegativeButton(R.string.no, (dialog, which) -> submitFeedback(false))
                .show();
    }

    private void submitFeedback(Boolean bikeRideable) {
        showProgress(R.string.progress_submitting_feedback);
        BikeFeedbackEvent feedbackEvent = new BikeFeedbackEvent(
                ApplicationState.getInstance().getCurrentBike(),
                LocationService.getInstance().getCurrentLatLng(),
                bikeRideable,
                starRating,
                feedbackPromptSet.getGuid(),
                feedbackDetails.values());
        feedbackEvent.save(new AsyncTaskCallback<BikeFeedbackEvent>() {
            @Override
            public void success(BikeFeedbackEvent data) {
                dismissProgress();
//                JSONObject mpProps = new JSONObject();
//                try {
//                    mpProps.put("feedback_rating", data.getStarRating());
//                    mpProps.put("feedback_is_rideable", data.isRideable());
//                    mpProps.put("feedback_propmpt_set", data.getPromptSetUUID().toString());
//                    for (BikeFeedbackDetail detail : data.getDetailSet()) {
//                        mpProps.put("feedback_prompt_"+detail.getPromptGuid().toString(), detail.getValue());
//                    }
//                } catch (JSONException e) {
//                    Log.d(TAG, e.getMessage());
//                }
//                BaasBikesApplication.getMixpanel().track("bike.feedback.submitted", mpProps);
                ApplicationState.getInstance().restore();
                BikeFeedbackActivity.this.finish();
            }

            @Override
            public void error(Throwable e) {
                dismissProgress();
                Log.e(BikeFeedbackActivity.this.getTAG(), "Error saving feedback.");
                ApplicationState.getInstance().restore();
                BikeFeedbackActivity.this.finish();
            }
        });
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    private final AdapterView.OnItemClickListener feedbackPromptClickListener = (parent, view, position, id) -> {
        CheckBox checkbox = view.findViewById(R.id.box_prompt_selected);
        checkbox.toggle();
    };

    private class FeedbackPromptSetAdapter extends ArrayAdapter<BikeFeedbackPrompt> {
        private final Context context;
        private final BikeFeedbackPromptSet promptSet;

        public FeedbackPromptSetAdapter(Context context, BikeFeedbackPromptSet promptSet) {
            super(context, R.layout.list_row_feedback_prompt, promptSet.getPrompts().toArray(new BikeFeedbackPrompt[0]));
            this.context = context;
            this.promptSet = promptSet;
        }

        @Override
        public View getView(final int position, View convertView, final ViewGroup parent) {
            final BikeFeedbackPrompt currentPrompt = promptSet.getPrompt(position);
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final ViewGroup rowView = (ViewGroup) inflater.inflate(R.layout.list_row_feedback_prompt, parent, false);
            final TextView promptName = rowView.findViewById(R.id.txt_prompt_name);
            final CheckBox checkbox = rowView.findViewById(R.id.box_prompt_selected);
            final EditText promptDetailsInput = rowView.findViewById(R.id.input_details);

            promptName.setText(currentPrompt.getDefaultText());
            checkbox.setOnCheckedChangeListener((buttonView, isChecked) -> {
                if (isChecked) {
                    feedbackDetails.put(
                            currentPrompt.getGuid(),
                            new BikeFeedbackDetail(currentPrompt, currentPrompt.getDefaultText())
                    );
                    if (currentPrompt.getDefaultText().equalsIgnoreCase("other")) {
                        promptDetailsInput.setVisibility(View.VISIBLE);
                        rowView.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
                    }
                } else {
                    feedbackDetails.remove(currentPrompt.getGuid());
                    if (currentPrompt.getDefaultText().equalsIgnoreCase("other")) {
                        rowView.setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
                        promptDetailsInput.setVisibility(View.GONE);
                    }
                }
            });

            promptDetailsInput.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    feedbackDetails.put(currentPrompt.getGuid(),
                            new BikeFeedbackDetail(currentPrompt, promptDetailsInput.getText().toString()));
                }
            });

            promptDetailsInput.setVisibility(View.GONE);
            return rowView;
        }
    }
}
