package com.baasbikes.baasbikesandroid;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;

import com.baasbikes.baasbikesandroid.models.ApplicationState;
import com.baasbikes.baasbikesandroid.models.Market;
import com.baasbikes.baasbikesandroid.models.Person;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.model.request.BaseZendeskFeedbackConfiguration;
import com.zendesk.sdk.model.request.CustomField;
import com.zendesk.sdk.network.impl.ZendeskConfig;

import java.util.Arrays;
import java.util.Locale;

public class MessageUsToBuyBikeActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_us_to_buy_bike);
        setupView();
    }

    @Override
    public void setupView() {
        Button btnContinue = findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(v -> launchZendeskContactUs());
    }

    private void launchZendeskContactUs() {
        ZendeskConfig.INSTANCE.setContactConfiguration(new BaseZendeskFeedbackConfiguration() {
            @Override
            public String getRequestSubject() {
                return "App ticket";
            }

            @Override
            public String getAdditionalInfo() {
                return "\n-------------\nSent from Baas Bikes Android.";
            }
        });

        ZendeskConfig.INSTANCE.setTicketFormId(62609l);

        String appVersion = String.format(Locale.US, "version_%s", BuildConfig.VERSION_NAME);
        CustomField customFieldAppVersion = new CustomField(28981538l, appVersion);

        String osVersion = String.format(Locale.US, "Android %s, Version %s", Build.VERSION.RELEASE, Build.VERSION.SDK_INT);
        CustomField customFieldOsVersion = new CustomField(28988197l, osVersion);

        String deviceModel = String.format(Locale.US, "%s %s %s", Build.BRAND, Build.DEVICE, Build.MODEL);
        CustomField customFieldDeviceModel = new CustomField(28981548l, deviceModel);

        Market currentMarket = ApplicationState.getInstance().getCurrentMarket();
        CustomField customFieldMarketGuid = new CustomField(28992127l, currentMarket.getGuid().toString());
        CustomField customFieldMarketName = new CustomField(28992047l, currentMarket.getName());

        ZendeskConfig.INSTANCE.setCustomFields(Arrays.asList(
                customFieldAppVersion, customFieldOsVersion, customFieldDeviceModel,
                customFieldMarketGuid, customFieldMarketName));

        person = Person.getLoggedInPerson(BaasBikesApplication.getAppContext());
        Identity identity = new AnonymousIdentity.Builder()
                .withNameIdentifier(person.getFullName())
                .withEmailIdentifier(person.getEmail())
                .withExternalIdentifier(person.getGuid().toString())
                .build();
        ZendeskConfig.INSTANCE.setIdentity(identity);

        Intent intent = new Intent(this, ContactZendeskActivity.class);
        setResult(RESULT_OK);
        finish();
        startActivity(intent);
    }
}
