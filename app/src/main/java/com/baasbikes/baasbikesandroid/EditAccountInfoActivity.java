package com.baasbikes.baasbikesandroid;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.baasbikes.baasbikesandroid.models.Person;
import com.baasbikes.baasbikesandroid.util.AsyncTaskCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class EditAccountInfoActivity extends BaseActivity {

    private static String TAG = EditAccountInfoActivity.class.getSimpleName();
    private JSONObject mpProps;

    private EditText firstNameEditText;
    private EditText lastNameEditText;
    private EditText emailEditText;
    private EditText phoneEditText;
    private EditText currentPasswordEditText;
    private EditText newPasswordEditText;
    private EditText confirmNewPasswordEditText;
    private Button submitButton;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account_info);

        setupView();
    }

    @Override
    public void setupView() {
        firstNameEditText = (EditText) findViewById(R.id.te_first_name);
        lastNameEditText = (EditText) findViewById(R.id.te_last_name);
        emailEditText = (EditText) findViewById(R.id.te_email);
        phoneEditText = (EditText) findViewById(R.id.te_phone);
        currentPasswordEditText = (EditText) findViewById(R.id.te_current_password);
        newPasswordEditText = (EditText) findViewById(R.id.te_new_password);
        confirmNewPasswordEditText = (EditText) findViewById(R.id.te_confirm_new_password);
        submitButton = (Button) findViewById(R.id.btn_submit);

        submitButton.setOnClickListener(v -> onSubmit());

        setPlaceholders();
    }

    public void setPlaceholders() {
        person = Person.getLoggedInPerson(BaasBikesApplication.getAppContext());
        firstNameEditText.setHint(person.getFirstName());
        lastNameEditText.setHint(person.getLastName());
        emailEditText.setHint(person.getEmail());
        phoneEditText.setHint(person.getPhoneNumber());
    }

    //todo: Needs validation on the application side for changed fields
    public void onSubmit() {
        Map<String, Object> changedAttributes = new HashMap<>();
        Map<String, Object> changedUserAttributes = new HashMap<>();
        mpProps = new JSONObject();
        try {
            person = Person.getLoggedInPerson(BaasBikesApplication.getAppContext());
            String firstName = firstNameEditText.getText().toString();
            if (!firstName.isEmpty() && !firstName.equals(person.getFirstName())) {
                changedAttributes.put("first_name", firstName);
                mpProps.put("$first_name", person.getFirstName());
                mpProps.put("person_first_name_changed", "yes");
            }
            String lastName = lastNameEditText.getText().toString();
            if (!lastName.isEmpty() && !lastName.equals(person.getLastName())) {
                changedAttributes.put("last_name", lastName);
                mpProps.put("$last_name", person.getLastName());
                mpProps.put("person_last_name_changed", "yes");
            }
            String email = emailEditText.getText().toString();
            if (!email.isEmpty() && !email.equals(person.getEmail())) {
                changedUserAttributes.put("email", email);
                BaasBikesApplication.getMixpanel().alias(person.getEmail(),
                        BaasBikesApplication.getMixpanel().getDistinctId());
                mpProps.put("$email", person.getEmail());
                mpProps.put("person_email_changed", "yes");
            }
            String phone = phoneEditText.getText().toString();
            if (!phone.isEmpty() && !phone.equals(person.getPhoneNumber())) {
                changedAttributes.put("phone_number", phone);
                mpProps.put("$phone", person.getPhoneNumber());
                mpProps.put("person_phone_number_changed", "yes");
            }

            if (attemptingPasswordChange()) {
                if (hasRequiredPasswordFields()) {
                    changedUserAttributes.put("old_password", currentPasswordEditText.getText().toString());
                    String newPassword = newPasswordEditText.getText().toString();
                    changedUserAttributes.put("new_password", newPassword);
                    String confirmPassword = confirmNewPasswordEditText.getText().toString();
                    changedUserAttributes.put("confirm_password", confirmPassword);
                    changedAttributes.put("user", changedUserAttributes);
                    if (confirmPassword.equals(newPassword)) {
                        mpProps.put("person_password_changed", "yes");
                        doSubmit(person, changedAttributes);
                    } else {
                        showAlertDialog(R.string.edit_account_alert_password_mismatch_title,
                                R.string.edit_account_alert_password_mismatch_message);
                    }
                } else {
                    showAlertDialog(R.string.edit_account_alert_password_missing_title,
                            R.string.edit_account_alert_password_missing_message);
                }
            } else {
                changedAttributes.put("user", changedUserAttributes);
                doSubmit(person, changedAttributes);
            }
        } catch (JSONException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    public void doSubmit(Person person, Map<String, Object> attributes) {
        final ProgressDialog progress = new ProgressDialog(this);
        progress.setMessage(getString(R.string.edit_account_progress_saving));
        person.updateAttributes(attributes, new AsyncTaskCallback<Person>() {
            @Override
            public void success(Person data) {
                progress.dismiss();
//                getMixpanelAPI().track("account.edit.submitted", mpProps);
                EditAccountInfoActivity.this.finish();
            }

            @Override
            public void error(Throwable e) {
                progress.dismiss();
                String message = e.getMessage();
                if (message != null) {
                    showAlertDialog(R.string.edit_account_alert_error_update_title, message);
                } else {
                    showAlertDialog(R.string.edit_account_alert_error_update_title, "---");
                }
            }
        });
    }

    private Boolean attemptingPasswordChange() {
        return currentPasswordEditText.getText().length() > 0
                || newPasswordEditText.getText().length() > 0
                || confirmNewPasswordEditText.getText().length() > 0;
    }

    private Boolean hasRequiredPasswordFields() {
        return currentPasswordEditText.getText().length() > 0
                && newPasswordEditText.getText().length() > 0
                && confirmNewPasswordEditText.getText().length() > 0;
    }
}
