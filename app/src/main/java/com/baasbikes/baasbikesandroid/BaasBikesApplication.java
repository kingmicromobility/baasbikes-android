package com.baasbikes.baasbikesandroid;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;


import java.util.List;

import io.branch.referral.Branch;
import com.mixpanel.android.mpmetrics.MixpanelAPI;

/**
 * Created by jmolineaux on 3/26/16.
 */
public class BaasBikesApplication extends Application {

    private static Context context;
    private static MixpanelAPI mixpanel;

    public void onCreate() {
        String projectToken = BuildConfig.MIXPANEL_TOKEN;
        mixpanel = MixpanelAPI.getInstance(this, projectToken);
        BaasBikesApplication.getMixpanel().track("app.launch.started");
        BaasBikesApplication.getMixpanel().timeEvent("app.launch.finished");
        super.onCreate();
        Branch.getAutoInstance(this);
        BaasBikesApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return BaasBikesApplication.context;
    }

    public static MixpanelAPI getMixpanel() { return BaasBikesApplication.mixpanel; }

    protected boolean isRunningInForeground() {
        ActivityManager manager =
                (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = manager.getRunningTasks(1);
        if (tasks.isEmpty()) {
            return false;
        }
        String topActivityName = tasks.get(0).topActivity.getPackageName();
        return topActivityName.equalsIgnoreCase(getPackageName());
    }
}
